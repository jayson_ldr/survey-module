-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2016 at 04:50 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `survey`
--

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  `data_type` varchar(30) DEFAULT NULL,
  `content` text,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `name`, `slug`, `type`, `data_type`, `content`, `is_active`) VALUES
(1, 'Name', 'name', 'textbox', 'character', NULL, 1),
(2, 'Age', 'age', 'textbox', 'integer', NULL, 1),
(3, 'Gender', 'gender', 'select', NULL, '["Male","Female"]', 1),
(4, 'Status', 'status', 'select', NULL, '["Married","Widowed","Separated","Divorced","Single"]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `code` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `term` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` int(11) NOT NULL,
  `survey_title` varchar(200) NOT NULL,
  `survey_title_slug` varchar(200) DEFAULT NULL,
  `quota` int(11) NOT NULL DEFAULT '0',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `survey_title`, `survey_title_slug`, `quota`, `is_active`, `date_created`, `created_by`) VALUES
(5, 'Jayson New Survey', 'jayson_new_survey', 1, 1, '2016-09-25 06:18:23', 1),
(6, 'sample survey 102', 'sample_survey_102', 10, 1, '2016-09-25 06:27:00', 1),
(8, 'new test survey', 'new_test_survey', 10, 1, '2016-10-24 04:27:20', 1),
(9, 'yii survey', 'yii_survey', 5, 1, '2016-10-28 11:11:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `survey_questions`
--

CREATE TABLE `survey_questions` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `question_title` text NOT NULL,
  `type` varchar(30) NOT NULL,
  `attempt_no` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survey_questions`
--

INSERT INTO `survey_questions` (`id`, `survey_id`, `question_title`, `type`, `attempt_no`, `date_created`, `created_by`) VALUES
(1, 5, 'my first question', 'skip', NULL, '2016-09-25 06:18:23', 1),
(2, 5, 'my second question', 'force', NULL, '2016-09-25 06:18:23', 1),
(3, 5, 'my third question', 'attempt', 2, '2016-09-25 06:18:23', 1),
(4, 6, 'my 1st question', 'skip', 0, '2016-09-25 06:27:00', 1),
(5, 6, 'my 2nd question', 'force', 0, '2016-09-25 06:27:00', 1),
(7, 8, 'my 1st question', 'skip', 0, '2016-10-24 04:27:20', 1),
(8, 8, 'my 2nd question', 'force', 0, '2016-10-24 04:27:20', 1),
(9, 9, 'sample question 1', 'skip', 0, '2016-10-28 11:11:30', 1),
(10, 9, '2nd question', 'force', 0, '2016-10-28 11:11:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `survey_question_ans`
--

CREATE TABLE `survey_question_ans` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `ans_type` int(11) NOT NULL,
  `details` text NOT NULL,
  `answer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survey_question_ans`
--

INSERT INTO `survey_question_ans` (`id`, `question_id`, `ans_type`, `details`, `answer`) VALUES
(1, 1, 1, '["Multiple Choice 1","Multiple Choice 2"]', 0),
(2, 2, 2, '["Yes","No"]', 1),
(3, 3, 1, '["Multiple Choice 1","Multiple Choice 2","Multiple Choice 3"]', 2),
(4, 4, 1, '["Multiple Choice 1","Multiple Choice 2"]', 0),
(5, 5, 2, '["Yes","No"]', 1),
(7, 7, 1, '["Multiple Choice 1","Multiple Choice 2","Multiple Choice 3"]', 0),
(8, 8, 2, '["Yes","No"]', 0),
(9, 9, 2, '["Yes","No"]', 0),
(10, 10, 1, '["Multiple Choice 1","Multiple Choice 2","Multiple Choice 3"]', 0);

-- --------------------------------------------------------

--
-- Table structure for table `survey_respondents`
--

CREATE TABLE `survey_respondents` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `dynamic_fields` text,
  `score` varchar(10) DEFAULT NULL,
  `overall_duration` double(20,2) DEFAULT '0.00',
  `ip_address` varchar(100) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survey_respondents`
--

INSERT INTO `survey_respondents` (`id`, `survey_id`, `dynamic_fields`, `score`, `overall_duration`, `ip_address`, `date_created`) VALUES
(11, 8, '[{"name":"jays"}]', '2', 0.88, '::1', '2016-10-24 02:43:05'),
(12, 8, '[{"name":"jays"}]', '1', 0.50, '::1', '2016-10-25 03:33:54'),
(13, 8, '[{"name":"jays"}]', '1', 0.26, '::1', '2016-10-25 08:01:47'),
(24, 8, '[{"name":"jays"}]', '2', 41.73, '::1', '2016-10-25 10:18:18'),
(25, 8, '[{"name":"jays"}]', '1', 14.96, '::1', '2016-10-25 10:22:52'),
(29, 8, '[{"name":"jays"}]', '2', 11.93, '::1', '2016-10-26 03:28:16'),
(30, 8, '[{"name":"jays"}]', '2', 11.76, '::1', '2016-10-26 09:27:42'),
(43, 8, '[{"name":"jayson","age":"2","gender":"Male"}]', '2', 33.44, '::1', '2016-10-27 04:43:51'),
(44, 8, '[{"name":"ldr","age":"5","gender":"Male"}]', '2', 9.69, '::1', '2016-10-27 07:00:43'),
(45, 5, '[{"name":"jayson","age":"29","gender":"Male"}]', '3', 26.95, '::1', '2016-10-28 10:51:27'),
(46, 9, '[{"name":"test","age":"25","gender":"Male"}]', '2', 13.70, '::1', '2016-10-28 11:15:20');

-- --------------------------------------------------------

--
-- Table structure for table `survey_respondent_ans`
--

CREATE TABLE `survey_respondent_ans` (
  `id` int(11) NOT NULL,
  `respondent_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `action` varchar(30) DEFAULT NULL,
  `answer_index` int(11) DEFAULT NULL,
  `duration` double(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `survey_respondent_ans`
--

INSERT INTO `survey_respondent_ans` (`id`, `respondent_id`, `survey_id`, `question_id`, `action`, `answer_index`, `duration`) VALUES
(1, 11, 8, 7, 'submit', 0, 0.59),
(2, 11, 8, 8, 'submit', 0, 0.08),
(3, 12, 8, 7, 'submit', 1, 0.09),
(4, 12, 8, 8, 'submit', 0, 0.09),
(5, 13, 8, 7, 'submit', 2, 0.07),
(6, 13, 8, 8, 'submit', 0, 0.08),
(9, 24, 8, 7, 'submit', 0, 29.34),
(10, 24, 8, 8, 'submit', 0, 5.62),
(11, 25, 8, 7, 'submit', 1, 8.72),
(12, 25, 8, 8, 'submit', 0, 3.91),
(13, 29, 8, 7, 'submit', 0, 5.21),
(14, 29, 8, 8, 'submit', 0, 4.49),
(15, 30, 8, 7, 'submit', 0, 4.51),
(16, 30, 8, 8, 'submit', 0, 4.62),
(17, 43, 8, 7, 'submit', 0, 4.68),
(18, 43, 8, 8, 'submit', 0, 3.95),
(19, 44, 8, 7, 'submit', 0, 4.53),
(20, 44, 8, 8, 'submit', 0, 3.45),
(21, 45, 5, 1, 'submit', 0, 4.00),
(22, 45, 5, 2, 'submit', 1, 8.60),
(23, 45, 5, 3, 'submit', 2, 12.29),
(24, 46, 9, 9, 'submit', 0, 7.20),
(25, 46, 9, 10, 'submit', 0, 4.09);

-- --------------------------------------------------------

--
-- Table structure for table `userlevels`
--

CREATE TABLE `userlevels` (
  `id` int(11) NOT NULL,
  `userlevel_name` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlevels`
--

INSERT INTO `userlevels` (`id`, `userlevel_name`, `slug`) VALUES
(-1, 'Administrator', 'administrator'),
(2, 'Staff', 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `userlevel_permissions`
--

CREATE TABLE `userlevel_permissions` (
  `id` int(11) NOT NULL,
  `userlevel_id` int(11) NOT NULL,
  `module_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlevel_permissions`
--

INSERT INTO `userlevel_permissions` (`id`, `userlevel_id`, `module_type`, `permission`) VALUES
(0, 2, 'Surveys', 13),
(0, 2, 'Respondents', 8),
(0, 2, 'Stats', 8),
(0, 2, 'Users', 12),
(0, 2, 'Userlevels', 0),
(0, 2, 'Userlevel Permissions', 0),
(0, 2, 'Dynamic Fields', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(50) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) DEFAULT '0',
  `userlevel_id` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `contact`, `username`, `password`, `is_active`, `is_deleted`, `userlevel_id`, `last_login`, `date_created`) VALUES
(1, 'admin', 'admin', 'jayson.sonido@yahoo.com', NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 0, -1, '2016-11-01 11:34:47', '2016-09-24 04:34:10'),
(2, 'John', 'Doe', 'john.doe@email.com', '1234567', 'john.doe', '827ccb0eea8a706c4c34a16891f84e7b', 1, 0, 2, '2016-10-28 07:05:22', '2016-09-24 04:34:10'),
(4, 'John', 'Smith', 'john@email.com', '123123', 'john', '202cb962ac59075b964b07152d234b70', 1, 0, 2, NULL, '2016-09-24 04:34:10'),
(5, 'sample XYZ', 'samoke', 'asdf@a', '123123123', 'asdf', '912ec803b2ce49e4a541068d495ab570', 0, 0, 2, '2016-10-25 11:35:11', '2016-09-24 04:34:10'),
(6, 'dexter', 'vivo', 'myemail@email.com', '123123', 'dex', '5dad4065c3dc25a179279b2dc4449d63', 1, 0, 2, '2016-10-28 03:13:21', '2016-09-24 04:34:10'),
(7, 'test', 'test', 'asdf@e', '123', 'test', '098f6bcd4621d373cade4e832627b4f6', 1, 1, 2, '2016-10-24 11:15:14', '2016-09-24 04:34:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_title` (`survey_title`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `survey_questions`
--
ALTER TABLE `survey_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_id` (`survey_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `survey_question_ans`
--
ALTER TABLE `survey_question_ans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `survey_respondents`
--
ALTER TABLE `survey_respondents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_id` (`survey_id`);

--
-- Indexes for table `survey_respondent_ans`
--
ALTER TABLE `survey_respondent_ans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `survey_id` (`survey_id`),
  ADD KEY `respondent_id` (`respondent_id`);

--
-- Indexes for table `userlevels`
--
ALTER TABLE `userlevels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userlevel_name` (`userlevel_name`);

--
-- Indexes for table `userlevel_permissions`
--
ALTER TABLE `userlevel_permissions`
  ADD KEY `userlevel_id` (`userlevel_id`),
  ADD KEY `permission` (`permission`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userlevel_id` (`userlevel_id`),
  ADD KEY `first_name` (`first_name`),
  ADD KEY `last_name` (`last_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `survey_questions`
--
ALTER TABLE `survey_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `survey_question_ans`
--
ALTER TABLE `survey_question_ans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `survey_respondents`
--
ALTER TABLE `survey_respondents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `survey_respondent_ans`
--
ALTER TABLE `survey_respondent_ans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `userlevels`
--
ALTER TABLE `userlevels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
