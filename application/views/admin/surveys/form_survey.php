<?php echo form_open('', array( 'class' => 'form-survey', 'role' => 'form' ) ); ?>
<table class="table">
	<tr>
		<td class="col-md-3"><b>Survey Title: *</b></td>
		<td class="col-md-6">
			<input type="text" id="survey_title" name="survey_title" class="form-control" required />
		</td>
		<td class="col-md-2"><b>Quota: *</b></td>
		<td class="col-md-1">
			<input type="text" id="quota" name="quota" class="form-control" required />
		</td>
	</tr>
</table>
<?php echo  form_close(); ?>

<div style="margin: 10px 0">
	<button type="button" id="submit_survey" name="submit_survey" class="btn btn-info" style="float: right"> Submit </button>
</div>