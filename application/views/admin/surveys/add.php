<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Room Category Name: *</td>
		<td class="col-md-9">
			<input type="text" id="category" name="category" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Rate / night: *</td>
		<td class="col-md-9">
			<input type="text" id="rate" name="rate" class="form-control" required />
		</td>
	</tr>
</table>
<?php echo  form_close(); ?>