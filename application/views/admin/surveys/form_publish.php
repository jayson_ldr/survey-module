<?php echo form_open('', array( 'class' => 'form-survey-publish', 'role' => 'form' ) ); ?>

<div class="alert alert-success a-float" style="top: 2%; left: 0px;">
	<a class="close" data-dismiss="alert">×</a>
	Congratulations, your new survey has been successfully created!
</div>

<table class="table">
	<tr>
		<td class="col-md-3"><b>Survey Title:</b></td>
		<td class="col-md-9">
			<?php echo $survey["survey_title"]; ?>
		</td>
	</tr>
	<tr>
		<td class="col-md-3"><b>Quota:</b></td>
		<td class="col-md-9">
			<?php echo $survey["quota"]; ?>
		</td>
	</tr>
	<tr>
		<td class="col-md-3"><b>Total Quesitons:</b></td>
		<td class="col-md-9">
			<?php echo count($survey["question"]); ?>
		</td>
	</tr>
</table>


<div class="col-md-12 question_answer_panel">
	<div class="answer_content">

		<div id="accordion" role="tablist" aria-multiselectable="true">
			<?php
				$index = 1;
				foreach($survey["question"] as $key => $question): 
			?>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading_<?php echo $key; ?>">
						<h4 class="panel-title">
							<a <?php echo ($key > 0) ? "class='collapsed'" : ""; ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $key; ?>" aria-expanded="<?php echo ($key == 0) ? "true" : "false"; ?>" aria-controls="collapse_<?php echo $key; ?>">
								<?php echo $question["question_title"]; ?>
							</a>
						</h4>
					</div>
					<div id="collapse_<?php echo $key; ?>" class="panel-collapse collapse collapse_new <?php echo ($key == 0) ? "in" : ""; ?>" role="tabpanel" aria-labelledby="heading__<?php echo $key; ?>">

						<table class="table table-stripped">
							<tbody>
								<?php
									#$details = json_decode($question["answer"]["details"]);
									foreach($question["answer"]["details"] as $i => $ans): ?>
									<tr>
										<td>
											<div class="ans_list">
												<input type="radio" /> 
												<input type="text" value="<?php echo $ans; ?>" class="form-control custom_input" readonly />
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>


						<div class="form-group">
							<label for="exampleSelect1"><b>Correct Answer:*</b></label>
							<select class="form-control" readonly disabled>
								<option value="">- Select -</option>
								<?php foreach($question["answer"]["details"] as $ii => $ans2): ?>
									<option value="<?php echo $ii; ?>" <?php echo ($question["answer"]["answer"] == $ii) ? "selected" : ""; ?>><?php echo $ans2; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

			<?php
				$index++;
				endforeach;
			?>

	</div>
	<div class="clearfix"></div>
</div>

<?php echo form_close(); ?>

<div class="clearfix"></div>
<div style="margin: 10px 0">
	<a href="<?php echo base_url("admin/surveys"); ?>" class="btn btn-info" style="float: right"> Finish </a>
	<div class="clearfix"></div>
</div>