<style>
#survey_wiz {
    overflow:hidden;
}
.col-md-3 {
    float: left;
    width: 22% !important;
}
.custom_input{width: inherit ! important; display: inline;}
.panel-heading {
    background-color: #daeeff;
    border: 1px solid #cadeef;
    border-radius: 5px;
    margin-bottom: 2px;
    padding: 10px 10px 5px;
}
.panel-collapse {
    background: #f9f9f9 none repeat scroll 0 0;
    padding: 10px;
}
</style>

<div style="margin: 20px 0 10px 0">
	<div class="col-md-6">
		<div>
			<h3><?php echo $page_title ?></h3>
		</div>
	</div>
	<div class="col-md-6">
		<a href="<?php echo base_url("admin/surveys"); ?>" class="btn btn-info" style="float:right">
			Back to list
		</a>
	</div>
	<div class="clearfix"></div>
</div>

<div id="survey_wiz" action="#">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="nav-item col-md-3" style="padding: 0px">
			<a class="nav-link active" style="text-align: center; padding: 0.5em 0em;" data-toggle="tab" href="#survey" role="tab">
				<b>Survey > </b>
			</a>
		</li>
		<li class="nav-item col-md-3" style="padding: 0px">
			<a class="nav-link disabled" style="text-align: center; padding: 0.5em 0em;" data-toggle="" href="#questions" role="tab">
				<b>Questions ></b>
			</a>
		</li>
		<li class="nav-item col-md-3" style="padding: 0px">
			<a class="nav-link disabled" style="text-align: center; padding: 0.5em 0em;" data-toggle="" href="#preview" role="tab">
				<b>Preview ></b>
			</a>
		</li>
		<li class="nav-item col-md-3" style="padding: 0px">
			<a class="nav-link disabled" style="text-align: center; padding: 0.5em 0em;" data-toggle="" href="#publish" role="tab">
				<b>Publish</b>
			</a>
		</li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content" style="padding: 10px">
		<div class="tab-pane fade in active" id="survey" role="tabpanel">
			<?php echo $this->load->view("admin/surveys/form_survey", array(), true); ?>
		</div>
		<div class="tab-pane fade" id="questions" role="tabpanel">
			<div class="form_questions"></div>
		</div>
		<div class="tab-pane fade" id="preview" role="tabpanel">
			<div class="form_preview"></div>
		</div>
		<div class="tab-pane fade" id="publish" role="tabpanel">
			<div class="form_publish"></div>
		</div>
	</div>

</div>