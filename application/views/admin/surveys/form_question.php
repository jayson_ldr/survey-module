<?php echo form_open('', array( 'class' => 'form-survey-question', 'role' => 'form' ) ); ?>
<div class="col-md-7">
	<table class="table">
		<tr>
			<td class="col-md-3"><b>Survey Title:</b></td>
			<td class="col-md-9">
				<?php echo $survey["survey_title"]; ?>
			</td>
		</tr>
		<tr>
			<td class="col-md-3"><b>Quota:</b></td>
			<td class="col-md-9">
				<?php echo $survey["quota"]; ?>
			</td>
		</tr>
		<tr>
			<td class="col-md-12"><b>Question: *</b></td>
		</tr>
		<tr>
			<td class="col-md-12">
				<!--
				<input type="text" id="question_title" name="question_title" class="form-control" required />
				-->
				<textarea id="question_title" name="question_title" class="form-control editor_tinymce" required></textarea>
			</td>
		</tr>
		<tr>
			<td class="col-md-3"><b>Condition: *</b></td>
			<td class="col-md-9">
				<select class="form-control" id="type" name="type" required >
					<option value="">- Select -</option>
					<option value="skip">Can Skip the Question</option>
					<option value="force">Right Answer Required</option>
					<option value="attempt">Allowed No of Attempts</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="col-md-3"><b>Attempts</b></td>
			<td class="col-md-9">
				<input type="text" id="attempt_no" name="attempt_no" class="form-control" value="" disabled required/>
			</td>
		</tr>
	</table>
</div>

<div class="col-md-5 answer_panel">
	<h4>Answer:</h4>
	<div class="form-group row">
		<label for="ans_type" class="col-xs-4 col-form-label"><b>Answer Type:</b></label>
		<div class="col-xs-8">
			<select class="form-control" id="ans_type" name="ans_type" required >
				<option value="">- Select -</option>
				<option value="1">Multipel Choice</option>
				<option value="2">Yes / No</option>
			</select>
		</div>
	</div>
	<div class="answer_content"></div>
	<div class="clearfix"></div>
</div>

<?php echo form_close(); ?>

<div class="clearfix"></div>
<div style="margin: 10px 0">
	<button type="button" id="submit_preview" class="btn btn-info" style="float: right" disabled> Preview </button>
	<button type="button" id="submit_question" class="btn btn-info" style="float: right; margin-right: 10px"> Save Question </button>
	<div class="clearfix"></div>
</div>



<!-- hidden template for answer types-->
<div class="answer_types_templates" style="display: none">
	<div class="type_multiple_chouce">
		<table id="MultipleChoiceTable" class="table table-stripped">
			<tbody>
				<tr>
					<td>
						<div class="ans_list">
							<input type="radio" id="MultipleChoice" /> 
							<input type="text" id="MultipleChoice_1" name="MultipleChoice[]" value="Multiple Choice 1" class="form-control custom_input" />
							<img src="<?php echo base_url('assets/img/x-button.png'); ?>" class="ans_delete"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ans_list">
							<input type="radio" id="MultipleChoice" /> 
							<input type="text" id="MultipleChoice_2" name="MultipleChoice[]" value="Multiple Choice 2" class="form-control custom_input" />
							<img src="<?php echo base_url('assets/img/x-button.png'); ?>" class="ans_delete"/>
						</div>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td>
						<input type="submit" id="MultipleChoice_Add" name="MultipleChoice_Add" value="Add Another Answer" class="btn btn-default orange-button" style="float: left"/>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="type_yes_no">
		<table id="YesNoTable" class="table table-stripped">
			<tbody>
				<tr>
					<td>
						<input type="radio" id="YesNo" name="YesNo" value="Yes" />
						<input type="text" id="YesNo_1" name="YesNo[]" value="Yes" class="form-control custom_input" readonly />
					</td>
				</tr>
				<tr>
					<td>
						<input type="radio" id="YesNo" name="YesNo" value="No" />
						<input type="text" id="YesNo_2" name="YesNo[]" value="No" class="form-control custom_input" readonly />
					</tr>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	
	/***************************************************
	** tinyMCR WYSIWYG integration [START]
	****************************************************/
	tinymce.init({
		selector: "textarea.editor_tinymce",
		entity_encoding : "raw",
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		content_css: base_url + 'assets/css/codepen.min.css',
		relative_urls : false,
		remove_script_host : false,
		convert_urls : true,
		statusbar:  false,
		menubar:    false,
	});
	/***************************************************
	** tinyMCR WYSIWYG integration [END]
	****************************************************/

</script>