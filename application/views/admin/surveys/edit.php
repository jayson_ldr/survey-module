<style>
.custom_input{width: inherit ! important; display: inline;}
.panel-heading {
    background-color: #daeeff;
    border: 1px solid #cadeef;
    border-radius: 5px;
    margin-bottom: 2px;
    padding: 10px 10px 5px;
}
.panel-collapse {
    background: #f9f9f9 none repeat scroll 0 0;
    padding: 10px;
}
.panel.panel-default{
    border: 1px solid #cadeef !important;
    margin: 5px 0 !important;
    padding: 5px !important;
    border-radius: 10px !important;
}
</style>

<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Survey Title: *</td>
		<td class="col-md-9">
			<input type="text" id="survey_title" name="survey_title" class="form-control" value="<?php echo $results->survey_title; ?>" required />
			<input type="hidden" id="id" name="id" value="<?php echo $results->id; ?>" />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Active (Y/N):</td>
		<td class="col-md-9">
			<label class="form-check-inline">
				<input type="radio" class="form-check-input" name="is_active" value="1" <?php echo ($results->is_active == "1") ? "checked" : ""; ?>/> Yes
			</label>
			<label class="form-check-inline">
				<input type="radio" class="form-check-input" name="is_active" value="0" <?php echo ($results->is_active == "0") ? "checked" : ""; ?>/> No
			</label>
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Quota: *</td>
		<td class="col-md-9">
			<input type="text" id="quota" name="quota" class="form-control" value="<?php echo $results->quota; ?>" required />
		</td>
	</tr>
</table>

<div class="question_answer_panel">
	<div class="answer_content">

		<div id="accordion" role="tablist" aria-multiselectable="true">
			<?php
				$index = 1;
				foreach($survey["question"] as $key => $question): 
			?>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading_<?php echo $key; ?>">
						<h4 class="panel-title">
							<a class='collapsed' <?php #class='collapsed' echo ($key > 0) ? "class='collapsed'" : ""; ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $key; ?>" aria-expanded="<?php echo ($key == 0) ? "true" : "false"; ?>" aria-controls="collapse_<?php echo $key; ?>">
								<?php #echo html_entity_decode($question["question_title"]); ?>

								<input type="hidden" id="question_id" name="question[<?php echo $key; ?>][question_id]" value="<?php echo $question["id"]; ?>" />

								<textarea id="question_title_<?php echo $key; ?>" name="question[<?php echo $key; ?>][question_title]" class="form-control editor_tinymce" required><?php echo html_entity_decode($question["question_title"]); ?></textarea>

								<!-- <input type="hidden" id="question_title" name="question[<?php echo $key; ?>][question_title]" value="<?php echo $question["question_title"]; ?>" /> -->

								<input type="hidden" id="type" name="question[<?php echo $key; ?>][type]" value="<?php echo $question["type"]; ?>" />
								<?php if($question["attempt_no"]): ?>
									<input type="hidden" id="attempt_no" name="question[<?php echo $key; ?>][attempt_no]" value="<?php echo $question["attempt_no"]; ?>" />
								<?php endif; ?>
							</a>
						</h4>
					</div>
					<div id="collapse_<?php echo $key; ?>" class="panel-collapse collapse collapse_new in <?php #echo ($key == 0) ? "in" : ""; ?>" role="tabpanel" aria-labelledby="heading__<?php echo $key; ?>">

						<input type="hidden" id="ans_type" name="question[<?php echo $key; ?>][answer][ans_type]" value="<?php echo $question["answer"]["ans_type"]; ?>" />

						<input type="hidden" id="answer_id" name="question[<?php echo $key; ?>][answer][answer_id]" value="<?php echo $question["answer"]["answer_id"]; ?>" />
						
						<table class="table table-stripped">
							<tbody>
								<?php foreach($question["answer"]["details"] as $i => $ans): ?>
									<tr>
										<td>
											<div class="ans_list">
												<input type="radio" disabled /> 
												<input type="text" name="question[<?php echo $key; ?>][answer][details][<?php echo $i; ?>]" value="<?php echo $ans; ?>" class="form-control custom_input" readonly />
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>


						<div class="form-group">
							<label for="exampleSelect1"><b>Set Correct Answer:*</b></label>
							<select class="form-control" id="exampleSelect1" name="question[<?php echo $key; ?>][answer][answer]" required>
								<option value="">- Select -</option>
								<?php foreach($question["answer"]["details"] as $ii => $ans2): ?>
									<option value="<?php echo $ii; ?>" <?php echo ($question["answer"]["answer"] == $ii) ? "selected" : ""; ?>><?php echo $ans2; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

			<?php
				$index++;
				endforeach;
			?>

	</div>
	<div class="clearfix"></div>
</div>

<?php echo  form_close(); ?>

<div class="clearfix"></div>

<script type="text/javascript">
	
	/***************************************************
	** tinyMCR WYSIWYG integration [START]
	****************************************************/
	tinymce.init({
		selector: "textarea.editor_tinymce",
		setup: function (editor) { //this is important to capture updated content of textarea
	        editor.on('change', function () {
	            editor.save();
	        });
	    },
		entity_encoding : "raw",
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		content_css: base_url + 'assets/css/codepen.min.css',
		relative_urls : false,
		remove_script_host : false,
		convert_urls : true,
		statusbar:  false,
		menubar:    false,
	});

	/***************************************************
	** tinyMCR WYSIWYG integration [END]
	****************************************************/

</script>