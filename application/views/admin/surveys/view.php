<style>
.custom_input{width: inherit ! important; display: inline;}
.panel-heading {
    background-color: #daeeff;
    border: 1px solid #cadeef;
    border-radius: 5px;
    margin-bottom: 2px;
    padding: 10px 10px 5px;
}
.panel-collapse {
    background: #f9f9f9 none repeat scroll 0 0;
    padding: 10px;
}
</style>

<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Survey ID:</td>
		<td class="col-md-9"><?php echo $results->id; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Survey titel:</td>
		<td class="col-md-9"><?php echo $results->survey_title; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Date Created:</td>
		<td class="col-md-9"><?php echo date("M d, Y H:i", strtotime($results->date_created)) ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Created By:</td>
		<td class="col-md-9"><?php echo $results->user_name; ?></td>
	</tr>
</table>

<h4>Questions:</h4>
<div class="question_answer_panel">
	<div class="answer_content">

		<div id="accordion" role="tablist" aria-multiselectable="true">
			<?php
				$index = 1;
				foreach($survey["question"] as $key => $question): 
			?>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading_<?php echo $key; ?>">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $key; ?>" aria-expanded="false" aria-controls="collapse_<?php echo $key; ?>">
								<?php echo html_entity_decode($question["question_title"]); ?>
							</a>
						</h4>
					</div>
					<div id="collapse_<?php echo $key; ?>" class="panel-collapse collapse collapse_new" role="tabpanel" aria-labelledby="heading_<?php echo $key; ?>">

						<table class="table table-stripped">
							<tbody>
								<?php foreach($question["answer"]["details"] as $i => $ans): ?>
									<tr>
										<td>
											<div class="ans_list">
												<input type="radio" disabled /> 
												<input type="text" value="<?php echo $ans; ?>" class="form-control custom_input" readonly />
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>


						<div class="form-group">
							<label for="exampleSelect1"><b>Correct Answer:*</b></label>
							<select class="form-control" readonly disabled>
								<option value="">- Select -</option>
								<?php foreach($question["answer"]["details"] as $ii => $ans2): ?>
									<option value="<?php echo $ii; ?>" <?php echo ($question["answer"]["answer"] == $ii) ? "selected" : ""; ?>><?php echo $ans2; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>

			<?php
				$index++;
				endforeach;
			?>

	</div>
	<div class="clearfix"></div>
</div>