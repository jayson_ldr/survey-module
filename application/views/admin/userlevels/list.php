<div style="margin: 20px 0 10px 0">
	<div class="col-md-6">
		<div>
			<h3><?php echo $page_title.": ".$total; ?></h3>
		</div>
	</div>
	<div class="col-md-6">
		<div class="input-group">
			<?php echo form_open(base_url("admin/".$module."/index"), array( 'class' => 'form-search', 'role' => 'form' ) ); ?>
				<input type="text" class="form-control" placeholder="Keyword" id="term" name="term" style="border-bottom-right-radius: 0; border-top-right-radius: 0;" />
			<?php echo  form_close(); ?>
			
			<span class="input-group-btn">
				<button type="button" class="btn btn-secondary btn_search_action">
					Search
				</button>
				<a href="<?php echo base_url("admin/".$module); ?>" class="btn btn-secondary">
					Reset
				</a>
				<?php if($security->CanAdd):?>
					<button type="button" class="btn btn-info pull-right" data-action="add" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal">
						Create New
					</button>
				<?php endif; ?>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<table class="table table-striped table-hover">
	<thead class="thead-inverse">
		<tr>
			<th>
				<?php echo set_url("admin/".$module, "ID", "id", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Userlevel / Role", "userlevel_name", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Slug", "slug", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th></th>
			<th style="text-align: right !important">Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if($results):
				foreach($results as $rec): ?>
					<tr>
						<td><?php echo $rec->id; ?></td>
						<td><?php echo $rec->userlevel_name; ?></td>
						<td><?php echo $rec->slug; ?></td>
						<td>
							<?php if($security_permission->CanView):?>
								<a href="<?php echo base_url("admin/".$module.'/permissions/'.$rec->id."/".$rec->userlevel_name);?>">Permissions</a>
							<?php endif; ?>
						</td>
						<td style="text-align: right !important">
							<div class="btn-group" role="group" aria-label="Basic example">
								<?php if($security->CanView):?>
									<button type="button" data-action="view" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal" class="btn btn-sm btn-info" data-id="<?php echo $rec->id?>">
										View
									</button>
								<?php endif; ?>
								<?php if($security->CanEdit):?>
									<button type="button" data-action="edit" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal" class="btn btn-sm btn-warning" data-id="<?php echo $rec->id?>" <?php echo ($userlevel_id == $rec->id) ? "disabled" : ""; ?>>
										Edit
									</button>
								<?php endif; ?>
								<?php if($security->CanDelete):?>
									<button type="button" data-controller="<?php echo "admin/".$module; ?>" class="btn btn-sm btn-danger btn_delete_action" data-id="<?php echo $rec->id?>" <?php echo ($userlevel_id == $rec->id) ? "disabled" : ""; ?>>
										Delete
									</button>
								<?php endif; ?>
							</div>
						</td>
					</tr>
		<?php
				endforeach;
			else:
		?>
			<tr>
				<td colspan="5">No record found!</td>
			</tr>
		<?php endif; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
				<div id="pagination">
					<?php echo $links; ?>
				</div>
			</td>
		</tr>
	</tfoot>
</table>