<div style="margin: 20px 0 10px 0">
    <div class="col-md-9">
        <div>
            <h3><?php echo $page_title; ?></h3>
        </div>
    </div>
    <div class="col-md-3">
        <a href="<?php echo base_url("admin/".$module); ?>" class="btn btn-info" style="float: right">
            << Back to List
        </a>
    </div>
    <div class="clearfix"></div>
</div>

<div class="form">
	<?php
		$attributes = array('name' => 'userpriv', 'id' => 'userpriv');
		echo form_open(base_url("admin/".$module."/permissions"), $attributes);
	?>
        <input type="hidden" name="userlevel_id" id="userlevel_id" value="<?php echo $record->userlevel_id ?>">
        <input type="hidden" name="userlevel_name" id="userlevel_name" value="<?php echo $userlevel_name ?>">
        
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>Tables</th>
                    <!--<th style="text-align:center">
                        List &nbsp;
                        <input type="checkbox" value="" onClick="ew_SelectAll('List', this.checked);"<?php echo $sDisabled ?>>
                    </th>-->
                    <th style="text-align:center">
                        View &nbsp;
                        <input type="checkbox" value="" onClick="ew_SelectAll('View', this.checked);"<?php echo $sDisabled ?>>
                    </th>
                    <th style="text-align:center">
                        Add &nbsp;
                        <input type="checkbox" value="" onClick="ew_SelectAll('Add', this.checked);"<?php echo $sDisabled ?>>
                    </th>
                    <th style="text-align:center">
                        Edit &nbsp;
                        <input type="checkbox" value="" onClick="ew_SelectAll('Edit', this.checked);"<?php echo $sDisabled ?>>
                    </th>
                    <th style="text-align:center">
                        Delete &nbsp;
                        <input type="checkbox" value="" onClick="ew_SelectAll('Delete', this.checked);"<?php echo $sDisabled ?>>
                    </th>
                </tr>
            </thead>
            <?php					            
                for ($i = 0; $i < count($USER_LEVEL_TABLE_NAME); $i++) {
                    $TempPriv = $this->access->GetUserLevelPrivEx($USER_LEVEL_TABLE_NAME[$i], $this->access->CurrentValue);	
            ?>
            <tbody>
                <tr> 
                    <td>
                        <?php echo $USER_LEVEL_TABLE_NAME[$i] ?>
                    </td>  
                    <?php /* ?><td style="text-align:center">
                        <input type="checkbox" name="List_<?php echo $i ?>" id="List_<?php echo $i ?>" value="8" <?php if (($TempPriv & $this->access->ALLOW_LIST) == $this->access->ALLOW_LIST) { ?>checked<?php } ?><?php echo $sDisabled ?>>
                    </td><?php */ ?>
                    <td style="text-align:center">
                        <input type="checkbox" name="View_<?php echo $i ?>" id="View_<?php echo $i ?>" value="8" <?php if (($TempPriv & $this->access->ALLOW_VIEW) == $this->access->ALLOW_VIEW) { ?>checked<?php } ?><?php echo $sDisabled ?>>
                    </td>
                    <td style="text-align:center">
                        <?php if($USER_LEVEL_TABLE_NAME[$i] != "Reports"): ?>
                            <input type="checkbox" name="Add_<?php echo $i ?>" id="Add_<?php echo $i ?>" value="1" <?php if (($TempPriv & $this->access->ALLOW_ADD) == $this->access->ALLOW_ADD) { ?>checked<?php } ?><?php echo $sDisabled ?>>
                        <?php else: ?>
                            -
                        <?php endif; ?>
                    </td>
                    <td style="text-align:center">
                        <?php if($USER_LEVEL_TABLE_NAME[$i] != "Reports"): ?>
                            <input type="checkbox" name="Edit_<?php echo $i ?>" id="Edit_<?php echo $i ?>" value="4" <?php if (($TempPriv & $this->access->ALLOW_EDIT) == $this->access->ALLOW_EDIT) { ?>checked<?php } ?><?php echo $sDisabled ?>>
                        <?php else: ?>
                            -
                        <?php endif; ?>
                    </td>
                    <td style="text-align:center">
                        <?php if($USER_LEVEL_TABLE_NAME[$i] != "Reports"): ?>
                            <input type="checkbox" name="Delete_<?php echo $i ?>" id="Delete_<?php echo $i ?>" value="2" <?php if (($TempPriv & $this->access->ALLOW_DELETE) == $this->access->ALLOW_DELETE) { ?>checked<?php } ?><?php echo $sDisabled ?>>
                        <?php else: ?>
                            -
                        <?php endif; ?>
                    </td>
                </tr>
            </tbody>
            <?php } ?>
        </table>
        <div class="row show-grid">
            <div class="col-md-12">
                <button type="submit" id="update" class="btn btn-warning" style="float: right">
                    Update Permission
                </button>
                <div class="clearfix"></div>
            </div>
        </div>  
	<?php echo form_close(); ?>    
</div>

<script language="javascript">
<!--
function ew_SelectAll(sCtrl, bChecked)	{
	for (i=0; i<document.userpriv.elements.length; i++) {
		var elm = document.userpriv.elements[i];
		if (elm.type == "checkbox" && elm.name.substr(0, sCtrl.length+1) == sCtrl + "_") {
			elm.checked = bChecked;
		}
	}
}
//-->
</script>