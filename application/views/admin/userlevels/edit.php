<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Userlevel / Role: *</td>
		<td class="col-md-9">
			<input type="text" id="userlevel_name" name="userlevel_name" class="form-control" value="<?php echo $results->userlevel_name; ?>" required />
			<input type="hidden" id="id" name="id" value="<?php echo $results->id; ?>" />
		</td>
	</tr>
</table>
<?php echo  form_close(); ?>