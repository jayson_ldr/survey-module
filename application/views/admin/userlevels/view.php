<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">ID:</td>
		<td class="col-md-9"><?php echo $results->id; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Userlevel / Role:</td>
		<td class="col-md-9"><?php echo $results->userlevel_name; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Slug:</td>
		<td class="col-md-9"><?php echo $results->slug; ?></td>
	</tr>
</table>