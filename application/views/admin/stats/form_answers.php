<style type="text/css">
.custom-alert {margin-bottom: 0 !important; padding: 5px 10px !important; background-color: #efefef !important;}
</style>

<?php if(count($answer) > 0):?>
	<table class="table table-stripped">
		<tbody>
			<tr>
				<th style="width: 90% !important;">Answer List</th>
				<th style="text-align: center !important;">Count</th>
			</tr>

			<?php foreach($answer["details"] as $i => $answer): ?>
				<tr>
					<td>
						<div class="ans_list">
							<div class="alert alert-info custom-alert" role="alert">
								<?php echo $answer["ans"]; ?>
							</div>
						</div>
					</td>
					<td align="center">
						<?php echo $answer["total"]; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="alert alert-warning" role="alert">
		There are total of <b><?php echo $all_respondents; ?></b> Respondents who answered this question.
	</div>
<?php else: ?>
	<div class="alert alert-danger" role="alert">
		<strong>Sorry!</strong> No answers available, please select a question.
	</div>
<?php endif; ?>