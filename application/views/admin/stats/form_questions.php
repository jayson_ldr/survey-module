<select id="question" name="question" class="form-control select_question">
	<option value="">- Select -</option>
	<?php foreach($questions as $question): ?>
		<option value="<?php echo $question->id; ?>"><?php echo $question->question_title; ?></option>
	<?php endforeach;?>
</select>

<script type="text/javascript">
	$(function () {
		$("select").select2({
			placeholder: "- Select -",
			allowClear: true
		});
	})
</script>