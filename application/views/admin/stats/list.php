<style type="text/css">
.search_survey, .survey_stats_panel, .survey_graph{border: 2px solid #ccc; padding: 20px; width: 100%; margin-bottom:  10px; border-radius: 5px 5px}
.question_list{width: 100%; margin-bottom: 10px}
#survey-error {color: red !important; float: right !important;}
/*.survey_overview {background-color: #e8ffff;border: 1px solid #5bc0de;border-radius: 5px;margin-bottom: 15px;}*/

.survey_overview .panel-info {
    border-color: #bce8f1;
}
.survey_overview .panel {
    background-color: #fff;
    border: 1px solid #bce8f1;
    border-radius: 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
    margin-bottom: 20px;
}
.survey_overview .panel-info > .panel-heading {
    background-color: #d9edf7;
    border-color: #bce8f1;
    color: #31708f;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    padding: 10px 15px;
}
.survey_overview .panel-body {
    padding: 15px;
}

.survey_overview .form-group {margin-bottom: 5px !important;}
.survey_overview .col-form-label {padding-bottom: 0.2rem !important; padding-top: 0.2rem !important;}
.survey_overview .form-control-static {min-height: 1.2rem !important;padding-bottom: 0.2rem !important;padding-top: 0.2rem !important;}
canvas {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}
</style>
<div style="margin: 20px 0 10px 0">
	<div class="col-md-12">
		<div>
			<h3><?php echo $page_title.": "; ?></h3>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<?php echo form_open('', array( 'class' => 'form-search', 'role' => 'form' ) ); ?>
<div class="search_survey">
	<div class="col-md-6">
		<label for="survey">Available Surveys:</label>
		<select id="survey" name="survey" class="form-control select_survey" required>
			<option value="">- Select -</option>
			<?php foreach($surveys as $survey): ?>
				<option value="<?php echo $survey->id; ?>"><?php echo $survey->survey_title; ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<div class="col-md-6">
		<label>Date Range:</label>
		<div class="input-group input-daterange">
		    <input type="text" id="date_from" name="date_from" class="form-control">
		    <span class="input-group-addon">to</span>
		    <input type="text" id="date_to" name="date_to" class="form-control">
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-12">
		<button type="button" class="btn btn-info" id="action_search_survey" style="float: right; margin-top: 10px">Search</button>
	</div>
	<div class="clearfix"></div>
</div>
<?php echo  form_close(); ?>

<div class="survey_stats_panel">
	<div class="col-md-12">

		<div class="survey_overview" hidden>
			<div class="panel panel-info">
				<div class="panel-heading">Survey Information:</div>
	  			<div class="panel-body container">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><b>Survey Title: </b></label>
						<div class="col-sm-10">
							<p class="form-control-static mb-0 s_title">Sample</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><b>Survey Quota: </b></label>
						<div class="col-sm-10">
							<p class="form-control-static mb-0 s_quota">0 / 0</p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><b>Survey Author: </b></label>
						<div class="col-sm-10">
							<p class="form-control-static mb-0 s_author">John Doe</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<label for="question">Available Questions:</label>
		<div class="question_list">
			<select id="question" name="question" class="form-control select_question">
				<option value="">- Select -</option>
			</select>
		</div>
		
		<div class="answers_list">
			<div class="alert alert-warning" role="alert">
				<strong>Notice: </strong> Select from the avilable survey first.
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="survey_graph">
	<label for="survey">Surveys Graph:</label>
	<div class="graph_container">
		<canvas id="myChart"></canvas>
	</div>
</div>