<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Field: *</td>
		<td class="col-md-9">
			<input type="text" id="name" name="name" class="form-control" value="<?php echo $results->name; ?>" required />
			<input type="hidden" id="id" name="id" value="<?php echo $results->id; ?>" />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Type: *</td>
		<td class="col-md-9">
			<input type="text" id="type" name="type" class="form-control" value="<?php echo $results->type; ?>" readonly />
		</td>
	</tr>
	<?php if(!$results->content): ?>
	<tr>
		<td class="col-md-3">Data Type: *</td>
		<td class="col-md-9">
			<input type="text" id="data_type" name="data_type" class="form-control" value="<?php echo $results->data_type; ?>" readonly />
		</td>
	</tr>
	<?php endif; ?>
	<?php if($results->content): ?>
	<tr>
		<td class="col-md-3">Content:</td>
		<td class="col-md-9">
			<button type="button" class="btn btn-info add_content">Add Content</button>
			<div class="content_list" style="margin-top: 10px;">
				<?php foreach($results->content as $value): ?>
					<div style='margin-bottom: 5px'>
						<input type='text' name='content[]' class='form-control' value="<?php echo $value; ?>" style='width: 90%; float: left'>
						<button type='button' class='btn btn-danger col-md-1 remove_field' style='float: left; margin-left: 5px;'>-</button>
						<div class='clearfix'></div>
					</div>
				<?php endforeach; ?>
			</div>
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Active (Y/N):</td>
		<td class="col-md-9">
			<label class="form-check-inline">
				<input type="radio" class="form-check-input" name="is_active" value="1" <?php echo ($results->is_active == "1") ? "checked" : ""; ?>/> Yes
			</label>
			<label class="form-check-inline">
				<input type="radio" class="form-check-input" name="is_active" value="0" <?php echo ($results->is_active == "0") ? "checked" : ""; ?>/> No
			</label>
		</td>
	</tr>
	<?php endif; ?>
</table>
<?php echo  form_close(); ?>

<div class="template_input" hidden>
<input type="text" name="content[]" class="form-control field_template">
</div>