<style type="text/css">
	.select2.select2-container{ width: 50% !important;}
	.select2-container--default .select2-selection--single {border-radius: 0 !important;}
</style>
<div style="margin: 20px 0 10px 0">
	<div class="col-md-4">
		<div>
			<h3><?php echo $page_title.": ".$total_rows; ?></h3>
		</div>
	</div>
	<div class="col-md-8">
		<div class="input-group">
			<?php echo form_open(base_url("admin/".$module."/index"), array( 'class' => 'form-search-new', 'role' => 'form' ) ); ?>
				<select id="survey" name="survey" class="form-control">
					<option value=""></option>
					<?php foreach($surveys as $survey): ?>
						<option value="<?php echo $survey->id; ?>" <?php echo ( ($survey->id == $this->input->post("survey")) || ($survey->id == $survey_id) ) ? "selected" : ""; ?>><?php echo $survey->survey_title; ?></option>
					<?php endforeach;?>
				</select>
				<input type="text" class="form-control" placeholder="Keyword" id="term" name="term" style="border-radius: 4px 0 0 4px !important; width: 50%" />
			<?php echo  form_close(); ?>
			
			<span class="input-group-btn">
				<button type="button" class="btn btn-secondary btn_search_action_new">
					Search
				</button>
				<a href="<?php echo base_url("admin/".$module); ?>" class="btn btn-secondary">
					Reset
				</a>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<table class="table table-striped table-hover">
	<thead class="thead-inverse">
		<tr>
			<th>
				<?php echo set_url("admin/".$module, "ID", "id", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Survey", "survey_title", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				Respondent
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Total Score", "score", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Total Time", "overall_duration", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Date Created", "date_created", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th style="text-align: right !important">Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if($results):
				foreach($results as $rec): ?>
					<tr>
						<td><?php echo $rec->id; ?></td>
						<td><?php echo $rec->survey_title; ?></td>
						<td><?php echo $rec->respondent; ?></td>
						<td><?php echo $rec->score." / ".$rec->total_questions; ?></td>
						<td><?php echo $rec->overall_duration." (s)"; ?></td>
						<td><?php echo date("M d, Y H:i", strtotime($rec->date_created)); ?></td>
						<td style="text-align: right !important">
							<div class="btn-group" role="group" aria-label="Basic example">
								<?php if($security->CanView):?>
									<button type="button" data-action="view" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal" class="btn btn-sm btn-info" data-id="<?php echo $rec->id?>">
										View
									</button>
								<?php endif; ?>
							</div>
						</td>
					</tr>
		<?php
				endforeach;
			else:
		?>
			<tr>
				<td colspan="7">No record found!</td>
			</tr>
		<?php endif; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
				<div id="pagination">
					<?php echo $links; ?>
				</div>
			</td>
		</tr>
	</tfoot>
</table>