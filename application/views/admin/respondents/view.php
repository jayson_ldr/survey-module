<style>
.custom_input{width: inherit ! important; display: inline;}
.panel-heading {
    background-color: #daeeff;
    border: 1px solid #cadeef;
    border-radius: 5px;
    margin-bottom: 2px;
    padding: 10px 10px 5px;
}
.panel-collapse {
    background: #f9f9f9 none repeat scroll 0 0;
    padding: 10px;
}
.table-success, .table-success > td, .table-success > th {
    background-color: #efffe8 !important;
}
</style>


<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3"><b>ID:</b></td>
		<td class="col-md-9"><?php echo $results->id; ?></td>
	</tr>
	<?php foreach($results->respondent_info as $key => $info): ?>
		<tr>
			<td class="col-md-3"><b><?php echo ucfirst($key); ?>:</b></td>
			<td class="col-md-9"><?php echo $info; ?></td>
		</tr>
	<?php endforeach; ?>
	<tr>
		<td class="col-md-3"><b>Survey Title:</b></td>
		<td class="col-md-9"><?php echo $results->survey_title; ?></td>
	</tr>
	<tr>
		<td class="col-md-3"><b>Score:</b></td>
		<td class="col-md-9"><?php echo $results->score." / ".$results->total_questions; ?></td>
	</tr>
	<tr>
		<td class="col-md-3"><b>Total Time (seconds):</b></td>
		<td class="col-md-9"><?php echo $results->overall_duration; ?></td>
	</tr>
	<tr>
		<td class="col-md-3"><b>Date Conducted:</b></td>
		<td class="col-md-9"><?php echo date("Y/m/d", strtotime($results->date_created)); ?></td>
	</tr>
</table>

<h4>Questions:</h4>
<div class="question_answer_panel">
	<div class="answer_content">

		<div id="accordion" role="tablist" aria-multiselectable="true">
			<?php
				$index = 1;
				foreach($survey["question"] as $key => $question): 

					$res_ans = $results->answers[$question["id"]];
			?>

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading_<?php echo $key; ?>">
						<h4 class="col-md-10 panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $key; ?>" aria-expanded="false" aria-controls="collapse_<?php echo $key; ?>">
								<?php echo html_entity_decode($question["question_title"]); ?>
							</a>
						</h4>
						<div class="col-md-2">
							<b>Time (s): <?php echo $res_ans->duration; ?></b>
						</div>
						<div class="clearfix"></div>
					</div>
					<div id="collapse_<?php echo $key; ?>" class="panel-collapse collapse collapse_new in" role="tabpanel" aria-labelledby="heading_<?php echo $key; ?>">

						<table class="table table-stripped">
							<tbody>
								<?php foreach($question["answer"]["details"] as $i => $ans): ?>
									<tr>
										<td class="<?php echo ($res_ans->answer_index == $i) ? "table-success" : ""; ?>">
											<div class="ans_list">
												<input type="radio" <?php echo ($res_ans->answer_index == $i) ? "checked" : ""; ?> readonly disabled/> 
												<?php echo $ans; ?>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

					</div>
				</div>

			<?php
				$index++;
				endforeach;
			?>

	</div>
	<div class="clearfix"></div>
</div>