<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Name: *</td>
		<td class="col-md-9">
			<input type="text" id="name" name="name" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Type: *</td>
		<td class="col-md-9">
			<select id="type" name="type" class="form-control" required="">
				<option value="">- Select -</option>
				<?php foreach($types as $type): ?>
					<option value="<?php echo $type?>"><?php echo $type?></option>
				<?php endforeach; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Data Type:</td>
		<td class="col-md-9">
			<select id="data_type" name="data_type" class="form-control" disabled>
				<option value="">- Select -</option>
				<?php foreach($data_types as $dtype): ?>
					<option value="<?php echo $dtype?>"><?php echo $dtype?></option>
				<?php endforeach; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Content:</td>
		<td class="col-md-9">
			<button type="button" class="btn btn-info add_content" disabled>Add Content</button>
			<div class="content_list" style="margin-top: 10px;">
			
			</div>
		</td>
	</tr>
</table>
<?php echo  form_close(); ?>


<div class="template_input" hidden>
<input type="text" name="content[]" class="form-control field_template">
</div>