<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">ID:</td>
		<td class="col-md-9"><?php echo $results->id; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Name:</td>
		<td class="col-md-9"><?php echo $results->name; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Type:</td>
		<td class="col-md-9"><?php echo $results->type; ?></td>
	</tr>
	<?php if(!$results->content): ?>
	<tr>
		<td class="col-md-3">Data Type:</td>
		<td class="col-md-9"><?php echo $results->data_type; ?></td>
	</tr>
	<?php endif; ?>
	<?php if($results->content): ?>
	<tr>
		<td class="col-md-3">Content:</td>
		<td class="col-md-9">
			<div class="content_list" style="margin-top: 10px;">
				<?php foreach($results->content as $value): ?>
					<div style='margin-bottom: 5px'>
						<input type='text' class='form-control' value="<?php echo $value; ?>" readonly>
					</div>
				<?php endforeach; ?>
			</div>
		</td>
	</tr>
	<?php endif; ?>
</table>