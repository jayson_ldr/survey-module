<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">User ID:</td>
		<td class="col-md-9"><?php echo $results->id; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">First Name:</td>
		<td class="col-md-9"><?php echo $results->first_name; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Last Name:</td>
		<td class="col-md-9"><?php echo $results->last_name; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Email:</td>
		<td class="col-md-9"><?php echo $results->email; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Contact:</td>
		<td class="col-md-9"><?php echo $results->contact; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Active (Y/N):</td>
		<td class="col-md-9"><?php echo ($results->is_active) ? "Yes" : "No"; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Username:</td>
		<td class="col-md-9"><?php echo $results->username; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Userlevel / Role:</td>
		<td class="col-md-9"><?php echo $results->userlevel_name; ?></td>
	</tr>
</table>