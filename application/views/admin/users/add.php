<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">First Name: *</td>
		<td class="col-md-9">
			<input type="text" id="first_name" name="first_name" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Last Name: *</td>
		<td class="col-md-9">
			<input type="text" id="last_name" name="last_name" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Email: *</td>
		<td class="col-md-9">
			<input type="email" id="email" name="email" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Contact:</td>
		<td class="col-md-9">
			<input type="text" id="contact" name="contact" class="form-control" />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Username: *</td>
		<td class="col-md-9">
			<input type="text" id="username" name="username" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Password: *</td>
		<td class="col-md-9">
			<input type="password" id="password" name="password" class="form-control" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Userlevel / Role:</td>
		<td class="col-md-9">
			<select class="form-control" id="userlevel_id" name="userlevel_id" required >
				<option value="">- Select -</option>
				<?php foreach($userlevels as $userlevel): ?>
					<option value="<?php echo $userlevel["id"]; ?>"><?php echo $userlevel["userlevel_name"]; ?></option>
				<?php endforeach; ?>
			</select>
		</td>
	</tr>
</table>
<?php echo  form_close(); ?>