<?php echo form_open('', array( 'class' => 'form-module', 'role' => 'form' ) ); ?>
<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">First Name: *</td>
		<td class="col-md-9">
			<input type="text" id="first_name" name="first_name" class="form-control" value="<?php echo $results->first_name; ?>" required />
			<input type="hidden" id="id" name="id" value="<?php echo $results->id; ?>" />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Last Name: *</td>
		<td class="col-md-9">
			<input type="text" id="last_name" name="last_name" class="form-control" value="<?php echo $results->last_name; ?>" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Email: *</td>
		<td class="col-md-9">
			<input type="email" id="email" name="email" class="form-control" value="<?php echo $results->email; ?>" required />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Contact:</td>
		<td class="col-md-9">
			<input type="text" id="contact" name="contact" class="form-control" value="<?php echo $results->contact; ?>" />
		</td>
	</tr>
	<?php if($userlevel_id == "-1"): ?>
		<tr>
			<td class="col-md-3">Active (Y/N):</td>
			<td class="col-md-9">
				<label class="form-check-inline">
					<input type="radio" class="form-check-input" name="is_active" value="1" <?php echo ($results->is_active == "1") ? "checked" : ""; ?>/> Yes
				</label>
				<label class="form-check-inline">
					<input type="radio" class="form-check-input" name="is_active" value="0" <?php echo ($results->is_active == "0") ? "checked" : ""; ?>/> No
				</label>
			</td>
		</tr>
	<?php else: ?>
		<tr>
			<td class="col-md-3">Active (Y/N):</td>
			<td class="col-md-9">
				<?php echo ($results->is_active == "1") ? "YES" : "NO"; ?>
			</td>
		</tr>
	<?php endif; ?>
	<tr>
		<td class="col-md-3">Username:</td>
		<td class="col-md-9">
			<input type="text" id="username" name="username" class="form-control" value="<?php echo $results->username; ?>" />
		</td>
	</tr>
	<tr>
		<td class="col-md-3">Password:</td>
		<td class="col-md-9">
			<input type="password" id="password" name="password" class="form-control" autocomplete="off" value="" />
		</td>
	</tr>
	<?php if($userlevel_id == "-1"): ?>
		<tr>
			<td class="col-md-3">Userlevel / Role:</td>
			<td class="col-md-9">
				<select class="form-control" id="userlevel_id" name="userlevel_id" <?php #echo ($results->userlevel_id == "-1") ? "disabled" : ""; ?> required >
					<option value="">- Select -</option>
					<?php foreach($userlevels as $userlevel): ?>
						<option value="<?php echo $userlevel["id"]; ?>" <?php echo ($results->userlevel_id == $userlevel["id"]) ? "selected" : ""; ?>><?php echo $userlevel["userlevel_name"]; ?></option>
					<?php endforeach; ?>
				</select>
			</td>
		</tr>
	<?php else: ?>
		<tr>
			<td class="col-md-3">Userlevel / Role:</td>
			<td class="col-md-9">
				<?php echo $results->userlevel_name; ?>
			</td>
		</tr>
	<?php endif; ?>
</table>
<?php echo  form_close(); ?>