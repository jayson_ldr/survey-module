<div style="margin: 20px 0 10px 0">
	<div class="col-md-6">
		<div>
			<h3><?php echo $page_title.": ".$total; ?></h3>
		</div>
	</div>
	<?php if($security->CanAdd):?>
		<div class="col-md-6">
			<div class="input-group">
				<?php echo form_open(base_url("admin/".$module."/index"), array( 'class' => 'form-search', 'role' => 'form' ) ); ?>
					<input type="text" class="form-control" placeholder="Keyword" id="term" name="term" style="border-bottom-right-radius: 0; border-top-right-radius: 0;" />
				<?php echo  form_close(); ?>
				
				<span class="input-group-btn">
					<button type="button" class="btn btn-secondary btn_search_action">
						Search
					</button>
					<a href="<?php echo base_url("admin/".$module); ?>" class="btn btn-secondary">
						Reset
					</a>
					<button type="button" class="btn btn-info pull-right" data-action="add" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal">
						Create New
					</button>
				</span>
			</div>
		</div>
	<?php endif; ?>
	<div class="clearfix"></div>
</div>

<table class="table table-striped table-hover">
	<thead class="thead-inverse">
		<tr>
			<th>
				<?php echo set_url("admin/".$module, "ID", "id", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "First Name", "first_name", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Last Name", "last_name", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Active", "is_active", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url("admin/".$module, "Date Last Login", "last_login", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th style="text-align: right !important">Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if($results):
				foreach($results as $rec): ?>
					<tr>
						<td><?php echo $rec->id; ?></td>
						<td><?php echo $rec->first_name; ?></td>
						<td><?php echo $rec->last_name; ?></td>
						<td><?php echo ($rec->is_active) ? "Yes" : "No"; ?></td>
						<td><?php echo ($rec->last_login) ? date("M d, Y H:i", strtotime($rec->last_login)) : ""; ?></td>
						<td style="text-align: right !important">
							<div class="btn-group" role="group" aria-label="Basic example">
								<?php if($security->CanView):?>
									<button type="button" data-action="view" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal" class="btn btn-sm btn-info" data-id="<?php echo $rec->id?>">
										View
									</button>
								<?php endif; ?>
								<?php if($security->CanEdit):?>
									<button type="button" data-action="edit" data-controller="<?php echo "admin/".$module; ?>" data-toggle="modal" data-target="#dynamicModal" class="btn btn-sm btn-warning" data-id="<?php echo $rec->id?>">
										Edit
									</button>
								<?php endif; ?>
								<?php if($security->CanDelete):?>
									<button type="button" data-controller="<?php echo "admin/".$module; ?>" class="btn btn-sm btn-danger btn_delete_action" data-id="<?php echo $rec->id?>">
										Delete
									</button>
								<?php endif; ?>
							</div>
						</td>
					</tr>
		<?php
				endforeach;
			else:
		?>
			<tr>
				<td colspan="6">No record found!</td>
			</tr>
		<?php endif; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
				<div id="pagination">
					<?php echo $links; ?>
				</div>
			</td>
		</tr>
	</tfoot>
</table>