<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/signin.css'; ?>">

<div style="padding-top: 30px;">
	<div class="login-form-left col-md-7" style="border-right: 1px solid #ccc;">
		<?php echo form_open('access/register', array( 'class' => 'form-signup', 'role' => 'form' ) ); ?>
			
			<?php if( $this->session->flashdata('reg_message') ): ?>
				<div class="alert alert-success a-float" style="top: 2%; left: 0px;">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('reg_message'); ?>
				</div>
			<?php endif; ?>
			
			<h2 class="form-signin-heading" style="margin: 15px 0">[Register]</h2>
			<hr>

			<div class="form-group">
				<div class="col-xs-6">
					<label for="first_name">First Name:* </label>
					<input type="text" class="form-control" id="first_name" name="first_name" required />
				</div>
				<div class="col-xs-6">
					<label for="last_name">Last Name:*</label>
					<input type="text" class="form-control" id="last_name" name="last_name" required />
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
					<label for="last_name">Email:*</label>
					<input type="email" class="form-control" id="email" name="email" required />
				</div>
				<div class="col-xs-6">
					<label for="last_name">Contact:</label>
					<input type="text" class="form-control" id="contact" name="contact" />
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="form-group">
				<div class="col-xs-6">
					<label for="last_name">Username:*</label>
					<input type="text" class="form-control" id="username" name="username" required />
				</div>
				<div class="col-xs-6">
					<label for="last_name">Password:*</label>
					<input type="password" class="form-control" id="password" name="password" required />
				</div>
				<div class="clearfix"></div>
			</div>
		
			<!--<table class="table">
				<tr>
					<td class="col-md-2" align="right">First Name:* </td>
					<td class="col-md-4">
						<input type="text" class="form-control" id="first_name" name="first_name" required />
					</td>
					<td class="col-md-2" align="right">Last Name:* </td>
					<td class="col-md-4">
						<input type="text" class="form-control" id="last_name" name="last_name" required />
					</td>
				</tr>
				<tr>
					<td class="col-md-2" align="right">Email:* </td>
					<td class="col-md-4">
						<input type="email" class="form-control" id="email" name="email" required />
					</td>
					<td class="col-md-2" align="right">Contact: </td>
					<td class="col-md-4">
						<input type="text" class="form-control" id="contact" name="contact" />
					</td>
				</tr>
				<tr>
					<td class="col-md-2" align="right">Username:* </td>
					<td class="col-md-4">
						<input type="text" class="form-control" id="username" name="username" required />
					</td>
					<td class="col-md-2" align="right">Password:* </td>
					<td class="col-md-4">
						<input type="password" class="form-control" id="password" name="password" required />
					</td>
				</tr>
			</table>-->
			<div class="col-md-6" style="margin-top: 10px">
				<button class="btn btn-lg btn-info btn-block" type="submit">Sign up</button>
			</div>
		<?php echo  form_close(); ?>
	</div>
	<div class="login-form-right col-md-5">
		<?php echo form_open('access/login', array( 'class' => 'form-signin', 'role' => 'form' ) ); ?>
			<?php if( $this->session->flashdata('message') ): ?>
				<div class="alert alert-success a-float" style="top: 2%; left: 0px;">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $message; ?>
				</div>
			<?php endif; ?>
		
			<?php if( $this->session->flashdata('error') ): ?>
				<div class="alert alert-danger a-float" style="top: 2%; left: 0px;">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php endif; ?>
				
			<?php if( !isset( $error ) && !empty( $error)  ): ?>
				<div class="alert alert-danger a-float" style="top: 2%; left: 0px;">
					<a class="close" data-dismiss="alert">×</a>
					<?php echo $error; ?>
				</div>
			<?php endif; ?>
	
			<h2 class="form-signin-heading">[Login]</h2>
			<hr>
		
			<label for="inputEmail" class="sr-only">Email address</label>
			<input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Username" required autofocus>
		
			<label for="inputPassword" class="sr-only">Password</label>
			<input type="password" class="form-control" name="password" id="password" autocomplete="off" placeholder="Password" required>
		
			<div class="checkbox">
				<label>
					<input type="checkbox" value="remember-me" name="remember" id="remember"> Remember me
				</label>
			</div>
		
			<button class="btn btn-lg btn-info btn-block" type="submit">Sign in</button>
			<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
		<?php echo  form_close(); ?>
	</div>
</div>