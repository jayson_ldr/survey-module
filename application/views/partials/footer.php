	</div>

	<footer class="footer">
		<div class="container">
			<span class="text-muted">
				Survey App
			</span>
		</div>
	</footer>


	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php #echo base_url().'assets/js/jquery.min.js'; ?>"></script> 
	<script src="<?php echo base_url().'assets/js/jquery-1.9.1.min.js'; ?>"></script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script> -->

    <script src="<?php echo base_url().'assets/js/tether.min.js'; ?>" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>
    
	<script src="<?php echo base_url().'assets/js/bootstrap.min.js'; ?>"></script>
	<script src="<?php echo base_url().'assets/js/bootstrap-datepicker.js'; ?>"></script>
	<!-- <script src="<?php echo base_url().'assets/js/jquery.session.js'; ?>"></script> -->
	<script src="<?php echo base_url().'assets/js/jquery.validate.js'; ?>"></script>
	<script src="<?php echo base_url().'assets/js/additional-methods.js'; ?>"></script>
	<script src="<?php echo base_url().'assets/js/sweetalert-dev.js'; ?>"></script>
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="<?php echo base_url().'assets/js/ie10-viewport-bug-workaround.js'; ?>"></script>

	<script src="<?php echo base_url().'assets/js/jquery.cslide.js'; ?>"></script>
	<script type="text/javascript">
		$(function () {
			$("#cslide-slides").cslide();
			$("select").select2({
				placeholder: "- Select -",
				allowClear: true
			});
		})
	</script>
	<script src="<?php echo base_url().'assets/js/select2.full.min.js'; ?>"></script>

	<?php
		$cur_nav = $this->uri->segment(1);
		if($cur_nav == "admin"):
	?>
		<script src="<?php echo base_url().'assets/js/tinymce/tinymce.min.js'; ?>"></script>	
		<script src="<?php echo base_url().'assets/js/Chart.bundle.js'; ?>"></script>
		<script src="<?php echo base_url().'assets/js/admin.js'; ?>"></script>
	<?php
		endif;
	?>
	
	<script src="<?php echo base_url().'assets/js/scripts.js'; ?>"></script>
	<script src="<?php echo base_url().'assets/js/survey_scripts.js'; ?>"></script>

</body>
</html>