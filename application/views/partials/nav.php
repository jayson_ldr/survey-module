<div class="pos-f-t">
	<div class="collapse" id="navbar-header">
		<div class="container bg-inverse p-a-1">
			<h3>Collapsed content</h3>
			<p>Toggleable via the navbar brand.</p>
		</div>
	</div>
	<nav class="navbar navbar-light bg-faded">
		<div class="container">
			<button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
			&#9776;
			</button>
			<div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
				<a class="navbar-brand" href="<?php echo base_url(); ?>">[Survey]</a>
				
				<?php
					$cur_nav 	= $this->uri->segment(1);
					$nav_active	= "<span class='sr-only'>(current)</span>";
				?>
				
				<ul class="nav navbar-nav">
					<?php if(!$user): ?>

						<li class="nav-item <?php echo ($cur_nav == "surveys") ? "active" : ""; ?>">
							<a class="nav-link" href="<?php echo base_url("surveys"); ?>">
								Available Surveys
								<?php echo ($cur_nav == "surveys") ? $nav_active : ""; ?>
							</a>
						</li>
						<li class="nav-item <?php echo ($cur_nav == "access") ? "active" : ""; ?>">
							<a class="nav-link" href="<?php echo base_url("access/login"); ?>">
								Login
								<?php echo ($cur_nav == "access") ? $nav_active : ""; ?>
							</a>
						</li>

					<?php
						else:
							$cur_nav = $this->uri->segment(2);
					?>
						<?php if($security->CanView_Surveys): ?>
							<li class="nav-item <?php echo ($cur_nav == "surveys") ? "active" : ""; ?>">
								<a class="nav-link" href="<?php echo base_url("admin/surveys"); ?>">
									<?php echo ($userlevel_id == "-1") ? "Surveys" : "My Surveys"; ?>
									<?php echo ($cur_nav == "surveys") ? $nav_active : ""; ?>
								</a>
							</li>
						<?php endif; ?>
                        <?php if($security->CanView_Respondents):?>
							<li class="nav-item <?php echo ($cur_nav == "respondents") ? "active" : ""; ?>">
								<a class="nav-link" href="<?php echo base_url("admin/respondents"); ?>">
									Respondents
									<?php echo ($cur_nav == "respondents") ? $nav_active : ""; ?>
								</a>
							</li>
						<?php endif; ?>
						<?php if($security->CanView_Reports):?>
							<li class="nav-item <?php echo (($cur_nav == "stats") || ($cur_nav == "reports")) ? "active" : ""; ?>">
								<a class="nav-link" href="<?php echo base_url("admin/reports"); ?>">
									Reports
									<?php echo (($cur_nav == "stats") || ($cur_nav == "reports")) ? $nav_active : ""; ?>
								</a>
							</li>
						<?php endif; ?>
						<?php if($security->CanView_Users):?>
						<li class="nav-item <?php echo ($cur_nav == "users") ? "active" : ""; ?>">
							<a class="nav-link" href="<?php echo base_url("admin/users"); ?>">
								<?php echo ($userlevel_id == "-1") ? "Users" : "My Account"; ?>
								<?php echo ($cur_nav == "users") ? $nav_active : ""; ?>
							</a>
						</li>
						<?php endif; ?>
						<li class="nav-item dropdown <?php echo (($cur_nav == "userlevels") || ($cur_nav == "fields")) ? "active" : ""; ?>"">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Settings
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								<?php if($security->CanView_Userlevels):?>
									<a class="dropdown-item" href="<?php echo base_url("admin/userlevels"); ?>">
										Userlevels / Roles
										<?php echo ($cur_nav == "userlevels") ? $nav_active : ""; ?>
									</a>
								<?php endif; ?>
								<?php if($security->CanView_Dynamic_Fields):?>
									<a class="dropdown-item" href="<?php echo base_url("admin/fields"); ?>">
										Fields
										<?php echo ($cur_nav == "fields") ? $nav_active : ""; ?>
									</a>
								<?php endif; ?>
								<a class="dropdown-item" href="<?php echo base_url("access/logout"); ?>">
									Logout
									<?php echo ($cur_nav == "access") ? $nav_active : ""; ?>
								</a>
							</div>
						</li>
							
					<?php endif; ?>
					
				</ul>
			</div>
		</div>
	</nav>
</div>