<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Google Tag Manager -->
	<!-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M5NDWT');</script> -->
	<!-- End Google Tag Manager -->

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">
	
	<title>Survey App</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/sticky-footer-navbar.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/styles.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/sweetalert.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/cslide-style.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/select2.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-datepicker.css'; ?>">

	<script src="<?php echo base_url().'assets/js/respond.src.js'; ?>"></script>
	
	<script type="text/javascript">
		var base_url = '<?php echo base_url();?>';

		/* GA code */
		/*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-84774104-1', 'auto');
		ga('send', 'pageview');*/
	</script>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M5NDWT"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
	<!-- End Google Tag Manager (noscript) -->

	<!-- Fixed navbar -->
	<?php $this->load->view('partials/nav'); ?>

	<!-- Begin page content -->
	<div class="container">

		<!-- Flash Alerts -->
		<?php if( isset($error) ||$this->session->flashdata('message') || $this->session->flashdata('error')): ?>
			<?php
                //lets have the flashdata overright "$message" if it exists
                if($this->session->flashdata('message')){
                    $message	= $this->session->flashdata('message');
                }
                
                if($this->session->flashdata('error')){
                    $error	= $this->session->flashdata('error');
                }
                
                if(function_exists('validation_errors') && validation_errors() != ''){
                    $error	= validation_errors();
                }
            ?>        
            <?php if (!empty($message)): ?>
                <div class="alert alert-success a-float">
                    <a class="close" data-dismiss="alert">×</a>
                    <?php echo $message; ?>
                </div>
            <?php endif; ?>
        
            <?php if (!empty($error)): ?>
                <div class="alert alert-danger a-float">
                    <a class="close" data-dismiss="alert">×</a>
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
	
		<!-- Modal -->
		<div class="modal fade" id="dynamicModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Information:</h4>
					</div>
					
					<div class="modal-body"></div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary btn_save_action" data-controller="" data-mode="">Save</button>
					</div>
				</div>
			</div>
		</div>