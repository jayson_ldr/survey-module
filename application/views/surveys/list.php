<div style="margin: 20px 0 10px 0">
	<div class="col-md-6">
		<div>
			<h3><?php echo $page_title.": ".$total; ?></h3>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<table class="table table-striped table-hover">
	<thead class="thead-inverse">
		<tr>
			<th>
				<?php echo set_url($module, "ID", "id", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th>
				<?php echo set_url($module, "Survey Title", "survey_title", $sort_order, $code, $page, $sort_by); ?>
			</th>
			<th class="tbl_hide_element" style="text-align: center !important;">
				<?php #echo set_url($module, "Respondents", "total_res", $sort_order, $code, $page, $sort_by); ?>
				Quota
			</th>
			<th style="text-align: right !important">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if($results):
				foreach($results as $rec): ?>
					<tr>
						<td><?php echo $rec->id; ?></td>
						<td><?php echo $rec->survey_title; ?></td>
						<td class="tbl_hide_element" align="center">
							<?php #echo $rec->total_res; ?>
							<?php echo $rec->total_res." / ".$rec->quota; ?>
						</td>
						<td style="text-align: right !important">
							<?php if($rec->total_res == $rec->quota): ?>
								<span class="btn btn-sm btn-warning" style="cursor: not-allowed !important;">
									Close
								</span>
							<?php else: ?>
								<!-- <a href="<?php echo base_url("surveys/process/".$rec->id); ?>" class="btn btn-sm btn-info process_survey">
									Try
								</a> -->
								<a href="<?php echo base_url("survey/".$rec->survey_title_slug); ?>" class="btn btn-sm btn-info process_survey">
									Try
								</a>
							<?php endif; ?>
						</td>
					</tr>
		<?php
				endforeach;
			else:
		?>
			<tr>
				<td colspan="4">No record found!</td>
			</tr>
		<?php endif; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">
				<div id="pagination">
					<?php echo $links; ?>
				</div>
			</td>
		</tr>
	</tfoot>
</table>