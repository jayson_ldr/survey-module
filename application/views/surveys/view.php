<table class="table table-sm table-striped">
	<tr>
		<td class="col-md-3">Room Category ID:</td>
		<td class="col-md-9"><?php echo $results->id; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Room Category Name:</td>
		<td class="col-md-9"><?php echo $results->category; ?></td>
	</tr>
	<tr>
		<td class="col-md-3">Rate / night:</td>
		<td class="col-md-9"><?php echo $results->rate; ?></td>
	</tr>
</table>