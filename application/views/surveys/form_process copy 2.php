<style>
.custom_input{width: inherit ! important; display: inline;}
.panel-heading {
    background-color: #daeeff;
    border: 1px solid #cadeef;
    border-radius: 5px;
    margin-bottom: 2px;
    padding: 10px 10px 5px;
}
.panel-collapse {
    background: #f9f9f9 none repeat scroll 0 0;
    padding: 10px;
}
.btn-cslide{
	border-color: #38678f !important;
    border-style: solid !important;
    border-width: 1px 1px 5px 1px !important;
}
.q_notes{color: #fff; font-size: 20px}
.cslide-link, .cslide-next-skip, .cslide-next-info{
    background: steelblue none repeat scroll 0 0;
    border-bottom: 5px solid #38678f;
    color: #fff;
    cursor: pointer;
    display: inline-block;
    padding: 10px 20px;
}
.select2.select2-container{width: 100% !important;}
</style>

<div class="page_title" style="margin: 20px 0 10px 0">
	<div>
		<h3><?php echo $page_title; ?></h3>
	</div>
</div>

<?php echo form_open('', array( 'class' => 'form-process', 'role' => 'form' ) ); ?>
<table class="table tbl_survey_info">
	<tr>
		<td class="col-md-3"><b>Survey Title:</b></td>
		<td class="col-md-9">
			<?php echo $survey["survey_title"]; ?>
			<input type="hidden" id="survey_id" name="survey_id" value="<?php echo $survey["id"]; ?>" />
		</td>
	</tr>
	<tr>
		<td class="col-md-3"><b>Total Quesitons:</b></td>
		<td class="col-md-9">
			<?php echo count($survey["question"]); ?>
		</td>
	</tr>
</table>

<div class="question_answer_panel" style="margin-bottom: 10px">
	
	<div class="container">
        <section id="cslide-slides" class="cslide-slides-master clearfix">
            <div class="cslide-slides-container clearfix">

                <div class="cslide-slide">
                    <div class="col-md-12">
                        <p>
                            Kindly fillup the information below before proceeding to the survey. Thank you!
                        </p>

                        <?php
                            if($respondent_fields):
                            foreach($respondent_fields as $index => $field):
                                $type = (($field->type == "textbox") && ($field->data_type == "character")) ? "text" : "number";
                                $class = (($field->type == "textbox") && ($field->data_type == "date")) ? "date" : "";
                        ?>
                            <?php if($field->type == "textbox"): ?>

                                <div class="form-group row">
                                    <label class="col-xs-3 col-form-label" style="color: white"><b><?php echo $field->name; ?>: </b></label>
                                    <div class="col-xs-9">
                                        <input type="<?php echo $type?>" name="fields[<?php echo $field->slug; ?>]" class="form-control <?php echo $class?>" required>
                                    </div>
                                </div>

                            <?php elseif($field->type == "select"): ?>

                                <div class="form-group row">
                                    <label class="col-xs-3 col-form-label" style="color: white"><b><?php echo $field->name; ?>: </b></label>
                                    <div class="col-xs-9">
                                        <select name="fields[<?php echo $field->slug; ?>]" class="form-control" required>
                                            <option value="">- Select -</option>
                                            <?php foreach($field->content as $val): ?>
                                                <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                            <?php endif; ?>
                        <?php
                            endforeach;
                            endif;
                        ?>

                        <!--<div class="form-group row">
                            <label class="col-xs-3 col-form-label" style="color: white"><b>Name: </b></label>
                            <div class="col-xs-9">
                                <input type="text" id="name" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xs-3 col-form-label" style="color: white"><b>Age: </b></label>
                            <div class="col-xs-9">
                                <input type="number" id="age" name="age" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xs-3 col-form-label" style="color: white"><b>Gender: </b></label>
                            <div class="col-xs-9">
                                <select id="gender" name="gender" class="form-control" required>
                                    <option value="">- Select -</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>-->

                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="cslide-prev-next clearfix" style="margin: 10px 0 0 ">
                        <span class="cslide-next-info btn-cslide">Proceed</span>
                    </div>
                </div>

            	<?php foreach($survey["question"] as $key => $q): ?>
	                <div class="cslide-slide">
	                    <div class="q_notes_block">

                    		<?php if($q["type"] == "attempt"): ?>
                    			<h4 class="q_notes">
                    				Attemps: 
                    				<span class="attemp_count_<?php echo $key; ?>">0</span> / 
                    				<span class="attemp_total_<?php echo $key; ?>"><?php echo $q["attempt_no"]; ?></span>
                    			</h4>
                    		<?php elseif($q["type"] == "skip"): ?>
                    			<h4 class="q_notes">
                    				Note: You can optionally skip this question
                    			</h4>
                    		<?php elseif($q["type"] == "force"): ?>
                    			<h4 class="q_notes">
                    				Note: Right answer is required for this question
                    			</h4>
                    		<?php endif; ?>

                    		<input type="hidden" id="type_<?php echo $key; ?>" value="<?php echo $q["type"]; ?>">
                    		<input type="hidden" id="qid_<?php echo $key; ?>" value="<?php echo $q["id"]; ?>">

	                    </div>
	                    <div class="q-title">
	                    	<h2><?php echo $q["question_title"]; ?></h2>
	                    </div>

	                    <div class="clearfix"></div>
                    	<table class="table table-stripped" style="color: rgb(255, 255, 255);">
							<tbody>
								<?php foreach($q["answer"]["details"] as $i => $ans): ?>
									<tr>
										<td>
											<div class="ans_list">
												<input type="radio" class="check_ans" id="respondent_answer_<?php echo $key.$i; ?>" name="respondent_answer[]" value="<?php echo $ans; ?>" data-qindex="<?php echo $key; ?>" data-ansindex="<?php echo $i; ?>" /> 
												
												<label for="respondent_answer_<?php echo $key.$i; ?>">
													<?php echo $ans; ?>
												</label>
											</div>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<input type="hidden" class="form-control" id="right_answer_<?php echo $key; ?>" value="<?php echo $q["answer"]["answer"]; ?>">

						<div class="cslide-prev-next clearfix" style="margin: 10px 0 0 ">
			                <!-- <span class="cslide-prev " style="display: none">prev slide</span> -->

			                <?php if($q["type"] == "skip"): ?>
			                	<span class="cslide-next-skip btn-cslide" data-qindex="<?php echo $key; ?>">Skip</span>
			                <?php endif; ?>

			                <span class="cslide-next btn-cslide" id="btn_cslide_submit_<?php echo $key; ?>" data-qindex="<?php echo $key; ?>">Submit</span>
			            </div>
	                </div>
	            <?php endforeach; ?>

                <div class="cslide-slide">
                    <h2 style="margin-bottom: 20px">Thank you for your time answering the questionnaire.</h2>
                    
                    <div class="col-md-12">
	                    <p>
	                    	Your Score result is:
	                    	<span style="font-size: 20px">
	                    		<span class="final_score">0</span> of <?php echo count($survey["question"]); ?>
	                    	</span>
	                    </p>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="cslide-prev-next clearfix" style="margin: 10px 0 0 ">
		                <a href="#<?php #echo base_url("surveys"); ?>" class="cslide-link btn-cslide" id="fin">Finish</a>
		            </div>
                </div>
            </div>

            

        </section><!-- /sliding content section -->
    </div>

</div>

<?php echo form_close(); ?>