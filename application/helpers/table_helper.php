<?php
function set_url($class, $title, $field, $sort_order, $code, $page, $sort_by)
{
	if($sort_order == "DESC")
	{
		$sort 		= "&uarr;";
		$sort_new 	= "ASC";
	}
	else
	{
		$sort 		= "&darr;";
		$sort_new 	= "DESC";
	}
	
	$uri = "";
	if(isset($code))
	{
		$uri .= $code."/";
	}
	if(isset($page))
	{
		$uri .= $page;
	}
	
	$link = base_url($class."/index/".$field."/".$sort_new."/".$uri."/");
	echo "<a href=".$link.">".$title." ".(($field == $sort_by) ? $sort : "")."</a>";
}