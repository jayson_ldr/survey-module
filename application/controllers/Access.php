<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access extends public_Controller
{
	public $fields = array(	
		array(
			'field'   => 'username', 
			'label'   => 'Username', 
			'rules'   => 'trim|required'
		),
		array(
		
			'field'   => 'password', 
			'label'   => 'Password', 
			'rules'   => 'trim|required'
		),		
	);
	
	function __construct() {
	
		parent::__construct();
		$this->load->model(array('user_model'));
		
	}	
	 
	public function login()
	{
		$post = $this->input->post();
		$this->form_validation->set_rules( $this->fields );
		if(!empty($post)){
		
			if( $this->form_validation->run() ) {
				
				$username	= $this->input->post('username');
				$password	= md5($this->input->post('password'));
				$remember   = $this->input->post('remember');
				$redirect	= $this->input->post('redirect');
				$login		= $this->auth->login_user($username, $password, $remember);
				if ($login){
					$this->user_model->update_logs($login);
					
					if ($redirect == ''){
						$redirect = 'admin/surveys';
					}
					redirect($redirect);
				}else{
					//this adds the redirect back to flash data if they provide an incorrect credentials
					$this->session->set_flashdata('redirect', $redirect);
					$this->session->set_flashdata('error', "Authentication Failed");
					redirect('access/login');
				}
			}
		}	
		
		$data['redirect'] = $this->session->flashdata('redirect');
		$this->template('login', $data);
	}
	
	function register()
	{
		if($post = $this->input->post())
		{
			if($post["password"])
			{
				$post["password"] = md5($post["password"]);
			}

			$post["userlevel_id"] = 2;
			$params["table"]	= "users";
			$params["data"] 	= $post;
			
			$this->user_model->dynamic_save($params);
			
			$this->session->set_flashdata('reg_message', "Registration Successful, you can now login");
			redirect('access/login');
		}
	}
	
	function logout()
	{	
		$user = $this->session->userdata('user');
		$this->session->unset_userdata('user');
		
		#$this->user_model->update_logs($user['user_id']);
		#$this->session->sess_destroy();

		$this->auth->logout();
		
		$this->session->set_flashdata('message', "Logout Successful");		
		redirect('surveys'); //access/login
	}
}