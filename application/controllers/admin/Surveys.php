<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Surveys extends Admin_Controller
{	
	public $folder;
	public $redirect;
	public $module;
	public $security;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('surveys_model'));
		$this->folder = strtolower(get_class($this));
		$this->redirect = "admin/".$this->folder;
		$this->module 	= get_class($this);
		$this->security	= $this->abstracts->set_security_access($this->module);
	}
	
	public function index($sort_by='id',$sort_order='DESC', $code=0, $page=0)
	{
		//IF CAN VIEW
		$data["security"] = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("access/logout");
		}

		$term				= false;
		$param		 		= array();

		if($post = $this->input->post())
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term			= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$param["search"] = $term;

		$user = $this->session->userdata('user');

		if($user["userlevel_id"] != "-1")
		{
			$param["created_by"]	= $user["user_id"];
		}

		$config 				= array();
        $config["base_url"] 	= site_url("admin/".$this->folder.'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config["total_rows"] 	= $this->surveys_model->fetch_record($param, true);
        $config["per_page"] 	= $this->config->item('rows_per_page');
        $config["uri_segment"] 	= 7;
		$config					= $this->paging_style->set_paging_style($config);

        $this->pagination->initialize($config);

		$param["limit"] 	= $config["per_page"];
		$param["offset"] 	= $page;
		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		
        $data["results"] 	= $this->surveys_model->fetch_record($param);
        $data["total"] 		= $config["total_rows"];
		$data["links"] 		= $this->pagination->create_links();
		$data["page"]	 	= $page;
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data["code"]		= $code;
		$data["module"]		= $this->folder;
		$data["page_title"]	= ($user["userlevel_id"] == "-1") ? "All Surveys" : "My Surveys";
		
		if ($this->input->is_ajax_request())
		{
			//IF CAN ADD
			$data["security"] = $this->security;
			if(!$data["security"]->CanAdd)
			{
				$this->session->set_flashdata('error', 'Access Not Allowed!');
				redirect($this->redirect);
			}

			echo $this->partial_view("admin/".$this->folder.'/add');
		}
		else
		{
			$this->template("admin/".$this->folder.'/list', $data);
		}
	}

	public function survey_wizard()
	{
		//IF CAN ADD
		$data["security"] = $this->security;
		if(!$data["security"]->CanAdd)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}
		
		if($post = $this->input->post())
		{
			if($post["survey_title"])
			{
				$survey = array();
				$survey['survey']					= array();
				$survey['survey']['survey_title'] 	= $post["survey_title"];
				$survey['survey']['quota'] 			= $post["quota"];
				$this->session->set_userdata($survey);
			}
		}
		else
		{
			$this->session->unset_userdata('survey');

			$data["module"]		= $this->folder;
			$data["page_title"]	= "Survey Wizard";

			$this->template("admin/".$this->folder.'/survey_wiz', $data);
		}
	}

	public function ajax_form_question()
	{
		$survey 			= array();
		$survey['survey'] 	= array();

		if($post = $this->input->post())
		{
			$temp = $this->session->userdata('survey');

			$survey['survey']['survey_title'] 	= $temp["survey_title"];
			$survey['survey']['quota'] 			= $temp["quota"];
			$survey['survey']["question"]		= (isset($temp["question"])) ? $temp["question"] : array();

			$question 	= array();
			$answer 	= array();
			
			if(isset($post["MultipleChoice"]))
			{
				$answer["details"] = $post["MultipleChoice"];
			}
			if(isset($post["YesNo"]))
			{
				$answer["details"] = $post["YesNo"];
			}
			$answer["ans_type"] = $post["ans_type"];

			$post["question_title"] = htmlentities($post["question_title"], ENT_QUOTES | ENT_IGNORE, "UTF-8");
			
			$question["question_title"] = $post["question_title"];
			$question["type"] 			= $post["type"];
			$question["attempt_no"] 	= (isset($post["attempt_no"])) ? $post["attempt_no"] : "";
			$question["answer"]			= $answer;

			if(isset($survey['survey']["question"]))
			{
				array_push($survey['survey']["question"], $question);
			}
			else
			{
				$survey['survey']["question"][] = $question;
			}

			$this->session->set_userdata($survey);
			pr($this->session->userdata('survey'));
		}
		else
		{
			$data["survey"]		= $this->session->userdata('survey');
			$data["module"]		= $this->folder;
			$data["page_title"]	= "Survey Wizard";

			$this->partial_view("admin/".$this->folder.'/form_question', $data);
		}
	}

	public function ajax_form_preview()
	{
		$data["module"]		= $this->folder;
		$data["page_title"]	= "Survey Wizard";
		$survey 			= array();
		$question 			= array();
		$answer				= array();
		$ses_survey['survey'] 	= array();

		if($post = $this->input->post())
		{
			$ses_survey['survey']['survey_title'] 	= $post["survey_title"];
			$ses_survey['survey']['quota'] 			= $post["quota"];
			$ses_survey['survey']["question"]		= $post["question"];
			$this->session->set_userdata($ses_survey);

			//save survey
			$user = $this->session->userdata('user');
			$survey["created_by"]	= $user["user_id"];
			$survey["survey_title"]	= $post["survey_title"];
			$survey["quota"]		= $post["quota"];
			$params["data"] 		= $survey;
			$params["table"]		= "surveys";
			$survey_id 				= $this->surveys_model->dynamic_save($params);

			if($survey_id)
			{
				foreach($post["question"] as $q)
				{
					$q["question_title"] = htmlentities($q["question_title"], ENT_QUOTES | ENT_IGNORE, "UTF-8");

					//save questions
					$question["survey_id"]		= $survey_id;
					$question["question_title"]	= $q["question_title"];
					$question["type"]			= $q["type"];
					$question["attempt_no"]		= (isset($q["attempt_no"])) ? $q["attempt_no"] : "";
					$question["created_by"]		= $user["user_id"];
					$params2["data"] 			= $question;
					$params2["table"]			= "survey_questions";
					$question_id 				= $this->surveys_model->dynamic_save($params2);

					if($question_id)
					{
						if($ans = $q["answer"])
						{
							//save answer
							$answer["question_id"]	= $question_id;
							$answer["ans_type"]		= $ans["ans_type"];
							$answer["details"]		= json_encode($ans["details"]);
							$answer["answer"]		= $ans["answer"];
							$params3["data"]		= $answer;
							$params3["table"]		= "survey_question_ans";
							$this->surveys_model->dynamic_save($params3);
						}
					}
				}
			}

			$data["survey"] = $this->session->userdata('survey');
			$this->partial_view("admin/".$this->folder.'/form_publish', $data);
		}
		else
		{
			$data["survey"] = $this->session->userdata('survey');
			$this->partial_view("admin/".$this->folder.'/form_preview', $data);
		}
	}
	
	public function view_record($id = null)
	{
		if($post = $this->input->post())
		{
			$data["page_title"]	= "Survey Information - Details";
			$data["results"]	= (object)$this->surveys_model->get_record($post["id"]);
			$data["survey"]["question"]	= (array)$this->surveys_model->dynamic_get_record("survey_id", $post["id"], "survey_questions");

			$ans 		= array();
			$arr_ans 	= array();
			if(count($data["survey"]["question"]) > 0)
			{
				foreach($data["survey"]["question"] as $key => $q)
				{
					$ans = $this->surveys_model->dynamic_get_record("question_id", $q["id"], "survey_question_ans");
					if(count($ans) > 0)
					{
						$ans = $ans[0];
						$arr_ans["answer_id"]	= $ans["id"];
						$arr_ans["ans_type"] 	= $ans["ans_type"];
						$arr_ans["details"] 	= json_decode($ans["details"]);
						$arr_ans["answer"] 		= $ans["answer"];

						$data["survey"]["question"][$key]["answer"] = $arr_ans;
					}
				}
			}

			if($post["mode"] == "view")
			{
				//IF CAN VIEW
				$data["security"] = $this->security;
				if(!$data["security"]->CanView)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect("admin/surveys");
				}

				echo $this->partial_view("admin/".$this->folder.'/view', $data);
			}
			else
			{
				//IF CAN EDIT
				$data["security"] = $this->security;
				if(!$data["security"]->CanEdit)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect($this->redirect);
				}

				echo $this->partial_view("admin/".$this->folder.'/edit', $data);
			}
		}
	}
	
	public function save_record()
	{
		if($post = $this->input->post())
		{
			if($post["survey_title"])
			{
				$post["survey_title_slug"] = str_replace(" ", "_", strtolower($post["survey_title"]));
			}

			$questions = $post["question"];
			unset($post["question"]);

			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$this->surveys_model->dynamic_save($params);

			$survey_id = $post["id"];
			if($survey_id)
			{
				foreach($questions as $q)
				{
					$q["question_title"] = htmlentities($q["question_title"], ENT_QUOTES | ENT_IGNORE, "UTF-8");

					//update questions
					$question["id"]				= $q["question_id"];
					$question["survey_id"]		= $survey_id;
					$question["question_title"]	= $q["question_title"];
					$question["type"]			= $q["type"];
					$question["attempt_no"]		= (isset($q["attempt_no"])) ? $q["attempt_no"] : "";
					$question["created_by"]		= $user["user_id"];
					$params2["data"] 			= $question;
					$params2["table"]			= "survey_questions";
					$this->surveys_model->dynamic_save($params2);

					if($q["question_id"])
					{
						if($ans = $q["answer"])
						{
							//save answer
							$answer["id"]			= $ans["answer_id"];
							$answer["question_id"]	= $q["question_id"];
							$answer["ans_type"]		= $ans["ans_type"];
							$answer["details"]		= json_encode($ans["details"]);
							$answer["answer"]		= $ans["answer"];
							$params3["data"]		= $answer;
							$params3["table"]		= "survey_question_ans";
							$this->surveys_model->dynamic_save($params3);
						}
					}
				}
			}//end if

		}
	}
	
	public function delete_record()
	{
		//IF CAN DELETE
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}

		if($post = $this->input->post())
		{
			$condition["id"]	= $post["id"];
			$params["where"] 	= $condition;
			$params["table"] 	= $this->folder;
			
			$this->surveys_model->dynamic_remove($params);
		}
	}
}
