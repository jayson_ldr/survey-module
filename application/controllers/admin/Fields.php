<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Fields extends Admin_Controller
{	
	public $folder;
	public $types 		= array("textbox","select");
	public $data_types 	= array("integer","character","date");
	public $redirect;
	public $module;
	public $security;

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('fields_model'));
		$this->folder = strtolower(get_class($this));
		$this->redirect = "admin/".$this->folder;
		$this->module 	= 'Dynamic Fields';
		$this->security	= $this->abstracts->set_security_access($this->module);
	}
	
	public function index($sort_by='id',$sort_order='DESC', $code=0, $page=0)
	{
		//IF CAN VIEW
		$data["security"] 			 = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("admin/surveys");
		}

		$term				= false;
		$param		 		= array();

		if($post = $this->input->post())
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term			= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$param["search"] 		= $term;
		$user 					= $this->session->userdata('user');
		$data["userlevel_id"]	= $user["userlevel_id"];

		$config 				= array();
        $config["base_url"] 	= site_url("admin/".$this->folder.'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config["total_rows"] 	= $this->fields_model->fetch_record($param, true);
        $config["per_page"] 	= $this->config->item('rows_per_page');
        $config["uri_segment"] 	= 7;
		$config					= $this->paging_style->set_paging_style($config);

        $this->pagination->initialize($config);

		$param["limit"] 	= $config["per_page"];
		$param["offset"] 	= $page;
		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		
        $data["results"] 	= $this->fields_model->fetch_record($param);
        $data["total"] 		= $config["total_rows"];
		$data["links"] 		= $this->pagination->create_links();
		$data["page"]	 	= $page;
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data["code"]		= $code;
		$data["module"]		= $this->folder;
		$data["types"]		= $this->types;
		$data["data_types"]	= $this->data_types;
		$data["page_title"]	= "Respondents Fields";
		
		if ($this->input->is_ajax_request())
		{
			//IF CAN ADD
			$data["security"] = $this->security;
			if(!$data["security"]->CanAdd)
			{
				$this->session->set_flashdata('error', 'Access Not Allowed!');
				redirect($this->redirect);
			}

			echo $this->partial_view("admin/".$this->folder.'/add', $data);
		}
		else
		{
			$this->template("admin/".$this->folder.'/list', $data);
		}
	}
	
	public function view_record($id = null)
	{
		if($post = $this->input->post())
		{
			$data["page_title"]	= "Role Information - Details";
			$record				= (object)$this->fields_model->get_record($post["id"]);
			$record->content 	= json_decode($record->content);
			$data["results"]	= $record;

			if($post["mode"] == "view")
			{
				//IF CAN VIEW
				$data["security"] = $this->security;
				if(!$data["security"]->CanView)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect("admin/surveys");
				}

				echo $this->partial_view("admin/".$this->folder.'/view', $data);
			}
			else
			{
				//IF CAN EDIT
				$data["security"] = $this->security;
				if(!$data["security"]->CanEdit)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect($this->redirect);
				}

				echo $this->partial_view("admin/".$this->folder.'/edit', $data);
			}
		}
	}
	
	public function save_record()
	{
		if($post = $this->input->post())
		{
			if($post["name"])
			{
				$post["slug"] = str_replace(" ", "_", strtolower($post["name"]));
			}
			
			if(isset($post["content"]))
			{
				$post["content"]	= json_encode($post["content"]);
			}
			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$this->fields_model->dynamic_save($params);
		}
	}
	
	public function delete_record()
	{
		//IF CAN DELETE
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}

		if($post = $this->input->post())
		{
			$condition["id"]	= $post["id"];
			$params["where"] 	= $condition;
			$params["table"] 	= $this->folder;
			
			$this->fields_model->dynamic_remove($params);
		}
	}
}
