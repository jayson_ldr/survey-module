<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Userlevels extends Admin_Controller
{	
	public $folder;
	public $redirect;
	public $module;
	public $security;
	public $permission_module = "Userlevel Permissions";
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('userlevels_model'));
		$this->folder	= strtolower(get_class($this));
		$this->redirect = "admin/".$this->folder;
		$this->module 	= get_class($this);
		$this->security	= $this->abstracts->set_security_access($this->module);
	}
	
	public function index($sort_by='id',$sort_order='DESC', $code=0, $page=0)
	{
		//IF CAN VIEW
		$data["security"] 			 = $this->security;
		$data["security_permission"] = $this->abstracts->set_security_access($this->permission_module);
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("admin/surveys");
		}

		$term				= false;
		$param		 		= array();

		if($post = $this->input->post())
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term			= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$param["search"] 		= $term;
		$user 					= $this->session->userdata('user');
		$data["userlevel_id"]	= $user["userlevel_id"];

		$config 				= array();
        $config["base_url"] 	= site_url("admin/".$this->folder.'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config["total_rows"] 	= $this->userlevels_model->fetch_record($param, true);
        $config["per_page"] 	= $this->config->item('rows_per_page');
        $config["uri_segment"] 	= 7;
		$config					= $this->paging_style->set_paging_style($config);

        $this->pagination->initialize($config);

		$param["limit"] 	= $config["per_page"];
		$param["offset"] 	= $page;
		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		
        $data["results"] 	= $this->userlevels_model->fetch_record($param);
        $data["total"] 		= $config["total_rows"];
		$data["links"] 		= $this->pagination->create_links();
		$data["page"]	 	= $page;
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data["code"]		= $code;
		$data["module"]		= $this->folder;
		$data["page_title"]	= "All Levels / Roles";
		
		if ($this->input->is_ajax_request())
		{
			//IF CAN ADD
			$data["security"] = $this->security;
			if(!$data["security"]->CanAdd)
			{
				$this->session->set_flashdata('error', 'Access Not Allowed!');
				redirect($this->redirect);
			}

			echo $this->partial_view("admin/".$this->folder.'/add', $data);
		}
		else
		{
			$this->template("admin/".$this->folder.'/list', $data);
		}
	}
	
	public function view_record($id = null)
	{
		if($post = $this->input->post())
		{
			$data["page_title"]	= "Userlevel Information - Details";
			$data["results"]	= (object)$this->userlevels_model->get_record($post["id"]);
			
			if($post["mode"] == "view")
			{
				//IF CAN VIEW
				$data["security"] = $this->security;
				if(!$data["security"]->CanView)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect("admin/surveys");
				}
				
				echo $this->partial_view("admin/".$this->folder.'/view', $data);
			}
			else
			{
				//IF CAN EDIT
				$data["security"] = $this->security;
				if(!$data["security"]->CanEdit)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect($this->redirect);
				}

				echo $this->partial_view("admin/".$this->folder.'/edit', $data);
			}
		}
	}
	
	public function save_record()
	{
		if($post = $this->input->post())
		{
			$post["slug"] = str_replace(" ", "_", strtolower($post["userlevel_name"]));

			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$this->userlevels_model->dynamic_save($params);
		}
	}
	
	public function delete_record()
	{
		//IF CAN DELETE
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}

		if($post = $this->input->post())
		{
			$condition["id"]	= $post["id"];
			$params["where"] 	= $condition;
			$params["table"] 	= $this->folder;
			
			$this->userlevels_model->dynamic_remove($params);
		}
	}

	function permissions($userlevel_id = false, $userlevel_name = false)
	{	
		//validate access if allowed
		$data["security"] = $this->abstracts->set_security_access($this->permission_module);
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}
		
		$data["USER_LEVEL_TABLE_NAME"] = $this->access->DynamicTables();
		
		if(!$data["USER_LEVEL_TABLE_NAME"])
		{
			$this->session->set_flashdata('error', 'No tables generated');
			redirect($this->redirect);
		}
	
		if($post = $this->input->post())
		{
			// Get fields from form
			$this->access->setFormValue($post["userlevel_id"]);
			for ($i = 0; $i < count($data["USER_LEVEL_TABLE_NAME"]); $i++) {
				$arPriv[$i] = intval(@$post["Add_" . $i]) + intval(@$post["Delete_" . $i]) + intval(@$post["Edit_" . $i]) + intval(@$post["View_" . $i]);
			}
			
			for ($i = 0; $i < count($data["USER_LEVEL_TABLE_NAME"]); $i++) {
				
				$TABLE_NAME_FIELD 		= $this->access->AdjustSql($data["USER_LEVEL_TABLE_NAME"][$i]);
				$USER_LEVEL_ID_FIELD	= $post["userlevel_id"];
				$PRIV_FIELD				= $arPriv[$i];
				$this->access->proccess($TABLE_NAME_FIELD, $USER_LEVEL_ID_FIELD, $PRIV_FIELD);
			}
			
			$this->session->set_flashdata('message', 'Userlevel Permissions Successfully Setup!');
			redirect($this->redirect."/permissions/".$post["userlevel_id"]."/".$post["userlevel_name"]);
		}
		
		if(!$userlevel_id)
		{
			$this->session->set_flashdata('error', 'No User Level ID Provided!');
			redirect($this->redirect);
		}
		else
		{
			$this->access->setQueryStringValue($userlevel_id);
		}
		
		$name = ($userlevel_name) ? $userlevel_name : "";
		$data["page_title"] 	= 'User Level Permission for: "'.$name.'"';
		$data["userlevel_id"] 	= $userlevel_id;
		$data["userlevel_name"] = $userlevel_name;
		$record					= $this->userlevels_model->fetch($userlevel_id);
		$data["record"] 		= $record[0]; 
		$data["arPriv"] 		= $this->access->InitArray(count($data["USER_LEVEL_TABLE_NAME"]), 0);
		
		if ($this->access->QueryStringValue == "-1") {
			$data["sDisabled"] = " disabled=\"true\"";
		} else {
			$data["sDisabled"] = "";
		}

		$data["module"] = $this->folder;
		
		$this->access->SetUpUserLevelEx($this->access->CurrentValue);
		
		$this->template("admin/".$this->folder.'/permissions_form', $data); 
	}
}
