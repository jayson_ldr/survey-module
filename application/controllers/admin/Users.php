<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_Controller
{	
	public $folder;
	public $redirect;
	public $module;
	public $security;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('user_model'));
		$this->folder = strtolower(get_class($this));
		$this->redirect = "admin/".$this->folder;
		$this->module 	= get_class($this);
		$this->security	= $this->abstracts->set_security_access($this->module);
	}
	
	public function index($sort_by='id',$sort_order='DESC', $code=0, $page=0)
	{
		//IF CAN VIEW
		$data["security"] = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("access/logout");
		}

		$term				= false;
		$param		 		= array();

		if($post = $this->input->post())
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term			= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$param["search"] = $term;

		$user = $this->session->userdata('user');
		
		if($user["userlevel_id"] != "-1")
		{
			$param["id"] = $user["user_id"];
		}

		$config 				= array();
        $config["base_url"] 	= site_url("admin/".$this->folder.'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config["total_rows"] 	= $this->user_model->fetch_record($param, true);
        $config["per_page"] 	= $this->config->item('rows_per_page');
        $config["uri_segment"] 	= 7;
		$config					= $this->paging_style->set_paging_style($config);

        $this->pagination->initialize($config);

		$param["limit"] 	= $config["per_page"];
		$param["offset"] 	= $page;
		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		
        $data["results"] 	= $this->user_model->fetch_record($param);
        $data["userlevels"]	= $this->user_model->fetch_userlevels();
        $data["total"] 		= $config["total_rows"];
		$data["links"] 		= $this->pagination->create_links();
		$data["page"]	 	= $page;
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data["code"]		= $code;
		$data["module"]		= $this->folder;
		$data["page_title"]	= ($user["userlevel_id"] == "-1") ? "All Users" : "My Account";
		
		if ($this->input->is_ajax_request())
		{
			//IF CAN ADD
			$data["security"] = $this->security;
			if(!$data["security"]->CanAdd)
			{
				$this->session->set_flashdata('error', 'Access Not Allowed!');
				redirect($this->redirect);
			}

			echo $this->partial_view("admin/".$this->folder.'/add', $data);
		}
		else
		{
			$this->template("admin/".$this->folder.'/list', $data);
		}
	}
	
	public function view_record($id = null)
	{
		if($post = $this->input->post())
		{
			$data["page_title"]	= "Account Information - Details";
			$data["results"]	= (object)$this->user_model->get_record($post["id"]);
			$data["userlevels"]	= $this->user_model->fetch_userlevels();
			$user 				= $this->session->userdata('user');
			$data["userlevel_id"] = $user["userlevel_id"];
			
			if($post["mode"] == "view")
			{
				//IF CAN VIEW
				$data["security"] = $this->security;
				if(!$data["security"]->CanView)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect("admin/surveys");
				}

				echo $this->partial_view("admin/".$this->folder.'/view', $data);
			}
			else
			{
				//IF CAN EDIT
				$data["security"] = $this->security;
				if(!$data["security"]->CanEdit)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect($this->redirect);
				}

				echo $this->partial_view("admin/".$this->folder.'/edit', $data);
			}
		}
	}
	
	public function save_record()
	{
		if($post = $this->input->post())
		{
			if(isset($post["password"]) && $post["password"] != "")
			{
				$post["password"] = md5($post["password"]);
			}
			else
			{
				unset($post["password"]);
			}

			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$user_id = $this->user_model->dynamic_save($params);

			//lets set the user unique id for newly added users
			if(!$post["id"])
			{
				$hash_id 			= substr(md5(uniqid($user_id)), 0, 6);
				$hash_rand 			= random_string('alnum', 6);
				$unique_id			= strtoupper($hash_id.$hash_rand);
				
				$rec["unique_id"] 	= $unique_id;
				$rec["id"] 			= $user_id;
				$params2["table"]	= $this->folder;
				$params2["data"] 	= $rec;
				
				$this->user_model->dynamic_save($params2);
			}
		}
	}
	
	public function delete_record()
	{
		//IF CAN DELETE
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}

		if($post = $this->input->post())
		{
			$post["is_deleted"] = 1;
			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$this->user_model->dynamic_save($params);	

			/*$condition["id"]	= $post["id"];
			$params["where"] 	= $condition;
			$params["table"] 	= $this->folder;
			
			$this->user_model->dynamic_remove($params);*/
		}
	}
}
