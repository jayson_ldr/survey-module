<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends Admin_Controller
{	
	public $folder;
	public $redirect;
	public $module;
	public $security;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('surveys_model', 'user_model'));
		$this->folder 	= strtolower(get_class($this));
		$this->redirect = "admin/".$this->folder;
		$this->module 	= "Reports";//get_class($this);
		$this->security	= $this->abstracts->set_security_access($this->module);
	}
	
	public function index($sort_by='id',$sort_order='DESC')
	{
		//IF CAN VIEW
		$data["security"] = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("access/logout");
		}
		
		$data["module"]		= $this->folder;
		$data["page_title"]	= "Survey Statistics Report";

		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		$param["is_active"]	= 1;

		$user = $this->session->userdata('user');
		if($user["userlevel_id"] != "-1")
		{
			$param["created_by"] = $user["user_id"];
		}

		$data["surveys"] = $this->surveys_model->fetch_record($param);

		$this->template("admin/".$this->folder.'/list', $data);
	}

	public function get_questions()
	{
		if($post = $this->input->post())
		{
			$params["survey_id"] = $post["survey_id"];
			$data["questions"]	 = $this->surveys_model->get_questions($params);

			echo $this->partial_view("admin/".$this->folder.'/form_questions', $data);
		}
	}

	public function get_survey_info()
	{
		if($post = $this->input->post())
		{
			$param["id"] = $post["survey_id"];
			$info = $this->surveys_model->fetch_record($param);
			$info = $info[0];
			
			echo json_encode($info);
		}
	}

	public function get_answers()
	{
		if($post = $this->input->post())
		{
			$ans 			= array();
			$arr_ans 		= array();
			$data["answer"] = array();

			$ans 		= $this->surveys_model->dynamic_get_record("question_id", $post["question_id"], "survey_question_ans");
			if(count($ans) > 0)
			{
				$ans = $ans[0];
				$arr_ans["ans_type"] 	= $ans["ans_type"];
				$arr_ans["details"] 	= json_decode($ans["details"]);
				$arr_ans["answer"] 		= $ans["answer"];
				
				$a_set = array();
				$all_respondents = 0;
				foreach($arr_ans["details"] as $key => $sagot)
				{
					$params["survey_id"] 	= $post["survey_id"];
					$params["question_id"] 	= $post["question_id"];
					$params["date_from"] 	= $post["date_from"];
					$params["date_to"] 		= $post["date_to"];
					$params["answer_index"]	= $key;
					$total = $this->surveys_model->get_respondents_ans($params);
					$all_respondents = $all_respondents + $total;
					
					$a_set["ans"] 	= $sagot;
					$a_set["total"] = $total;
					$arr_ans["details"][$key] = $a_set;
				}
				$data["answer"] 		 = $arr_ans;
				$data["all_respondents"] = $all_respondents;
			}

			echo $this->partial_view("admin/".$this->folder.'/form_answers', $data);
		}
	}

	public function display_graph()
	{
		if($post = $this->input->post())
		{
			$result = $this->surveys_model->get_survey_graph_data($post);

			$arr_total	= array();
			$arr_date	= array();

			if($result)
			{
				foreach($result as $res)
				{
					$arr_total[] 	= $res->total_respondents;
					$arr_date[] 	= $res->date_created;
				}
			}

			$data["total"] 	= $arr_total;
			$data["date"] 	= $arr_date;

			echo json_encode($data);
		}
	}
}