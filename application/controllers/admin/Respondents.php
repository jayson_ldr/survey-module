<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Respondents extends Admin_Controller
{	
	public $folder;
	public $redirect;
	public $module;
	public $security;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('respondents_model', 'surveys_model'));
		$this->folder = strtolower(get_class($this));
		$this->redirect = "admin/".$this->folder;
		$this->module 	= get_class($this);
		$this->security	= $this->abstracts->set_security_access($this->module);
	}
	
	public function index($sort_by='id',$sort_order='DESC', $code=0, $page=0)
	{
		$param = array();

		$data["survey_id"]	= "";
		if($get = $this->input->get())
		{
			$survey_id = (isset($get["survey_id"])) ? $get["survey_id"] : "";
			if($survey_id)
			{
				$param["survey_id"]	= $survey_id;
				$param2["survey_id"]= $survey_id;

				$data["survey_id"]	= $survey_id;
			}
		}

		//IF CAN VIEW
		$data["security"] = $this->security;
		if(!$data["security"]->CanView)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect("access/logout");
		}

		$term				= false;

		if($post = $this->input->post())
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term			= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$param["search"] = $term;

		$user = $this->session->userdata('user');
		if($user["userlevel_id"] != "-1")
		{
			$param["created_by"] 	= $user["user_id"];
			$param2["created_by"] 	= $user["user_id"];
			$p["created_by"]	 	= $user["user_id"];
		}

		$config 				= array();
        $config["base_url"] 	= site_url("admin/".$this->folder.'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config["total_rows"] 	= $this->respondents_model->fetch_record($param, true);
        $config["per_page"] 	= $this->config->item('rows_per_page');
        $config["uri_segment"] 	= 7;
		$config					= $this->paging_style->set_paging_style($config);

        $this->pagination->initialize($config);

		$param["limit"] 	= $config["per_page"];
		$param["offset"] 	= $page;
		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		
        $data["results"] 	= $this->respondents_model->fetch_record($param);
        if($data["results"])
        {
	        foreach($data["results"] as $key => $res)
	        {
	        	$d_fields = json_decode($res->dynamic_fields);
	        	$d_fields = $d_fields[0];

	        	foreach($d_fields as $i => $field)
	        	{
	        		if($i == "name")
	        		{
	        			$data["results"][$key]->respondent = $field;
	        		}
	        	}
	        }
		}
        $param2["search"]	= $term;
        $data["total_rows"] = $this->respondents_model->fetch_record($param2, true);
        $data["total"] 		= $config["total_rows"];
		$data["links"] 		= $this->pagination->create_links();
		$data["page"]	 	= $page;
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data["code"]		= $code;
		$data["module"]		= $this->folder;
		$data["page_title"]	= "Respondents";

		$p["sort_by"] 		= 'id';
		$p["sort_order"]	= 'DESC';
		$p["is_active"]		= 1;
		$data["surveys"] 	= $this->surveys_model->fetch_record($p);
		
		if ($this->input->is_ajax_request())
		{
			//IF CAN ADD
			$data["security"] = $this->security;
			if(!$data["security"]->CanAdd)
			{
				$this->session->set_flashdata('error', 'Access Not Allowed!');
				redirect($this->redirect);
			}

			echo $this->partial_view("admin/".$this->folder.'/add', $data);
		}
		else
		{
			$this->template("admin/".$this->folder.'/list', $data);
		}
	}
	
	public function view_record($id = null)
	{
		if($post = $this->input->post())
		{
			$data["page_title"]	= "Role Information - Details";
			$record				= (object)$this->respondents_model->get_record($post["id"]);
			$d_fields 			= json_decode($record->dynamic_fields);
        	$d_fields 			= $d_fields[0];
        	$record->respondent_info = $d_fields;
        	$params["respondent_id"] = $record->id;
        	$answers = $this->surveys_model->get_respondents_ans($params, false);

        	$arr_answers = array();
        	foreach($answers as $index => $ans)
        	{
        		$arr_answers[$ans->question_id] = $ans;
        		//unset($answers[$index]);
        	}
        	$record->answers	= $arr_answers;
			$data["results"]	= $record;

			$data["survey"]["question"]	= (array)$this->surveys_model->dynamic_get_record("survey_id", $record->survey_id, "survey_questions");

			$ans 		= array();
			$arr_ans 	= array();
			if(count($data["survey"]["question"]) > 0)
			{
				foreach($data["survey"]["question"] as $key => $q)
				{
					$ans = $this->surveys_model->dynamic_get_record("question_id", $q["id"], "survey_question_ans");
					if(count($ans) > 0)
					{
						$ans = $ans[0];
						$arr_ans["ans_type"] 	= $ans["ans_type"];
						$arr_ans["details"] 	= json_decode($ans["details"]);
						$arr_ans["answer"] 		= $ans["answer"];

						$data["survey"]["question"][$key]["answer"] = $arr_ans;
					}
				}
			}

			if($post["mode"] == "view")
			{
				//IF CAN VIEW
				$data["security"] = $this->security;
				if(!$data["security"]->CanView)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect("admin/surveys");
				}

				echo $this->partial_view("admin/".$this->folder.'/view', $data);
			}
			else
			{
				//IF CAN EDIT
				$data["security"] = $this->security;
				if(!$data["security"]->CanEdit)
				{
					$this->session->set_flashdata('error', 'Access Not Allowed!');
					redirect($this->redirect);
				}

				echo $this->partial_view("admin/".$this->folder.'/edit', $data);
			}
		}
	}
	
	public function save_record()
	{
		if($post = $this->input->post())
		{
			$post["slug"] = str_replace(" ", "_", strtolower($post["name"]));

			if(isset($post["content"]))
			{
				$post["content"]	= json_encode($post["content"]);
			}
			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$this->respondents_model->dynamic_save($params);
		}
	}
	
	public function delete_record()
	{
		//IF CAN DELETE
		$data["security"] = $this->security;
		if(!$data["security"]->CanDelete)
		{
			$this->session->set_flashdata('error', 'Access Not Allowed!');
			redirect($this->redirect);
		}

		if($post = $this->input->post())
		{
			$condition["id"]	= $post["id"];
			$params["where"] 	= $condition;
			$params["table"] 	= $this->folder;
			
			$this->respondents_model->dynamic_remove($params);
		}
	}
}
