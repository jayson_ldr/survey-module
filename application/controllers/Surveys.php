<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Surveys extends Public_Controller
{	
	public $folder;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model(array('surveys_model', 'fields_model'));
		$this->folder = strtolower(get_class($this));

		$user = $this->session->userdata('user');
		if($user){
			redirect('admin/surveys');
		}
	}
	
	public function index($sort_by='id',$sort_order='DESC', $code=0, $page=0)
	{
		$term				= false;
		$param		 		= array();

		if($post = $this->input->post())
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Search_model->record_term($term);
			$data['code']	= $code;
			//reset the term to an object for use
			$term			= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
		$param["search"] 		= $term;
		$param["is_active"]		= 1;
		
		$config 				= array();
        $config["base_url"] 	= site_url($this->folder.'/index/'.$sort_by.'/'.$sort_order.'/'.$code.'/');
        $config["total_rows"] 	= $this->surveys_model->fetch_record($param, true);
        $config["per_page"] 	= $this->config->item('rows_per_page');
        $config["uri_segment"] 	= 6;
		$config					= $this->paging_style->set_paging_style($config);

        $this->pagination->initialize($config);

		$param["limit"] 	= $config["per_page"];
		$param["offset"] 	= $page;
		$param["sort_by"] 	= $sort_by;
		$param["sort_order"]= $sort_order;
		
        $data["results"] 	= $this->surveys_model->fetch_record($param);
        $data["total"] 		= $config["total_rows"];
		$data["links"] 		= $this->pagination->create_links();
		$data["page"]	 	= $page;
		$data["sort_by"	]	= $sort_by;
		$data["sort_order"]	= $sort_order;
		$data["code"]		= $code;
		$data["module"]		= $this->folder;
		$data["page_title"]	= "Available Surveys";
		
		if ($this->input->is_ajax_request())
		{
			echo $this->partial_view($this->folder.'/add');
		}
		else
		{
			$this->template($this->folder.'/list', $data);
		}
	}

	public function check_quota($id)
	{
		if($id)
		{
			$survey = $this->surveys_model->get_record($id);
			$check 	= false;
			if( $survey["total_res"] < $survey["quota"] )
			{
				$check = true;
			}
			return $check;
		}
	}

	public function process($id)
	{
		if($id)
		{
			// we first need to check if quota already reached
			$q_check =  $this->check_quota($id);
			if($q_check)
			{
				$data["page_title"]	= "Survey Questionnaire:";

				//lets get the dynamic respondent fields
				$params["is_active"] = 1;
				$respondent_fields = $this->fields_model->fetch_record($params);
				foreach($respondent_fields as $index => $field)
				{
					$respondent_fields[$index]->content = json_decode($field->content);
				}
				$data["respondent_fields"]	= $respondent_fields;
				
				$data["survey"]				= $this->surveys_model->get_record($id);
				$data["survey"]["question"]	= $this->surveys_model->dynamic_get_record("survey_id", $id, "survey_questions");

				if(!empty($data["survey"]["question"]))
				{
					$ans 		= array();
					$arr_ans 	= array();
					foreach($data["survey"]["question"] as $key => $q)
					{
						$ans = $this->surveys_model->dynamic_get_record("question_id", $q["id"], "survey_question_ans");
						$ans = $ans[0];
						$arr_ans["ans_type"] 	= $ans["ans_type"];
						$arr_ans["details"] 	= json_decode($ans["details"]);
						$arr_ans["answer"] 		= $ans["answer"];

						$data["survey"]["question"][$key]["answer"] = $arr_ans;
					}
					#pr($data["survey"]);

					$this->template($this->folder.'/form_process', $data);
				}
				else
				{
					$this->session->set_flashdata('error', 'Survey not found.');
					redirect("surveys");
				}
			}
			else
			{
				$this->session->set_flashdata('error', 'Survey not available, quota already reached.');
				redirect("surveys");
			}
		}
	}

	public function process_by_slug($slug)
	{
		if($slug)
		{
			$record = $this->surveys_model->get_record_by_slug($slug);
			if($record["id"])
			{
				$this->process($record["id"]);
			}
			else
			{
				$this->session->set_flashdata('error', 'Survey not found.');
				redirect("surveys");
			}			
		}
		else
		{
			$this->session->set_flashdata('error', 'Survey not available.');
			redirect("surveys");
		}
	}

	public function save_respondent_ans()
	{
		if($post = $this->input->post())
		{
			$answers = json_decode($post["answers"]);
			unset($post["answers"]);

			$respondent_id = $post["respondent_id"];
			if($respondent_id)
			{
				$res["score"]			= $post["score"];
            	$res["overall_duration"]= $post["overall_duration"];
            	//update respondent info
				$params1["id"]		= $respondent_id;
				$params1["table"]	= "survey_respondents";
				$params1["data"] 	= $res;	
            	$this->surveys_model->dynamic_update($params1);

				$ans["respondent_id"] 	= $respondent_id;
				$ans["survey_id"] 		= $post["survey_id"];

				foreach($answers as $sagot)
				{
					$ans["question_id"]	= $sagot->question_id;
                    $ans["action"]		= $sagot->action;
                    $ans["answer_index"]= $sagot->answer_index;
                    $ans["duration"]	= $sagot->duration;
                    //save respondents answers per questions
                    $params2["table"]	= "survey_respondent_ans";
					$params2["data"] 	= $ans;			
					$this->surveys_model->dynamic_save($params2);
				}
			}
		}
	}

	public function save_respondent_info()
	{
		if($post = $this->input->post())
		{
			// we first need to check if quota already reached
			if( $this->check_quota($post["survey_id"]) )
			{
				//save respondents info
				$post["ip_address"]		= $this->get_ip_address();
				$params["table"]		= "survey_respondents";
				$params["data"] 		= $post;			
				$data["respondent_id"] 	= $this->surveys_model->dynamic_save($params);

				echo json_encode($data);
			}
			else
			{
				$data["error_val"] = 1;

				echo json_encode($data);
			}//endif
		}//endif
	}

	public function get_ip_address()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	public function view_record($id = null)
	{
		if($post = $this->input->post())
		{
			$data["page_title"]	= "Survey Information - Details";
			$data["results"]	= (object)$this->surveys_model->get_record($post["id"]);
			
			if($post["mode"] == "view")
			{
				echo $this->partial_view($this->folder.'/view', $data);
			}
			else
			{
				echo $this->partial_view($this->folder.'/edit', $data);
			}
		}
	}
	
	public function save_record()
	{
		if($post = $this->input->post())
		{
			$params["table"]	= $this->folder;
			$params["data"] 	= $post;
			
			$this->surveys_model->dynamic_save($params);
		}
	}
	
	public function delete_record()
	{
		if($post = $this->input->post())
		{
			$condition["id"]	= $post["id"];
			$params["where"] 	= $condition;
			$params["table"] 	= $this->folder;
			
			$this->surveys_model->dynamic_remove($params);
		}
	}
}
