<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Base_Model extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
}
class MY_Model extends Base_Model {
	public $user_information; 
	
	public function __construct() {
		parent::__construct();
		//RESET SYSTEM CLOCK
		date_default_timezone_set("Asia/Manila");
		if( $this->session->userdata( 'user' ) ) {
			//LOGGED USER MAKE IT GLOBAL
			//LOGGED USER INFORMATION MAKE IT GLOBAL
			
			$this->user_information = $this->session->userdata( 'user' );
		} else {
			//REDIRECT TO LOGIN PAGE
			//redirect( 'security/login' );
		}
	}
	
	
	public function dynamic_save($params = array()){
		error_reporting(E_ALL);
		if(!empty($params['data']['id'])){
			$id = $params['data']['id'];
			unset($params['data']['id']);
			$this->db->where('id', $id); 
			$this->db->update($params['table'], $params['data']);
			
		} else {
			$this->db->insert($params['table'], $params['data']);
			// echo $this->db->last_query();
			$id = $this->db->insert_id();
		}
		
		#$this->record_history($id, $params['table'], 'saved');
		
		return $id;
	}
	
	public function dynamic_get($params, $count = false ){
		if(!empty($params['fields'])) {
			$this->db->select($params['fields']);
		}
		
		$this->db->from($params['table']);
		
		if(!empty($params['where'])) {
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}
		
		if(!empty($params['joins'])) {
			foreach($params['joins'] as $key => $value){
			// print_r($value); die;
				$this->db->join($value['table'], $value['condition'], $value['type']);
			}
		}
		if(!empty($params['or_where'])){
			 foreach($params['or_where'] as $key => $value){
				$this->db->or_where($key, $value);
			} 
			//$this->db->or_where('(`created_by` = 15 AND `user_type_id` = 2)');
		}
		
		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}

		if(isset($params['limit']) && isset($params['offset'])) {
			$this->db->limit($params['limit'], $params['offset']);
		}
		
		if(!empty($params['where_in'])) {
			foreach($params['where_in'] as $key => $value){
				$this->db->where_in($key, $value);
			}
		}

		if(!empty($params['where_in'])) {
			foreach($params['where_in'] as $key => $value){
				$this->db->where_in($key, $value);
			}
		}

		if(!empty($params['group_by'])) {
			foreach($params['group_by'] as $key => $value){
				$this->db->group_by($params['group_by']); 
			}
		}

		if(!empty($params['title']) && !empty($params['match'])) {
			$this->db->like($params['title'], $params['match']); 
		}
		
		if(!empty($params['like'])) {
			foreach($params['like'] as $key => $value){
				
				$this->db->like($value['title'], $value['match']);
			}
		}
		
		if(!empty($params['search'])){
			$params['condition'] .= " AND (
									`name`  LIKE  '%$params[search]%'
									OR  `industry` LIKE  '%$params[search]%'
									OR  `website` LIKE  '%$params[search]%'
									OR  `phone` LIKE  '%$params[search]%'
									)";
			$this->db->where($params['condition']);
		}
		// die($params['condition']);
		
		
		if ($count) {
			return $this->db->count_all_results( );
		} else {
			return $this->db->get()->result_array();
		}
	}
	
	public function dynamic_query_get($params, $count = false, $distinct = false){
		// echo "<pre>";
		// print_r($params);
		// die();
		if(!empty($params['fields'])){
			$this->db->select($params['fields']);
		}
		
		$this->db->from($params['table']);
		
		if(!empty($params['joins'])) {
			foreach($params['joins'] as $key => $value){
				$this->db->join($value['table'], $value['condition'], $value['type']);
			}
		}
		
		if(!empty($params['limit'])){
			$offset = 0;
			if(!empty($params['offset'])) 
				$offset = $params['offset'];
			
			$this->db->limit($params['limit'], $offset);
		} else {
		
		}

		if(!empty($params['group_by'])) {
			foreach($params['group_by'] as $key => $value){
				$this->db->group_by($params['group_by']); 
			}
		}

		if(!empty($params['title']) && !empty($params['match'])) {
			$this->db->like($params['title'], $params['match']); 
		}
		
		if(!empty($params['like'])) {
			// foreach($params['like'] as $key => $value){
				//$this->db->like($params['like']['title'], $params['like']['match']);
			// }
			/* $this->db->like('name', $params['like']);
			$this->db->or_like('industry', $params['like']); 
			$this->db->or_like('website', $params['like']); 
			$this->db->or_like('phone', $params['like']);  */
			
			$params['condition'] .= " AND (
									`name`  LIKE  '%$params[like]%'
									OR  `industry` LIKE  '%$params[like]%'
									OR  `website` LIKE  '%$params[like]%'
									OR  `phone` LIKE  '%$params[like]%'
									)";
		}
		//die($params['condition']);
		if(!empty($params['condition']))
			$this->db->where($params['condition']);
		
		
		if(!empty($params['search'])){
			$params['condition'] .= " AND (
									`name`  LIKE  '%$params[search]%'
									OR  `industry` LIKE  '%$params[search]%'
									OR  `website` LIKE  '%$params[search]%'
									OR  `phone` LIKE  '%$params[search]%'
									)";
			$this->db->where($params['condition']);
		}
		
		if(!empty($params['lead_search'])){
			$params['condition'] .= " AND (
									`company_information`.`name`  LIKE  '%$params[lead_search]%'
									OR  `company_information`.`industry` LIKE  '%$params[lead_search]%'
									OR  `company_information`.`website` LIKE  '%$params[lead_search]%'
									OR  `company_information`.`phone` LIKE  '%$params[lead_search]%'
									OR  `lead_information`.`need` LIKE  '%$params[lead_search]%'
									OR  `lead_information`.`description` LIKE  '%$params[lead_search]%'
									OR  `lead_information`.`comment` LIKE  '%$params[lead_search]%'
									)";
			$this->db->where($params['condition']);
		}

		if(!empty($params['order_by'])){
			foreach($params['order_by'] as $key => $value){
				$this->db->order_by($key, $value);
			}
		}

		if(!empty($params['where'])){
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
		}

		if($count) {
			#return $this->db->get()->num_rows();
			
			if($distinct)
			{
				$this->db->select("COUNT($distinct) as total");
				return $this->db->get()->result_array();
			}
			else
			{
				return $this->db->count_all_results();
			}

		} else {

			return $this->db->get()->result_array();
		}
		 
		
		
	}
	
	public function dynamic_update($params = array()){
		
		$this->db->where('id', $params['id']);
		$this->db->update($params['table'], $params['data']);
		
		#$this->record_history($params['id'], $params['table'], 'updated');
		
		return $this->db->affected_rows();
	}
	
	public function dynamic_delete($params = array()){
		$data = array('is_deleted' => 1);
		
		
		if(!empty($params['table']) && !empty($params['where'])){
		
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
			$this->db->update($params['table'], $data);
			// echo $this->db->last_query();
			$affected_rows = $this->db->affected_rows();
			#$this->record_history($value, $params['table'], 'deleted');
			
			return $affected_rows;
		} 
		return 0;
		
		 
	}
	
	public function dynamic_remove($params = array()){
		$data = array('is_deleted' => 1);
		if(!empty($params['table']) && !empty($params['where'])){
		
			foreach($params['where'] as $key => $value){
				$this->db->where($key, $value);
			}
			$this->db->delete($params['table']);
			
			#$this->record_history($value, $params['table'], 'deleted');
			
		}
		
		return $this->db->affected_rows();
	}
	
	public function save_images( $id = 0, $params ) {
	
		$data['id'] = $id;
		
		
		$this->load->library('upload');
		
		
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '8000';
		$config['max_width']  = '4000';
		$config['max_height']  = '4000';
		$config['encrypt_name'] = TRUE;
		

		if( $id && ! empty( $_FILES ) ) {
			
			foreach( $_FILES as $name => $image_infomation ) {
				
				$config['upload_path'] = 'assets/uploads/' .$name;
				$this->upload->initialize( $config );
				
				if ( ! $this->upload->do_upload( $name ) ) {
				
					$error = $this->upload->display_errors();
					
				} else {
				
					$image = $this->upload->data();
					
					
					$data[$name] = $image['file_name'];
					$image_params = array(
						'table' => $params['module'],
						'data' => $data,
					);
					
					
					
					$this->dynamic_save( $image_params );
					
					if(!empty($params)){
						$file_upload_data = array(
								'orig_name'		=> $image['orig_name'],
								'file_name'		=> $image['file_name'],
								'module'		=> $params['module'],
								'module_id'		=> $params['id'],
								'date_created' 	=> 	date('Y-m-d'),
							);
						
						$save_data = array(
							'table' => 'core_documents',
							'data'		=> $file_upload_data,
						);
						$this->dynamic_save($save_data);
					}
					
				}
			
			}
		
		}
	
	}
	
	public function save_file( $id = 0, $params = array() ) {
	
		$data['id'] = $id;
		
		
		$this->load->library('upload');
		
		$config['allowed_types'] = '*'; 
		$config['max_size']	= '8000';
		$config['max_width']  = '4000';
		$config['max_height']  = '4000';
		$config['encrypt_name'] = TRUE;
		

		if( $id && ! empty( $_FILES ) ) {
			
			foreach( $_FILES as $name => $image_infomation ) {
				
				$config['upload_path'] = 'assets/files/' .$name;
				$this->upload->initialize( $config );
				
				if ( ! $this->upload->do_upload( $name ) ) {
				
					$error = $this->upload->display_errors();
					die($error);
				} else {
				
					$image = $this->upload->data();
					
					$data[$name] = $image['file_name'];
					
					if(!empty($params)){
						$file_upload_data = array(
								'orig_name'		=> $image['orig_name'],
								'file_name'		=> $image['file_name'],
								'module'		=> $params['module'],
								'module_id'		=> $params['id'],
								'date_created' 	=> 	date('Y-m-d'),
							);
						
						$save_data = array(
							'table' => 'core_documents',
							'data'		=> $file_upload_data,
						);
						$this->dynamic_save($save_data);
						
						
						
						$return = $image;
						
						if(!empty($params['get_data'])){
							return  $return;
						} else {
							return true;
						}
						
					}
					
				}
			
			}
		
		}
	
	}
	
	public function save_multi_file( $params = array() ) {
	
		
		$data['id'] = $params['id'];
		
		
		$this->load->library('upload');
		
		$config['allowed_types'] = '*';  /* $params['allowed_types'];  */
		$config['max_size']	= '8000';
		$config['max_width']  = '4000';
		$config['max_height']  = '4000';
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = 'assets/files/form_file/';

		if( $params['id'] && ! empty( $_FILES['file_name'] ) ) {
		
		
			$file_counter = count($_FILES['file_name']['name']);
			$failed='';
			
		
			for($x = 0; $x  <= $file_counter; $x++){
				
				
				if(empty($_FILES['file_name']['name'][$x]))
					continue;
					
				$_FILES['file']	= array(
								'name' => $_FILES['file_name']['name'][$x],
								'type' => $_FILES['file_name']['type'][$x],
								'tmp_name' => $_FILES['file_name']['tmp_name'][$x],
								'error' => $_FILES['file_name']['error'][$x],
								'size' => $_FILES['file_name']['size'][$x],
							);
			
				$this->load->library('upload');
				$this->upload->initialize( $config );
				
				if ( ! $this->upload->do_upload('file')){
				
					$error = array('error' => $this->upload->display_errors());
					$failed .= $_FILES['file']['name'].' : '.$error['error'].' <br/>';
					
				} else {
					$file_details = array('upload_data' => $this->upload->data());

					$file_upload_data = array(
							'file_name'		=> $file_details['upload_data']['orig_name'],
							'filename'		=> $file_details['upload_data']['file_name'],
							'module'		=> $params['module'],
							'module_id'		=> $params['id'],
							'date_created' 	=> 	date('Y-m-d'),
						);
					
					$save_data = array(
						'table' => 'core_documents',
						'data'		=> $file_upload_data,
					);
					
					
					$this->dynamic_save($save_data); 
				
				}
			}
			
		}
	
	}
	
	
	/* ALWAYS RECORD HISTORY */
	public function record_history( $id = 0, $table = '', $action = '' ) {
		
		if( $id && $table && $action ) {
		
			$data['affected_id'] = $id;
			$data['affected_table'] = $table;
			$data['action'] = $action;
			$data['created_by'] = $this->user_information['user_id'];
			$this->db->insert( 'history', $data );
		
		} else {
			
			$this->session->set_flashdata( 'error', 'Error recording history! Check your source code!' );
			redirect( base_url() );
		
		}
		
	}
}