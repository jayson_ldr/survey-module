<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// core/MY_Controller.php
/**
 * Base Controller
 * 
 */

class MY_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Load shared resources here or in autoload.php
        date_default_timezone_set("Asia/Singapore");
		
		$this->load->model( array('Search_model') );
		$this->load->helper( array('table_helper', 'default_helper'));
		$this->load->library( array('access', 'abstracts'));

		//security to validate if user is already loggedin
		$logged_in = $this->auth->is_logged_in(false, false);
		if (!$logged_in){
			redirect('access/login');
		}
    }
}

class Admin_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
		
		$user = $this->session->userdata('user');
		if(empty($user)){
			redirect('access/login');
		}
    }

	public function template($view, $vars = array(), $string = false)
	{
		
		$vars['user'] 			= $this->session->userdata('user');
		$vars["userlevel_id"]	= $vars['user']["userlevel_id"];

		if($string)
		{
			$result	 = $this->load->view('partials/header', $vars, true);
			$result	.= $this->load->view($view, $vars, true);
			$result	.= $this->load->view('partials/footer', $vars, true);
			
			return $result;
		}
		else
		{
			$this->load->view('partials/header', $vars);
			$this->load->view($view, $vars);
			$this->load->view('partials/footer', $vars);
		}
	}

	public function partial_view($view, $vars = array(), $string = false, $json = false)
	{
		$result	= $this->load->view($view, $vars, true);
		
		if ($json)
		{
			echo json_encode($result);
		}
		elseif($string)
		{
			return $result;
		}
		else
		{
			echo $result;
		}		
	}
}

/**
 * Default Front-end Controller
 * 
 */ 
class Public_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Load any front-end only dependencies
        
        $this->load->helper( array('table_helper', 'default_helper'));
	}
	
	public function template($view, $vars = array(), $string = false)
	{
		
		$vars['user'] = $this->session->userdata('user');

		if($string)
		{
			$result	 = $this->load->view('partials/header', $vars, true);
			$result	.= $this->load->view($view, $vars, true);
			$result	.= $this->load->view('partials/footer', $vars, true);
			
			return $result;
		}
		else
		{
			$this->load->view('partials/header', $vars);
			$this->load->view($view, $vars);
			$this->load->view('partials/footer', $vars);
		}
	}

	public function partial_view($view, $vars = array(), $string = false, $json = false)
	{
		$result	= $this->load->view($view, $vars, true);
		
		if ($json)
		{
			echo json_encode($result);
		}
		elseif($string)
		{
			return $result;
		}
		else
		{
			echo $result;
		}		
	}
}