<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {
	
	private $table_name = 'admin_users';

	var $CI;
	
	//this is the expiration for a non-remember session
	var $session_expire	= 36000; //7200;
	
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('encrypt');
		$this->CI->load->library('session');
		
		/* $admin_session_config = array(
		    'sess_cookie_name' => 'admin_session_config',
		    'sess_expiration' => 0
		);
		$this->CI->load->library('session', $admin_session_config, 'admin_session'); */
		
		$this->CI->load->helper('url');
	}
	
	
	public function login_user($username = false, $password = false, $remember = false){
		$this->CI->db->select('*');
		$this->CI->db->where('username', $username);
		$this->CI->db->or_where('email', $username);
		$this->CI->db->where('password',  $password);
		$this->CI->db->limit(1);
		$result = $this->CI->db->get('users');
		$result	= $result->row_array();
		
		if (sizeof($result) > 0)
		{		
			$user = array();
			$user['user']					= array();
			$user['user']['user_id'] 		= $result['id'];
			$user['user']['password'] 		= $result['password'];
			$user['user']['first_name']		= $result['first_name'];
			$user['user']['last_name']		= $result['last_name'];
			$user['user']['email']			= $result['email'];
			$user['user']['contact']		= $result['contact'];
			$user['user']['last_login']		= $result['last_login'];
			$user['user']['userlevel_id']	= $result['userlevel_id'];
			
			if(!$remember)
			{
				$user['user']['expire'] = time()+$this->session_expire;
			}
			else
			{
				$user['user']['expire'] = false;
			}
			
			$this->CI->session->set_userdata($user);
			return $result['id']; //true;
		}
		else
		{
			return false;
		}		
	}
	
	public function is_logged_in($redirect = false, $default_redirect = true){
		$user = $this->CI->session->userdata('user');
		
		if (!$user){
			if ($redirect){
				$this->CI->session->set_flashdata('redirect', $redirect);
			}
				
			if ($default_redirect){	
				redirect('login');
			}
			
			return false;
		}else{
			//check if the session is expired if not reset the timer
			if($user['expire'] && $user['expire'] < time()){
				$this->logout();
				if($redirect){
					$this->CI->session->set_flashdata('redirect', $redirect);
				}

				if($default_redirect){
					redirect('login');
				}

				return false;
			}else{
				//update the session expiration to last more time if they are not remembered
				if($user['expire']){
					$user['expire'] = time()+$this->session_expire;
					$this->CI->session->set_userdata(array('user'=>$user));
				}
			}
			return true;
		}
	}
	
	/*
	this function does the logging in.
	*/
	function login_admin($email, $password, $remember=false)
	{
		$this->CI->db->select('*');
		$this->CI->db->where('username', $email);
		$this->CI->db->where('password',  $password);
		$this->CI->db->limit(1);
		$result = $this->CI->db->get($this->table_name);
		$result	= $result->row_array();
		
		if (sizeof($result) > 0)
		{
			$user = array();
			$user['user']				= array();
			$user['user']['user_id']	= $result['user_id'];
			$user['user']['username'] 	= $result['username'];
			$user['user']['password'] 	= $result['password'];
			$user['user']['first_name']	= $result['first_name'];
			$user['user']['last_name']	= $result['last_name'];
			$user['user']['email']		= $result['email'];
			$user['user']['last_login']	= $result['last_login'];
			
			if($remember <> "")
			{
				$user['user']['expire'] = time()+$this->session_expire;
			}
			else
			{
				$user['user']['expire'] = false;
			}

			$this->CI->admin_session->set_userdata($user);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	this function does the logging out
	*/
	function logout()
	{
		$this->CI->session->unset_userdata('user');
		$this->CI->session->sess_destroy();
	}

	/*
	This function resets the admins password and emails them a copy
	*/
	function reset_password($username)
	{
		$admin = $this->get_admin_by_username($username);
		if ($admin)
		{
			$this->CI->load->helper('string');
			$this->CI->load->library('email');
			
			$new_password		= random_string('alnum', 8);
			$admin['password']	= $new_password;
			$this->save_admin($admin);
			
			$this->CI->email->from($this->CI->config->item('email'), $this->CI->config->item('site_name'));
			$this->CI->email->to($email);
			$this->CI->email->subject($this->CI->config->item('site_name').': Admin Password Reset');
			$this->CI->email->message('Your password has been reset to '. $new_password .'.');
			$this->CI->email->send();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	This function gets the admin by their email address and returns the values in an array
	it is not intended to be called outside this class
	*/
	private function get_admin_by_username($username)
	{
		$this->CI->db->select('*');
		$this->CI->db->where('username', $username);
		$this->CI->db->limit(1);
		$result = $this->CI->db->get($this->table_name);
		$result = $result->row_array();

		if (sizeof($result) > 0)
		{
			return $result;	
		}
		else
		{
			return false;
		}
	}
	
	/*
	This function takes admin array and inserts/updates it to the database
	*/
	function save($admin)
	{
		if ($admin['user_id'])
		{
			$this->CI->db->where('user_id', $admin['user_id']);
			$this->CI->db->update($this->table_name, $admin);
		}
		else
		{
			$this->CI->db->insert($this->table_name, $admin);
		}
	}
	
	
	/*
	This function gets a complete list of all admin
	*/
	function get_admin_list()
	{
		$this->CI->db->select('*');
		$this->CI->db->order_by('last_name', 'ASC');
		$this->CI->db->order_by('first_name', 'ASC');
		$this->CI->db->order_by('email', 'ASC');
		$result = $this->CI->db->get($this->table_name);
		$result	= $result->result();
		
		return $result;
	}

	/*
	This function gets an individual admin
	*/
	function get_admin($id)
	{
		$this->CI->db->select('*');
		$this->CI->db->where('user_id', $id);
		$result	= $this->CI->db->get($this->table_name);
		$result	= $result->row();

		return $result;
	}		
	
	function check_id($str)
	{
		$this->CI->db->select('user_id');
		$this->CI->db->from($this->table_name);
		$this->CI->db->where('user_id', $str);
		$count = $this->CI->db->count_all_results();
		
		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	function check_email($str, $id=false)
	{
		$this->CI->db->select('email');
		$this->CI->db->from($this->table_name);
		$this->CI->db->where('email', $str);
		if ($id)
		{
			$this->CI->db->where('user_id !=', $id);
		}
		$count = $this->CI->db->count_all_results();
		
		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function delete($id)
	{
		if ($this->check_id($id))
		{
			$admin	= $this->get_admin($id);
			$this->CI->db->where('user_id', $id);
			$this->CI->db->limit(1);
			$this->CI->db->delete($this->table_name);

			return $admin->firstname.' '.$admin->lastname.' has been removed.';
		}
		else
		{
			return 'The admin could not be found.';
		}
	}
}
// END Auth class

/* End of file Auth.php */
/* Location: ./Application/libraries/Auth.php */
