<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paging_style {
	
	public function set_paging_style($config)
	{
		//pagination customization using bootstrap styles
		$config['full_tag_open'] 	= '<div class="pagination pagination-centered"><ul class="page_test">';
		$config['full_tag_close'] 	= '</ul></div><!--pagination-->';
		$config['first_link'] 		= '&laquo; First';
		$config['first_tag_open'] 	= '<li class="page-item prev page">';
		$config['first_tag_close'] 	= '</li>';

		$config['last_link'] 		= 'Last &raquo;';
		$config['last_tag_open'] 	= '<li class="page-item next page">';
		$config['last_tag_close'] 	= '</li>';

		$config['next_link'] 		= 'Next &rarr;';
		$config['next_tag_open'] 	= '<li class="page-item next page">';
		$config['next_tag_close'] 	= '</li>';

		$config['prev_link'] 		= '&larr; Previous';
		$config['prev_tag_open'] 	= '<li class="page-item prev page">';
		$config['prev_tag_close'] 	= '</li>';

		$config['cur_tag_open'] 	= '<li class="page-item active"><a class="page-link" href="">';
		$config['cur_tag_close'] 	= '</a></li>';

		$config['num_tag_open'] 	= '<li class="page-item page">';
		$config['num_tag_close'] 	= '</li>';
		
		return $config;
	}
}