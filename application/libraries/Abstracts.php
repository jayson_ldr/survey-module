<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Abstracts {
	
	public $tbl_userlevels 	= 'userlevels';
	public $tbl_users	 	= 'users';
	
	var $CI;
	
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->database();
		
		$this->CI->load->library( 'access');
	}
	
	
	public function get_userlevels()
	{
		$result = $this->CI->db->get($this->tbl_userlevels)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->id] = $obj->userlevel_name;
		}
		return $arr;
	}
	
	public function get_users()
	{
		$result = $this->CI->db->get($this->tbl_users)->result();
		$arr 	= array();
		foreach($result as $obj)
		{
			$arr[$obj->user_id] = $obj->first_name." ".$obj->last_name;
		}
		return $arr;
	}
	
	public function set_security_access($module)
	{
		$data["CanList"]	= $this->CI->access->ValidateAccess($module, "list");
		$data["CanView"]	= $this->CI->access->ValidateAccess($module, "view");
		$data["CanAdd"]		= $this->CI->access->ValidateAccess($module, "add");
		$data["CanEdit"]	= $this->CI->access->ValidateAccess($module, "edit");
		$data["CanDelete"]	= $this->CI->access->ValidateAccess($module, "delete");
		
		$tables = $this->CI->access->DynamicTables();
		
		foreach($tables as $table)
		{
			$table_nav = str_replace(" ", "_", $table);
			$data["CanView_".$table_nav] = $this->CI->access->ValidateAccess($table, "view");
		}
		
		return (object)$data;
	}

}
// END Abstracts class

/* End of file Abstracts.php */
/* Location: ./Application/libraries/Abstracts.php */
