<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access {
	var $ALLOW_ADD 		= 1;
	var $ALLOW_DELETE 	= 2;
	var $ALLOW_EDIT		= 4;
	var $ALLOW_VIEW		= 8;
	var $ALLOW_LIST		= 8;
	
	// Dynamic User Level settings
	// User level definition table/field names
	var $USER_LEVEL_TABLE		= "userlevels";
	var $USER_LEVEL_ID_FIELD 	= "id";
	var $USER_LEVEL_NAME_FIELD 	= "userlevel_name";
	
	// User Level privileges table/field names
	var $USER_LEVEL_PRIV_TABLE 					= "userlevel_permissions";
	var $USER_LEVEL_PRIV_TABLE_NAME_FIELD 		= "module_type";
	var $USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD 	= "userlevel_id";
	var $USER_LEVEL_PRIV_PRIV_FIELD 			= "permission";
	
	var $QueryStringValue;
	var $CurrentValue;
	var $FormValue;
	var $UserLevel 		= array();
	var $UserLevelPriv 	= array();
	var $CI;
	
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->database();
		$this->CI->load->library('encrypt');
	}
	
	public function proccess($TABLE_NAME_FIELD, $USER_LEVEL_ID_FIELD, $PRIV_FIELD)
	{
		$this->CI->db->select("*");
		$this->CI->db->from($this->USER_LEVEL_PRIV_TABLE);
		$this->CI->db->where($this->USER_LEVEL_PRIV_TABLE_NAME_FIELD, $TABLE_NAME_FIELD);
		$this->CI->db->where($this->USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD, $USER_LEVEL_ID_FIELD);
		
		$query = $this->CI->db->get();
		if($query->num_rows() > 0)
		{
			$data["$this->USER_LEVEL_PRIV_PRIV_FIELD"] = $PRIV_FIELD;
			
			$this->CI->db->where($this->USER_LEVEL_PRIV_TABLE_NAME_FIELD, $TABLE_NAME_FIELD);
			$this->CI->db->where($this->USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD, $USER_LEVEL_ID_FIELD);
			$this->CI->db->update($this->USER_LEVEL_PRIV_TABLE, $data);
		}
		else
		{
			$data["$this->USER_LEVEL_PRIV_TABLE_NAME_FIELD"] 	= $TABLE_NAME_FIELD;
			$data["$this->USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD"] = $USER_LEVEL_ID_FIELD;
			$data["$this->USER_LEVEL_PRIV_PRIV_FIELD"] 			= $PRIV_FIELD;
			
			$this->CI->db->insert($this->USER_LEVEL_PRIV_TABLE, $data);
		}
	}	
	// function to get (all) User Level settings from database
	public function SetUpUserLevelEx($UserLevelID) {
		
		if ($UserLevelID == "" || is_null($UserLevelID) || !is_numeric($UserLevelID)) return;
	
		// Get the User Level definitions
		$this->CI->db->select('id', $this->USER_LEVEL_NAME_FIELD);
		$this->CI->db->from($this->USER_LEVEL_TABLE);
		
		if ($UserLevelID >= -1)
		{
			
			$this->CI->db->where($this->USER_LEVEL_ID_FIELD, $UserLevelID);
		}
		$result = $this->CI->db->get();
		$this->UserLevel = $result->result_array();
		$result->free_result();
		
		// Get the User Level privileges
		$this->CI->db->select("*"); //$this->USER_LEVEL_PRIV_TABLE_NAME_FIELD, $this->USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD, $this->USER_LEVEL_PRIV_PRIV_FIELD
		$this->CI->db->from($this->USER_LEVEL_PRIV_TABLE);
		if ($UserLevelID >= -1)
		{
			$this->CI->db->where($this->USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD, $UserLevelID);
		}
		$result2 = $this->CI->db->get();
		
		$this->UserLevelPriv = $result2->result_array();
		
		$result2->free_result();
	}
		
	// Get user privilege based on table name and User Level
	public function GetUserLevelPrivEx($TableName, $UserLevelID) { 
		
		if (strval($UserLevelID) == -1) { // System Administrator
		
			return 31;
			
		} elseif ($UserLevelID >= 0) {
			
			if (is_array($this->UserLevelPriv)) {
				foreach ($this->UserLevelPriv as $row) {
					#list($table, $levelid, $priv) = $row;
					// print_r($row); die;
					$table		= $row["module_type"];
					$levelid	= $row["userlevel_id"];
					$priv		= $row["permission"];
					
					if (strtolower($table) == strtolower($TableName) && strval($levelid) == strval($UserLevelID)) {
						if (is_null($priv) || !is_numeric($priv)) return 0;
						return intval($priv);
					}
				}
			}
		}
		return 0;
	}
	
	public function InitArray($iLen, $vValue) {
		if (function_exists('array_fill')) { // PHP 4 >= 4.2.0,
			return array_fill(0, $iLen, $vValue);
		} else {
			$aResult = array();
			for ($iCount = 0; $iCount < $iLen; $iCount++)
				$aResult[] = $vValue;
			return $aResult;
		}
	}
	
	public function setQueryStringValue($v) {
		
		$this->QueryStringValue = $this->StripSlashes($v);
		$this->CurrentValue = $this->QueryStringValue;
	}
	
	public function setFormValue($v) {
		$this->FormValue = StripSlashes($v);
		if (is_array($this->FormValue)) $this->FormValue = implode(",", $this->FormValue);
		$this->CurrentValue = $this->FormValue;
	}
	
	// Strip slashes
	public function StripSlashes($value) {
		if (!get_magic_quotes_gpc()) return $value;
		if (is_array($value)) { 
			return array_map('StripSlashes', $value);
		} else {
			return stripslashes($value);
		}
	}
	
	// Add slashes for SQL
	function AdjustSql($val) {
		$val = addslashes(trim($val));
		return $val;
	}
	
	// Check if menu item is allowed for current user level
	function AllowListMenu($TableName, $UserLevelID) {
		$arMenuUserLevelPriv = $this->FetchUserLevelPriv($UserLevelID);
		
		$logged_in = $this->CI->auth->is_logged_in(false, false);
		if ($logged_in) {
			if (strval($UserLevelID) == "-1") {
				return TRUE;
			} else {
				if (is_array($arMenuUserLevelPriv)) {
					foreach ($arMenuUserLevelPriv as $row) {
						
						$table		= $row["table_name"];
						$levelid	= $row["userlevel_id"];
						$priv		= $row["permission"];
						
						if (strtolower($table) == strtolower($TableName) && strval($levelid) == strval($UserLevelID)) {
							if (is_null($priv) || !is_numeric($priv)) return 0;
							return intval($priv);
						}
					}
				}
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}
	
	function FetchUserLevelPriv($UserLevelID){
		$this->CI->db->select("*");
		$this->CI->db->from($this->USER_LEVEL_PRIV_TABLE);
		if ($UserLevelID >= -1)
		{
			$this->CI->db->where($this->USER_LEVEL_PRIV_USER_LEVEL_ID_FIELD, $UserLevelID);
		}
		$result = $this->CI->db->get();
		return $result->result_array();
	}
	
	// Can add
	function CanAdd($Permission) {
		return (($Permission & $this->ALLOW_ADD) == $this->ALLOW_ADD);
	}
	
	// Can delete
	function CanDelete($Permission) {
		return (($Permission & $this->ALLOW_DELETE) == $this->ALLOW_DELETE);
	}
	
	// Can edit
	function CanEdit($Permission) {
		return (($Permission & $this->ALLOW_EDIT) == $this->ALLOW_EDIT);
	}
	
	// Can view
	function CanView($Permission) {
		
		return (($Permission & $this->ALLOW_VIEW) == $this->ALLOW_VIEW);
	}
	
	// Can list
	function CanList($Permission) {
		return (($Permission & $this->ALLOW_LIST) == $this->ALLOW_LIST);
	}
	
	//validate access permission
	function ValidateAccess($table_name, $mode, $redirect_url = false)
	{
		$user = $this->CI->session->userdata('user');
		
		$this->CI->load->model('User_model','',TRUE);
		$user = (object)$this->CI->User_model->get_record($user['user_id']);

		$this->SetUpUserLevelEx($user->userlevel_id);
	
		$Priv = $this->GetUserLevelPrivEx($table_name, $user->userlevel_id);
		
		switch($mode)
		{
			case "list":
				if (!$this->CanList($Priv))
				{
					return false;
				}
				else
				{
					
					return true;
				}
				break;
			case "view":
				
				if (!$this->CanView($Priv))
				{
					return false;
				}
				else
				{
					
					return true;
				}
				break;
			case "add":
				if (!$this->CanAdd($Priv))
				{
					return false;
				}
				else
				{
					return true;
				}
				break;
			case "edit":
				if (!$this->CanEdit($Priv))
				{
					return false;
				}
				else
				{
					return true;
				}
				break;
			case "delete";
				if (!$this->CanDelete($Priv))
				{
					return false;
				}
				else
				{
					return true;
				}
				break;
		}
		
	}
	
	function DynamicTables()
	{
		// Dynamic User Level table
		$USER_LEVEL_TABLE_NAME[0] = 'Surveys';
		$USER_LEVEL_TABLE_NAME[1] = 'Respondents';
		$USER_LEVEL_TABLE_NAME[2] = 'Reports';
		$USER_LEVEL_TABLE_NAME[3] = 'Users';
		$USER_LEVEL_TABLE_NAME[4] = 'Userlevels';
		$USER_LEVEL_TABLE_NAME[5] = 'Userlevel Permissions';
		$USER_LEVEL_TABLE_NAME[6] = 'Dynamic Fields';
		
		return $USER_LEVEL_TABLE_NAME;
	}

	
}
// END Access class

/* End of file Access.php */
/* Location: ./Application/libraries/Access.php */