<?php
class Userlevels_model extends MY_Model
{	
	public $tbl 			= 'userlevels';
	public $user_permission = 'userlevel_permissions';
	public $pk_field 		= 'id';

	public function fetch_record($param = array(), $count = false)
	{
		if(isset($param["search"]))
		{
			if(!empty($param['search']->term))
			{
				$search_fields = array(
										$this->tbl.'.userlevel_name', 
										$this->tbl.'.slug'
									);
				$term = explode(' ', $param['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t			= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
		}

		if(isset($param['id']))
		{
			$this->db->where($this->tbl.'.id',$param['id']);
		}
		
		if(!empty($param['limit']) && isset($param['limit']) > 0)
		{
			$this->db->limit($param['limit'], $param['offset']);
		}
		if(!empty($param['sort_by']))
		{
			$this->db->order_by($param['sort_by'], $param['sort_order']);
		}
			
		if(!$count)
		{
			$query = $this->db->get($this->tbl);

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = $row;
				}
				return $data;
			}
			return false;
		}
		else
		{
			return $this->db->count_all_results($this->tbl);
		}
	}

	function get_record($id = 0)
	{
		if($id)
		{
			$this->db->where($this->tbl.'.id',$id);
			return $this->db->get($this->tbl)->row_array();
		}
		return array();
	}
	
	function get_all(){
		return $this->db->get($this->tbl)->result();
	}
	
	function add_level($data){
		$this->db->insert($this->tbl, $data);
		return $this->db->insert_id();
	}
	
	function update($id, $data) {
		$this->db->where($this->pk_field, $id);
		$this->db->update($this->tbl, $data);
	}
	
	function delete($id) {
		return $this->db->delete($this->tbl , array('id' => $id));
	}
	
	function check_title($data, $id = false){
		if(!$id) 
		{
			return $this->db->get_where($this->tbl, $data)->row();
		}
		else {
			$data = array_merge($data, array('id !=' => $id));
			return $this->db->get_where($this->tbl, $data);
		}
	}
	
	function fetch($userlevel_id){
		$this->db->select('id as userlevel_id, userlevel_name');
		$this->db->where($this->pk_field, $userlevel_id);
		return $this->db->get($this->tbl)->result();
	}
	function fetch_row($userlevel_id){
		$this->db->select('id as userlevel_id, userlevel_name');
		$this->db->where($this->pk_field, $userlevel_id);
		return $this->db->get($this->tbl)->row();
	}
	
}
?>