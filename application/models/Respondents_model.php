<?php
class Respondents_model extends MY_Model
{	
	public $tbl 		= 'survey_respondents';
	public $tbl_survey 	= 'surveys';
	public $tbl_survey_q= 'survey_questions';
	public $tbl_users	= 'users';
	
	public function fetch_record($param = array(), $count = false)
	{
		$select_count = "(SELECT COUNT($this->tbl_survey_q.survey_id) FROM $this->tbl_survey_q WHERE $this->tbl_survey_q.survey_id = $this->tbl.survey_id) as total_questions";
		$this->db->select($this->tbl.'.*, '.$this->tbl_survey.'.survey_title, '.$select_count);
		$this->db->join("$this->tbl_survey", $this->tbl_survey.'.id = '.$this->tbl.'.survey_id', "left");

		if(isset($param['created_by']))
		{
			$this->db->join("$this->tbl_users", $this->tbl_users.'.id = '.$this->tbl_survey.'.created_by', "left");
			$this->db->where($this->tbl_survey.'.id', $param['created_by']);
		}

		if(isset($param["search"]))
		{
			if(!empty($param['search']->term))
			{
				$search_fields = array(
										$this->tbl.'.dynamic_fields'
									);
				$term = explode(' ', $param['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t			= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
			if(!empty($param['search']->survey))
			{
				$this->db->where($this->tbl.'.survey_id', $param['search']->survey);
			}
		}

		if(isset($param['id']))
		{
			$this->db->where($this->tbl.'.id',$param['id']);
		}

		if(isset($param['survey_id']))
		{
			$this->db->where($this->tbl.'.survey_id',$param['survey_id']);
		}
		
		if(!empty($param['limit']) && isset($param['limit']) > 0)
		{
			$this->db->limit($param['limit'], $param['offset']);
		}
		if(!empty($param['sort_by']))
		{
			$this->db->order_by($param['sort_by'], $param['sort_order']);
		}
			
		if(!$count)
		{
			$query = $this->db->get($this->tbl);

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = $row;
				}
				return $data;
			}
			return false;
		}
		else
		{
			return $this->db->count_all_results($this->tbl);
		}
	}
	
	function get_record($id = 0)
	{
		if($id)
		{
			$select_count = "(SELECT COUNT($this->tbl_survey_q.survey_id) FROM $this->tbl_survey_q WHERE $this->tbl_survey_q.survey_id = $this->tbl.survey_id) as total_questions";
			$this->db->select($this->tbl.'.*, '.$this->tbl_survey.'.survey_title, '.$select_count);
			$this->db->join("$this->tbl_survey", $this->tbl_survey.'.id = '.$this->tbl.'.survey_id', "left");

			$this->db->where($this->tbl.'.id',$id);
			return $this->db->get($this->tbl)->row_array();
		}
		return array();
	}
	
}
?>