<?php
class Surveys_model extends MY_Model
{	
	public $tbl 			= 'surveys';
	public $tbl_users 		= 'users';
	public $tbl_questions 	= 'survey_questions';
	public $tbl_res 		= 'survey_respondents';
	public $tbl_res_ans 	= 'survey_respondent_ans';
	
	public function fetch_record($param = array(), $count = false)
	{	

		$count_res = " (SELECT COUNT(survey_id) FROM survey_respondents WHERE survey_respondents.survey_id = surveys.id) as total_res";
		$this->db->select($this->tbl.'.*, CONCAT('.$this->tbl_users.'.first_name, " ", '.$this->tbl_users.'.last_name) as user_name, '.$count_res);
		$this->db->join("$this->tbl_users", $this->tbl_users.'.id = '.$this->tbl.".created_by","left");

		if(isset($param['created_by']))
		{
			$this->db->where($this->tbl.'.created_by', $param['created_by']);
		}

		if(isset($param["search"]))
		{
			if(!empty($param['search']->term))
			{
				$search_fields = array(
										$this->tbl.'.survey_title'
									);
				$term = explode(' ', $param['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t			= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
		}

		if(isset($param['id']))
		{
			$this->db->where($this->tbl.'.id',$param['id']);
		}

		if(isset($param['is_active']))
		{
			$this->db->where($this->tbl.'.is_active',$param['is_active']);
		}

		if(isset($param['created_by']))
		{
			$this->db->where($this->tbl.'.created_by',$param['created_by']);
		}
		
		if(!empty($param['limit']) && isset($param['limit']) > 0)
		{
			$this->db->limit($param['limit'], $param['offset']);
		}
		if(!empty($param['sort_by']))
		{
			$this->db->order_by($param['sort_by'], $param['sort_order']);
		}
			
		if(!$count)
		{
			$query = $this->db->get($this->tbl);

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = $row;
				}
				return $data;
			}
			return false;
		}
		else
		{
			return $this->db->count_all_results($this->tbl);
		}
	}
	
	function get_record($id = 0)
	{
		if($id)
		{
			$count_res = " (SELECT COUNT(survey_id) FROM survey_respondents WHERE survey_respondents.survey_id = surveys.id) as total_res";
			$this->db->select($this->tbl.'.*, CONCAT('.$this->tbl_users.'.first_name, " ", '.$this->tbl_users.'.last_name) as user_name, '.$count_res);
			$this->db->join("$this->tbl_users", $this->tbl_users.'.id = '.$this->tbl.".created_by","left");

			$this->db->where($this->tbl.'.id',$id);
			return $this->db->get($this->tbl)->row_array();
		}
		return array();
	}

	function get_record_by_slug($slug = 0)
	{
		if($slug)
		{
			$this->db->select($this->tbl.'.*, CONCAT('.$this->tbl_users.'.first_name, " ", '.$this->tbl_users.'.last_name) as user_name');
			$this->db->join("$this->tbl_users", $this->tbl_users.'.id = '.$this->tbl.".created_by","left");

			$this->db->where($this->tbl.'.survey_title_slug',$slug);
			return $this->db->get($this->tbl)->row_array();
		}
		return array();
	}

	function get_questions($params)
	{
		if($params)
		{
			if(isset($params["survey_id"]))
			{
				$this->db->where($this->tbl_questions.'.survey_id', $params["survey_id"]);
			}
			
			return $this->db->get($this->tbl_questions)->result();
		}
		return array();
	}

	function get_respondents_ans($params, $count = true)
	{
		if($params)
		{
			$this->db->select($this->tbl_res_ans.'.*, '.$this->tbl_res.'.date_created');
			$this->db->join("$this->tbl_res", $this->tbl_res.'.id = '.$this->tbl_res_ans.".respondent_id","left");

			if(isset($params["answer_index"]))
			{
				$this->db->where($this->tbl_res_ans.'.answer_index', $params["answer_index"]);
			}

			if(isset($params["survey_id"]))
			{
				$this->db->where($this->tbl_res_ans.'.survey_id', $params["survey_id"]);
			}

			if(isset($params["question_id"]))
			{
				$this->db->where($this->tbl_res_ans.'.question_id', $params["question_id"]);
			}

			if(isset($params["respondent_id"]))
			{
				$this->db->where($this->tbl_res_ans.'.respondent_id', $params["respondent_id"]);
			}
			
			if(!empty($params["date_from"]) && !empty($params["date_to"]))
			{
				$this->db->where("DATE(".$this->tbl_res.".date_created) BETWEEN '".$params["date_from"]."' AND '".$params["date_to"]."'");
			}

			if($count)
			{
				return $this->db->count_all_results($this->tbl_res_ans);
			}
			else
			{
				return $this->db->get($this->tbl_res_ans)->result();
			}
		}
		return array();
	}

	function get_survey_graph_data($params)
	{
		$this->db->select("COUNT(DISTINCT($this->tbl_res_ans.respondent_id)) as total_respondents, DATE($this->tbl_res.date_created) as date_created");
		$this->db->join("$this->tbl_res", $this->tbl_res.'.id = '.$this->tbl_res_ans.".respondent_id","left");

		if(isset($params["survey_id"]))
		{
			$this->db->where($this->tbl_res_ans.'.survey_id', $params["survey_id"]);
		}
		
		if(!empty($params["date_from"]) && !empty($params["date_to"]))
		{
			$this->db->where("DATE(".$this->tbl_res.".date_created) BETWEEN '".$params["date_from"]."' AND '".$params["date_to"]."'");
		}

		$this->db->group_by("DATE($this->tbl_res.date_created)");
		$this->db->order_by("DATE($this->tbl_res.date_created)", "ASC");

		return $this->db->get($this->tbl_res_ans)->result();
	}

	function dynamic_get_record($id_name = false, $id = false, $table_name = false)
	{
		if($id)
		{
			$this->db->where($id_name, $id);
			$query = $this->db->get($table_name);

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = (array)$row;
				}
				return $data;
			}
			return false;
		}
		return array();
	}
}
?>