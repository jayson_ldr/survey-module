<?php
class User_model extends MY_Model
{	
	public $tbl 			= 'users';
	public $tbl_userlevels 	= 'userlevels';
	
	public function fetch_record($param = array(), $count = false)
	{	

		$this->db->select($this->tbl.'.*, '.$this->tbl_userlevels.'.userlevel_name');
		$this->db->join("$this->tbl_userlevels", $this->tbl_userlevels.'.id = '.$this->tbl.'.userlevel_id', "left");

		if(isset($param["search"]))
		{
			if(!empty($param['search']->term))
			{
				$search_fields = array(
										$this->tbl.'.first_name', 
										$this->tbl.'.last_name', 
										$this->tbl.'.email'
									);
				$term = explode(' ', $param['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t			= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
		}

		$this->db->where($this->tbl.'.is_deleted', 0);

		if(isset($param['id']))
		{
			$this->db->where($this->tbl.'.id',$param['id']);
		}
		
		if(!empty($param['limit']) && isset($param['limit']) > 0)
		{
			$this->db->limit($param['limit'], $param['offset']);
		}
		if(!empty($param['sort_by']))
		{
			$this->db->order_by($param['sort_by'], $param['sort_order']);
		}
			
		if(!$count)
		{
			$query = $this->db->get($this->tbl);

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = $row;
				}
				return $data;
			}
			return false;
		}
		else
		{
			return $this->db->count_all_results($this->tbl);
		}
	}

	function fetch_userlevels()
	{
		return $this->db->get($this->tbl_userlevels)->result_array();
	}
	
	function update_logs($id)
	{
		$data = array('last_login' => date("Y/m/d h:i:s"));
		
        $this->db->where('id', $id);
        $this->db->update($this->tbl, $data);
    }
	
	function get_record($id = 0)
	{
		if($id)
		{
			$this->db->select($this->tbl.'.*, '.$this->tbl_userlevels.'.userlevel_name');
			$this->db->join("$this->tbl_userlevels", $this->tbl_userlevels.'.id = '.$this->tbl.'.userlevel_id', "left");

			$this->db->where($this->tbl.'.id',$id);
			return $this->db->get($this->tbl)->row_array();
		}
		return array();
	}
	
	function get_all_users()
	{
		return $this->db->get($this->tbl)->result_array();
	}
	
	/* function get_all_user_by_superior_id($id=0)
	{
		if($id != 1)
		{
			$this->db->where('superior_id',$id);
			return $this->db->get($this->tbl)->result_array();
		} 
		else if( $id == 1 ) 
		{
			$this->db->where( 'user_type_id !=' , 1);
			return $this->db->get($this->tbl)->result_array();
		}
		else
		{
			return array();
		}
	} */
	
	function update_user($id=0,$data=array()){
		if( $id && $data ){
			$this->db->where('id',$id);
			$this->db->update($this->tbl,$data);
			return $id;
		}
		return;
	}
	
}
?>