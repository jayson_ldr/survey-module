<?php
class Fields_model extends MY_Model
{	
	public $tbl = 'fields';
	
	public function fetch_record($param = array(), $count = false)
	{
		if(isset($param["search"]))
		{
			if(!empty($param['search']->term))
			{
				$search_fields = array(
										$this->tbl.'.name'
									);
				$term = explode(' ', $param['search']->term);
				
				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t			= substr($t,1,strlen($t));
					}

					$index	 = 0;
					$like	 = '';
					$like	.= "(";
					foreach ($search_fields as $field){
						$index++;
						
						$like .= " ".$field." ".$not."LIKE '%".$t."%' ";
						$like .= (count($search_fields) <= $index) ? "" : $operator;
					}
					$like	.= ") ";
					
					$this->db->where($like);
				}
			}
		}

		if(isset($param['id']))
		{
			$this->db->where($this->tbl.'.id',$param['id']);
		}

		if(isset($param['is_active']))
		{
			$this->db->where($this->tbl.'.is_active',$param['is_active']);
		}
		
		if(!empty($param['limit']) && isset($param['limit']) > 0)
		{
			$this->db->limit($param['limit'], $param['offset']);
		}
		if(!empty($param['sort_by']))
		{
			$this->db->order_by($param['sort_by'], $param['sort_order']);
		}
			
		if(!$count)
		{
			$query = $this->db->get($this->tbl);

			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$data[] = $row;
				}
				return $data;
			}
			return false;
		}
		else
		{
			return $this->db->count_all_results($this->tbl);
		}
	}
	
	function get_record($id = 0)
	{
		if($id)
		{
			$this->db->where($this->tbl.'.id',$id);
			return $this->db->get($this->tbl)->row_array();
		}
		return array();
	}
	
	function get_all_users()
	{
		return $this->db->get($this->tbl)->result_array();
	}
	
	function update_user($id=0,$data=array()){
		if( $id && $data ){
			$this->db->where('id',$id);
			$this->db->update($this->tbl,$data);
			return $id;
		}
		return;
	}
	
}
?>