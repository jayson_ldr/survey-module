$(function () {
	$('[data-toggle="tooltip"]').tooltip();

	$('#dynamicModal').on('shown.bs.modal', function (event) {
		var button 		= $(event.relatedTarget);
		var btn_action 	= button.data('action');
		var controller 	= button.data('controller');
		var modal 		= $(this);
		
		modal.find(".btn_save_action").show();
		modal.find(".btn_save_action").attr("data-controller", controller);
		modal.find(".btn_save_action").attr("data-mode", btn_action);
		
		if( (btn_action == "view") || (btn_action == "edit") || (btn_action == "add") )
		{
			if( (btn_action == "view") || (btn_action == "edit") )
			{
				if(btn_action == "view")
				{
					modal.find(".btn_save_action").hide();
				}
				
				var data_id = button.data('id');
				$.post(base_url + controller + "/view_record", { id: data_id, mode: btn_action }, function(data){
					modal.find(".modal-body").html(data);
				});

				// tinymce
				// Remove specific instance by id
				// tinymce.remove('#question_title');

				// Remove all editors bound to textareas
				tinymce.remove('textarea');
			}
			else if(btn_action == "add")
			{
				$.post(base_url + controller + "/index", function(data){
					modal.find(".modal-body").html(data);
				});
			}			
		}
	});
	
	$(".form-module").validate({
		rules:{
			email:{
				required:	true,
				email: 		true
			},
			category: 			"required",
			rate: 				"required",
			room_no: 			"required",
			room_category_id: 	"required",
		},
		messages:{
			email:{
				required:	"Email is required",
				email:		"Please type a valid email"
			}
		}
	});
	
	$(".btn_save_action").click(function(){
		var $form = $(".form-module");
		
		if($form.valid())
		{
			swal({
				title: "Are you sure?",
				text: "Your about to save a reccord.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Save",
				cancelButtonText: "Cancel",
				closeOnConfirm: false,
				closeOnCancel: false,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				if (isConfirm) {
					var controller 	= $(".btn_save_action").data("controller");
					var mode	 	= $(".btn_save_action").data("mode");
					
					$.post(base_url + controller + "/save_record", $form.serialize(), function(data){
						$('#dynamicModal').modal('hide');
						$('#dynamicModal').find(".modal-body").html("");
						swal(mode.toUpperCase() + "ED!", "Transaction Successful.", "success");						
						setTimeout(function(){
							window.location.href = base_url + controller;
						}, 1000);
					});
					
				} else {
					swal("Cancelled", "Transaction has been cancelled", "error");
				}
			});
		}
	});
	
	$(".btn_search_action").click(function(){
		$(".form-search").submit();
	});

	$(".btn_search_action_new").click(function(){
		$(".form-search-new").submit();
	});
	
	$(".btn_delete_action").click(function(){
		var $obj = $(this);
		
		swal({
			title: "Are you sure?",
			text: "Your about to delete a reccord.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel",
			closeOnConfirm: false,
			closeOnCancel: false,
			showLoaderOnConfirm: true,
		},
		function(isConfirm){
			if (isConfirm) {
				var id 			= $obj.data("id");	
				var controller 	= $obj.data("controller");
				
				$.post(base_url + controller + "/delete_record", {id: id}, function(data){					
					swal("Deleted!", "Transaction Successful.", "success");					
					setTimeout(function(){
						window.location.href = base_url + controller;
					}, 1000);
				});				
			} else {
				swal("Cancelled", "Transaction has been cancelled", "error");
			}
		});
	});
	
	$(document).on("change", "#room_category_id", function(){
		var $obj 		= $(this);
		var controller 	= $obj.data("controller");
		var cat_id		= $obj.val();
		
		$.post(base_url + controller + "/fetch_rooms_by_cat", {cat_id: cat_id}, function(data){					
			$(".ajax_rooms").html(data);
		});		
	});
	
	$(".action_change_stats").click(function(){
		var $obj 		= $(this);
		var controller 	= $obj.data("controller");
		var room_id 	= $obj.data("room_id");
		var id 			= $obj.data("id");
		
		swal({
			title: "Are you sure?",
			text: "Your about to update a reccord.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Checkout",
			cancelButtonText: "Cancel",
			closeOnConfirm: false,
			closeOnCancel: false,
			showLoaderOnConfirm: true,
		},
		function(isConfirm){
			if (isConfirm) {
				
				$.post(base_url + controller + "/update_reservation_status", {room_id: room_id, id: id}, function(data){					
					swal("Update!", "Update Successful.", "success");
					
					$(".reservation_status_" + id).html("Checked Out");
				});				
			} else {
				swal("Cancelled", "Transaction has been cancelled", "error");
			}
		});
		
	});
})