var arr_values      = [];
var qs_time         = 0;
var respondent_id   = 0;
var start_time      = 0;

(function($) {

    $.fn.cslide = function() {

        this.each(function() {

            var slidesContainerId = "#"+($(this).attr("id"));
            
            var len = $(slidesContainerId+" .cslide-slide").size();     // get number of slides
            var slidesContainerWidth = len*100+"%";                     // get width of the slide container
            var slideWidth = (100/len)+"%";                             // get width of the slides
            
            // set slide container width
            $(slidesContainerId+" .cslide-slides-container").css({
                width : slidesContainerWidth,
                visibility : "visible"
            });

            // set slide width
            $(".cslide-slide").css({
                width : slideWidth
            });

            // add correct classes to first and last slide
            $(slidesContainerId+" .cslide-slides-container .cslide-slide").last().addClass("cslide-last");
            $(slidesContainerId+" .cslide-slides-container .cslide-slide").first().addClass("cslide-first cslide-active");

            // initially disable the previous arrow cuz we start on the first slide
            $(slidesContainerId+" .cslide-prev").addClass("cslide-disabled");

            // if first slide is last slide, hide the prev-next navigation
            if (!$(slidesContainerId+" .cslide-slide.cslide-active.cslide-first").hasClass("cslide-last")) {           
                $(slidesContainerId+" .cslide-prev-next").css({
                    display : "block"
                });
            }

            // handle the next clicking functionality
            $(slidesContainerId+" .cslide-next").click(function(){
                var key = $(this).data("qindex");

                swal({
                    title: "Are you sure?",
                    text: "Your about to submit your answer.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Submit",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: false,
                },
                function(isConfirm){
                    if (isConfirm) {
                        var msg_response = validate_on_submit(key);

                        if(msg_response != "no-answer")
                        {
                            swal(msg_response, "Answer submitted", "success");

                            //---- original code
                            var i = $(slidesContainerId+" .cslide-slide.cslide-active").index();
                            var n = i+1;
                            var slideLeft = "-"+n*100+"%";
                            if (!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) {
                                $(slidesContainerId+" .cslide-slide.cslide-active").removeClass("cslide-active").next(".cslide-slide").addClass("cslide-active");
                                $(slidesContainerId+" .cslide-slides-container").animate({
                                    marginLeft : slideLeft
                                },250);
                                if ($(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) {
                                    $(slidesContainerId+" .cslide-next").addClass("cslide-disabled");
                                }
                            }
                            if ((!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-first")) && $(".cslide-prev").hasClass("cslide-disabled")) {
                                $(slidesContainerId+" .cslide-prev").removeClass("cslide-disabled");
                            }
                            //---- original code
                        }
                        else
                        {
                            swal("Error", "Wrong answer", "error");
                        }
                    }
                    else
                    {
                        swal("Cancelled", "Submit cancelled", "error");
                    }
                });

            });

            // handle the skip clicking functionality
            $(slidesContainerId+" .cslide-next-skip").click(function(){
                var key = $(this).data("qindex");

                swal({
                    title: "Are you sure?",
                    text: "Your about to submit your answer.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Submit",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: false,
                },
                function(isConfirm){
                    if (isConfirm) {

                        var q_id        = $("#qid_" + key).val();
                        var q_action    = "skip";
                        var q_ans_index = "";
                        save_question_details(q_id, q_action, q_ans_index);

                        swal("Skipped!", "Answer submitted", "success");

                        var i = $(slidesContainerId+" .cslide-slide.cslide-active").index();
                        var n = i+1;
                        var slideLeft = "-"+n*100+"%";
                        if (!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) {
                            $(slidesContainerId+" .cslide-slide.cslide-active").removeClass("cslide-active").next(".cslide-slide").addClass("cslide-active");
                            $(slidesContainerId+" .cslide-slides-container").animate({
                                marginLeft : slideLeft
                            },250);
                            if ($(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) {
                                $(slidesContainerId+" .cslide-next").addClass("cslide-disabled");
                            }
                        }
                        if ((!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-first")) && $(".cslide-prev").hasClass("cslide-disabled")) {
                            $(slidesContainerId+" .cslide-prev").removeClass("cslide-disabled");
                        }
                    }
                });

            });

            // handles the initial button "proceed" clicking functionality
            $(slidesContainerId+" .cslide-next-info").click(function(){
                var survey_id   = $("#survey_id").val();

                var token = 1;
                $('.dynamic_fields').each(function(index, obj){
                    if($(obj).val() == "")
                    {
                        token = 0;
                    }
                });

                if(token == "0")
                {
                    swal("Error", "All Fields are required", "error");
                    return false;
                }       

                swal({
                    title: "Are you sure?",
                    text: "Your about to save your information.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Submit",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    showLoaderOnConfirm: false,
                },
                function(isConfirm){
                    if (isConfirm) {

                        var msg = save_respondent();
                        if(msg == "saved")
                        {
                            swal("Saved!", "Info saved! Good luck with the survey.", "success");

                            start_time  = new Date();
                            start_time  = start_time.getTime();

                            var i = $(slidesContainerId+" .cslide-slide.cslide-active").index();
                            var n = i+1;
                            var slideLeft = "-"+n*100+"%";
                            if (!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) {
                                $(slidesContainerId+" .cslide-slide.cslide-active").removeClass("cslide-active").next(".cslide-slide").addClass("cslide-active");
                                $(slidesContainerId+" .cslide-slides-container").animate({
                                    marginLeft : slideLeft
                                },250);
                                if ($(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) {
                                    $(slidesContainerId+" .cslide-next").addClass("cslide-disabled");
                                }
                            }
                            if ((!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-first")) && $(".cslide-prev").hasClass("cslide-disabled")) {
                                $(slidesContainerId+" .cslide-prev").removeClass("cslide-disabled");
                            }
                        }
                        else
                        {
                            swal("Error", "All fields are required", "error");
                        }
                    }
                    else
                    {
                        swal("Cancelled", "Submit cancelled", "error");
                    }
                });

            });

            // handle the prev clicking functionality
            $(slidesContainerId+" .cslide-prev").click(function(){
                var i = $(slidesContainerId+" .cslide-slide.cslide-active").index();
                var n = i-1;
                var slideRight = "-"+n*100+"%";
                if (!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-first")) {
                    $(slidesContainerId+" .cslide-slide.cslide-active").removeClass("cslide-active").prev(".cslide-slide").addClass("cslide-active");
                    $(slidesContainerId+" .cslide-slides-container").animate({
                        marginLeft : slideRight
                    },250);
                    if ($(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-first")) {
                        $(slidesContainerId+" .cslide-prev").addClass("cslide-disabled");
                    }
                }
                if ((!$(slidesContainerId+" .cslide-slide.cslide-active").hasClass("cslide-last")) && $(".cslide-next").hasClass("cslide-disabled")) {
                    $(slidesContainerId+" .cslide-next").removeClass("cslide-disabled");
                }
            });

            // functioni to validate answer every submit
            function validate_on_submit(key)
            {  
                var return_val  = "no-answer";
                var type        = $("#type_" + key).val();
                var q_id        = $("#qid_" + key).val();
                var right_ans   = $("#right_answer_" + key).val();
                var final_score = $(".final_score").text();
                var q_ans_index = "";
                var q_action    = "";

                console.log("question id: " + q_id);
                console.log("question type: " + type);
                console.log("right ans index: " + right_ans);

                $(slidesContainerId+" .cslide-slide.cslide-active .selected_answer").each(function() {
                    var val         = $(this).val();
                    var qIndex      = $(this).data("qindex");
                    var ansIndex    = $(this).data("ansindex");

                    console.log("selected ans: " + val);
                    console.log("selected ans index: " + ansIndex);

                    if(type == "skip")
                    {
                        if(ansIndex == right_ans)
                        {
                            return_val  = "Correct!";
                            console.log("tama");

                            final_score = parseInt(final_score) + 1
                            $(".final_score").html(final_score);
                        }
                        else
                        {
                            return_val  = "Wrong Answer!";
                            console.log("mali");
                        }

                        q_action    = "submit";
                        q_ans_index = ansIndex;
                        save_question_details(q_id, q_action, q_ans_index);

                    }
                    else if(type == "force")
                    {
                        if(ansIndex == right_ans)
                        {
                            return_val  = "Correct!";
                            console.log("tama");

                            final_score = parseInt(final_score) + 1
                            $(".final_score").html(final_score);

                            q_action    = "submit";
                            q_ans_index = ansIndex;
                            save_question_details(q_id, q_action, q_ans_index);
                        }
                        else
                        {
                            return_val  = "no-answer";
                            console.log("mali");
                        }
                    }
                    else if(type == "attempt")
                    {
                        var attemp_total = $(".attemp_total_" + key).text();
                        var attemp_count = $(".attemp_count_" + key).text();
                        var new_count    = parseInt(attemp_count) + 1;
                        $(".attemp_count_" + key).html(new_count);

                        console.log("attemp_total: " + attemp_total);
                        console.log("attemp_count: " + new_count);

                        if(ansIndex == right_ans)
                        {
                            return_val  = "Correct!";
                            console.log("tama");

                            final_score = parseInt(final_score) + 1
                            $(".final_score").html(final_score);

                            q_action    = "submit";
                            q_ans_index = ansIndex;
                            save_question_details(q_id, q_action, q_ans_index);
                        }
                        else if(new_count <= attemp_total)
                        {
                            return_val  = "no-answer";
                            console.log("mali");
                        }
                        else
                        {
                            return_val  = "Attempts Failed!";;
                            console.log("failed");

                            q_action    = "submit";
                            q_ans_index = ansIndex;
                            save_question_details(q_id, q_action, q_ans_index);
                        }
                    }
                });

                return return_val;   
            }

            function save_question_details(q_id, q_action, q_ans_index)
            {
                //###########
                qs_time         = (qs_time == "0") ? start_time : qs_time;
                var qf_time     = new Date();
                qf_time         = qf_time.getTime();
                var diff        = parseFloat(qf_time - qs_time);
                var q_duration  = parseFloat(diff/1000).toFixed(2);
                qs_time         = qf_time; //set the last time to be the start for the next question
                //###########

                var info = {
                            "question_id":  q_id ,
                            "action":       q_action,
                            "answer_index": q_ans_index,
                            "duration":     q_duration
                        };
                
                arr_values.push(info);
                console.log(arr_values);
            }

            function save_respondent()
            {
                var survey_id   = $("#survey_id").val();
                var dynamic_fields  = [];
                var datastring  = [];
                var d_field = {};
                
                $('.dynamic_fields').each(function(index, obj){
                    var field_name = $(obj).attr("name");

                    d_field[field_name] = $(this).val();
                });
                dynamic_fields.push(d_field);

                datastring.push({name: "dynamic_fields", value: JSON.stringify(dynamic_fields)});
                datastring.push({name: "survey_id", value: survey_id});

                console.log(datastring);

                $.post(base_url + "surveys/save_respondent_info", datastring, function(data){
                    respondent_id = data.respondent_id;
                    console.log("respondent_id = "+respondent_id);

                    if(data.error_val == "1")
                    {
                        swal("Error!", "Survey not available anymore, quota already reached.", "error");            
                        setTimeout(function(){
                            window.location.href = base_url + "surveys";
                        }, 1000);
                    }

                },"JSON");

                return "saved";
            }
        });

        // return this for chainability
        return this;

    }

    $("#cslide-slides .check_ans").click(function(){
        $("#cslide-slides .check_ans").removeClass("selected_answer");
        $(this).addClass("selected_answer");
    });

    $("#fin").click(function(){
        var final_score = $(".final_score").text();
        var survey_id   = $("#survey_id").val();
        var finish_time = new Date();
        finish_time     = finish_time.getTime();
        var diff        = parseFloat(finish_time - start_time);
        var seconds     = parseFloat(diff/1000).toFixed(2);

        console.log("final_score: " + final_score);
        console.log("overall_duration: " + seconds);
        console.log("respondent_id: " + respondent_id);
        console.log("survey_id: " + survey_id);

        $.post(base_url + "surveys/save_respondent_ans", {
            score:              final_score,
            overall_duration:   seconds,
            respondent_id:      respondent_id,
            survey_id:          survey_id, 
            answers:            JSON.stringify(arr_values) 
        }, function(data){
            swal("Success!", "Survey Completed", "success");
            
            setTimeout(function(){
                window.location.href = base_url + "surveys";
            }, 1000);
        });
    });

}(jQuery));