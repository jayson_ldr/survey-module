$(function () {

	$('.collapse_new').collapse();

	$(".form-survey").validate({
		rules:{
			category: 		"required",
			question_title: "required",
			type: 			"required",
			attempt_no: 	"required",
			ans_type: 		"required", 
		}
	});
	
	$("#submit_survey").click(function(){
		var $form = $(".form-survey");
		
		if($form.valid())
		{
			swal({
				title: "Are you sure?",
				text: "Your about to save a record.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Submit",
				cancelButtonText: "Cancel",
				closeOnConfirm: true,
				closeOnCancel: false,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				if (isConfirm) {
					
					$.post(base_url + "admin/surveys/survey_wizard", $form.serialize(), function(data){
						$('#survey_wiz a[href="#questions"]').attr("data-toggle", "tab");
						$('#survey_wiz a[href="#questions"]').removeClass("disabled");
						$('#survey_wiz a[href="#questions"]').tab('show');

						$.post(base_url + "admin/surveys/ajax_form_question", function(data){
							$(".form_questions").html(data);
						});
					});
					
				} else {
					swal("Cancelled", "Transaction has been cancelled", "error");
				}
			});
		}
	});

	$(".form-survey-question").validate({
		ignore: '',
		rules:{
			question_title: "required",
			type: 			"required",
			attempt_no: 	"required",
			ans_type: 		"required", 
		}
	});

	$(document).on("click", "#submit_question", function() {
		var $form = $(".form-survey-question");
		tinyMCE.triggerSave();

		var istinymceValid = $("#question_title").val();
		if (!istinymceValid) {
			swal("Error!", "Question field is required.", "error");
			return false;
		}
		
		if($form.valid())
		{
			swal({
				title: "Are you sure?",
				text: "Your about to save a record.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Submit",
				cancelButtonText: "Cancel",
				closeOnConfirm: true,
				closeOnCancel: false,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				$("tr.to_remove").remove();

				if (isConfirm) {	
					$.post(base_url + "admin/surveys/ajax_form_question", $form.serialize(), function(data){
						tinymce.get("question_title").setContent('');

						$("#question_title").val("");
						$("#type").val("");
						$("#attempt_no").prop("disabled", true);
						$("#attempt_no").val("");
						$(".answer_content").html("");
						$("#ans_type").val("");

						$("#submit_preview").prop("disabled", false);
					});
					
				} else {
					swal("Cancelled", "Transaction has been cancelled", "error");
				}
			});
		}
	});
	
	$(document).on("click", "#submit_preview", function() {

		swal({
			title: "Are you sure you want to preview?",
			text: "Your about to save a record.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Preview",
			cancelButtonText: "Cancel",
			closeOnConfirm: true,
			closeOnCancel: false,
			showLoaderOnConfirm: true,
		},
		function(isConfirm){
			if (isConfirm) {	
				$.post(base_url + "admin/surveys/ajax_form_preview", function(data){
					$('#survey_wiz a[href="#preview"]').attr("data-toggle", "tab");
					$('#survey_wiz a[href="#preview"]').removeClass("disabled");
					$('#survey_wiz a[href="#preview"]').tab('show');

					$(".form_preview").html(data);
				});
				
			} else {
				swal("Cancelled", "Transaction has been cancelled", "error");
			}
		});

	});

	$(".form-survey-preview").validate({
		ignore: false,
		rules:{
			'question[]': "required",
		}
	});

	$(document).on("click", "#submit_publish", function() {
		var $form = $(".form-survey-preview");
		
		if($form.valid())
		{
			swal({
				title: "Are you sure you want to Publish?",
				text: "Your about to save a record.",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Submit",
				cancelButtonText: "Cancel",
				closeOnConfirm: false,
				closeOnCancel: false,
				showLoaderOnConfirm: true,
			},
			function(isConfirm){
				if (isConfirm) {	
					$.post(base_url + "admin/surveys/ajax_form_preview", $form.serialize(), function(data){
						$('#survey_wiz a[href="#survey"]').attr("data-toggle", "");
						$('#survey_wiz a[href="#survey"]').addClass("disabled");
						
						$('#survey_wiz a[href="#questions"]').attr("data-toggle", "");
						$('#survey_wiz a[href="#questions"]').addClass("disabled");

						$('#survey_wiz a[href="#preview"]').attr("data-toggle", "");
						$('#survey_wiz a[href="#preview"]').addClass("disabled");

						$('#survey_wiz a[href="#publish"]').attr("data-toggle", "tab");
						$('#survey_wiz a[href="#publish"]').removeClass("disabled");
						$('#survey_wiz a[href="#publish"]').tab('show');

						$(".form_preview").html("");
						$(".form_publish").html(data);

						swal("Published!", "Transaction Successful.", "success");
					});
					
				} else {
					swal("Cancelled", "Transaction has been cancelled", "error");
				}
			});
		}
	});

	$('#survey_title').on('input',function(e){
		$('#survey_wiz a[href="#questions"]').attr("data-toggle", "");
		$('#survey_wiz a[href="#questions"]').addClass("disabled");

		$('#survey_wiz a[href="#preview"]').attr("data-toggle", "");
		$('#survey_wiz a[href="#preview"]').addClass("disabled");
	});
	
	$(document).on("change", "#type", function(event) {
		var type = $(this).val();

		if(type == "attempt")
		{
			$("#attempt_no").prop("disabled", false);
		}
		else
		{
			$("#attempt_no").prop("disabled", true);
			$("#attempt_no").val("");
		}

		
	});

	$(document).on("change", "#ans_type", function(event) {
		var type = $(this).val();
		$( ".answer_content" ).html("");
		
		if(type == "1")
		{
			$( "#MultipleChoiceTable" ).clone().appendTo( ".answer_content" );
		}
		else if(type == "2")
		{
			$( "#YesNoTable" ).clone().appendTo( ".answer_content" );
		}
		else
		{
			$( ".answer_content" ).html("");
		}
	});

	$(document).on("click", "#MultipleChoice_Add", function(event) {
		event.preventDefault();
		var count_row = $('.answer_content #MultipleChoiceTable tbody tr').length + 1;


		var row_append = "<tr><td><div class=\"ans_list\"><input type=\"radio\" id=\"MultipleChoice\" /> <input type=\"text\" id=\"MultipleChoice_" + count_row +"\" name=\"MultipleChoice[]\" value=\"Multiple Choice " + count_row + "\" class=\"form-control custom_input\" /> <img src=\"" + base_url + "assets/img/x-button.png\" class=\"ans_delete\"/></div></td></tr>";
		$(".answer_content #MultipleChoiceTable tbody").append(row_append);
	});
	$(document).on("click", ".answer_content table#MultipleChoiceTable .ans_delete", function() {
		$(this).parents("tr").addClass("to_remove").fadeOut();
	});

})