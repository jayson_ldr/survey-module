$(function(){
	$(".date").datepicker({
	    format: 'yyyy/mm/dd',
	    autoclose: true,
	});

	$("#date_from").datepicker({
	    format: 'yyyy/mm/dd',
	    autoclose: true,
	}).on('changeDate', function (selected) {
	    var startDate = new Date(selected.date.valueOf());
	    $('#date_to').datepicker('setStartDate', startDate);
	}).on('clearDate', function (selected) {
	    $('#date_to').datepicker('setStartDate', null);
	});

	$("#date_to").datepicker({
	    format: 'yyyy/mm/dd',
	    autoclose: true,
	}).on('changeDate', function (selected) {
	    var endDate = new Date(selected.date.valueOf());
	    $('#date_from').datepicker('setEndDate', endDate);
	}).on('clearDate', function (selected) {
	    $('#date_from').datepicker('setEndDate', null);
	});

	/***************************************************
	** stats - select survey [START]
	****************************************************/
	$('.select_survey').select2().on("change", function(e) {
		var survey_id = $(this).val();

		$.post(base_url + "admin/stats/get_questions", {survey_id:survey_id}, function(data){
            $(".question_list").html(data);            
            //fix to select2 bug when showing the error msg
            $("#survey-error").remove();
            set_question_info(survey_id);
        });
	});

	$(".form-search").validate({
		ignore: 'input[type=hidden]',
		rules:{
			survey: "required"
		}
	});

	$("#action_search_survey").click(function(){
		var $form = $(".form-search");
		if($form.valid())
		{
			var survey_id 	= $("#survey").val();
			$.post(base_url + "admin/stats/get_questions", {survey_id:survey_id}, function(data){
	            $(".question_list").html(data);

	            $('.graph_container').html("");
				$('.graph_container').append('<canvas id="myChart"><canvas>');

	            set_question_info(survey_id);
	        });
		}
	});

	function set_question_info(survey_id)
	{
		if(survey_id)
        {
            $(".answers_list").html('<div class="alert alert-warning" role="alert"><strong>Notice: </strong> Select from the avilable questions above.</div>');

	        $.post(base_url + "admin/stats/get_survey_info", {survey_id:survey_id}, function(data){
	        	if(data != null)
	        	{
	            	$(".s_title").html(data.survey_title);
	            	$(".s_quota").html(data.total_res + " / " + data.quota);
	            	$(".s_author").html(data.user_name);
	            	$(".survey_overview").removeAttr("hidden");
	            	$(".survey_overview").show();
	            }
	            else
	            {
	            	$(".s_title").html("");
	            	$(".s_quota").html("");
	            	$(".s_author").html("");
	            	$(".survey_overview").hide();
	            }
	        },"JSON");

	        var date_from 	= $("#date_from").val();
			var date_to 	= $("#date_to").val();

	       	//lets display the graph
	        $.post(base_url + "admin/stats/display_graph", {
	    		survey_id: 	survey_id, 
	    		date_from: 	date_from, 
	    		date_to: 	date_to
	    	}, function(data){
	        	console.log(data);

	        	set_graph(data.date, data.total);
	        },"JSON");
        }
        else
        {
        	$(".answers_list").html('<div class="alert alert-warning" role="alert"><strong>Notice: </strong> Select from the avilable survey first.</div>');

			$(".s_title").html("");
			$(".s_quota").html("");
			$(".s_author").html("");
			$(".survey_overview").hide();

			$('.graph_container').html("");
			$('.graph_container').append('<canvas id="myChart"><canvas>');
        }

	}

	function set_graph(date, total)
	{
	    var ctx = document.getElementById("myChart");
		var myChart = new Chart(ctx, {
		    type: 'bar',
		    data: {
		        labels: date, //["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
		        datasets: [{
		            label: 'No. of Respondents',
		            data: total, //[12, 19, 3, 5, 2, 3],
		            backgroundColor: 'rgba(54, 162, 235, 0.2)',
		            borderColor: 'rgba(54, 162, 235, 1)',
		            borderWidth: 1
		        }]
		    },
		    options: {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }],
		            xAxes: [{ barPercentage: 0.5 }]
		        }
		    }
		});	
	}
	/***************************************************
	** stats - select survey [END]
	****************************************************/

	$(document).on("change", "#question", function(){
		var question_id = $(this).val();
		var survey_id 	= $("#survey").val();
		var date_from 	= $("#date_from").val();
		var date_to 	= $("#date_to").val();

		$.post(base_url + "admin/stats/get_answers", {
			question_id: 	question_id,
			survey_id: 		survey_id,
			date_from: 		date_from,
			date_to: 		date_to
		}, function(data){
            $(".answers_list").html(data);
        });
	});

	/***************************************************
	** Respondents Fields JS [START]
	****************************************************/
	$(document).on("change", "#type", function(){
		var type = $(this).val();
		if(type == "textbox")
		{
			$(".add_content").prop("disabled", true);
			$("#data_type").prop("disabled", false);
			$(".content_list").html("");
		}
		else
		{
			$(".add_content").prop("disabled", false);
			$("#data_type").prop("disabled", true);
			$("#data_type").val('');
		}
	});

	$(document).on("click", ".add_content", function(){
		$( ".content_list" ).append( "<div style='margin-bottom: 5px'><input type='text' name='content[]' class='form-control' style='width: 90%; float: left'><button type='button' class='btn btn-danger col-md-1 remove_field' style='float: left; margin-left: 5px;'>-</button><div class='clearfix'></div></div>" );
	});

	$(document).on("click", ".remove_field", function(){
		$(this).parent().remove();
	});
	/***************************************************
	** Respondents Fields JS [END]
	****************************************************/

	/**
	* this workaround fixed the issue when tinymce is called inside a bootstrap modal
	* thanks @harry: http://stackoverflow.com/questions/18111582/tinymce-4-links-plugin-modal-in-not-editable
	*/
	$(document).on('focusin', function(e) {
		if ($(e.target).closest(".mce-window").length) {
		    e.stopImmediatePropagation();
		}
	});
});